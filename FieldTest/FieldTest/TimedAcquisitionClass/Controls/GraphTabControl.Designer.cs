﻿namespace FieldTest.TimedAcquisitionClass.Controls
{
    partial class GraphTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title10 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chartAcquisition = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTagSerie2 = new System.Windows.Forms.Label();
            this.lblTagSerie1 = new System.Windows.Forms.Label();
            this.btnColor2 = new System.Windows.Forms.Button();
            this.btnColor1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.lblUnitSerie2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblUnitSerie1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboDevice2 = new System.Windows.Forms.ComboBox();
            this.cboDevice1 = new System.Windows.Forms.ComboBox();
            this.cboReadDevice2 = new System.Windows.Forms.ComboBox();
            this.cboReadDevice1 = new System.Windows.Forms.ComboBox();
            this.colorDialogTimeAcq = new System.Windows.Forms.ColorDialog();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAcquisition)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.chartAcquisition);
            this.groupBox5.Location = new System.Drawing.Point(0, 105);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(817, 511);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Graph";
            // 
            // chartAcquisition
            // 
            chartArea10.AxisX.IsStartedFromZero = false;
            chartArea10.AxisX.LabelStyle.Format = "HH:mm:ss";
            chartArea10.AxisX.Title = "Time";
            chartArea10.AxisY.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY.IsStartedFromZero = false;
            chartArea10.AxisY.LabelStyle.Format = "N2";
            chartArea10.AxisY.LabelStyle.Interval = 0D;
            chartArea10.AxisY.LabelStyle.IntervalOffset = 0D;
            chartArea10.AxisY.LabelStyle.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY.Title = "Data";
            chartArea10.AxisY2.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY2.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY2.IsStartedFromZero = false;
            chartArea10.AxisY2.LabelStyle.Format = "N2";
            chartArea10.AxisY2.LabelStyle.Interval = 0D;
            chartArea10.AxisY2.LabelStyle.IntervalOffset = 0D;
            chartArea10.AxisY2.LabelStyle.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY2.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.AxisY2.Title = "Data";
            chartArea10.CursorX.Interval = 500D;
            chartArea10.CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Milliseconds;
            chartArea10.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Milliseconds;
            chartArea10.CursorX.IsUserEnabled = true;
            chartArea10.CursorX.IsUserSelectionEnabled = true;
            chartArea10.CursorY.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.CursorY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea10.CursorY.IsUserEnabled = true;
            chartArea10.CursorY.IsUserSelectionEnabled = true;
            chartArea10.Name = "ChartArea1";
            this.chartAcquisition.ChartAreas.Add(chartArea10);
            this.chartAcquisition.Dock = System.Windows.Forms.DockStyle.Fill;
            legend10.Alignment = System.Drawing.StringAlignment.Far;
            legend10.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend10.Name = "Legend1";
            this.chartAcquisition.Legends.Add(legend10);
            this.chartAcquisition.Location = new System.Drawing.Point(3, 16);
            this.chartAcquisition.Name = "chartAcquisition";
            series19.ChartArea = "ChartArea1";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series19.Legend = "Legend1";
            series19.Name = "Series1";
            series19.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series19.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series20.ChartArea = "ChartArea1";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series20.Legend = "Legend1";
            series20.Name = "Series2";
            series20.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series20.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series20.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chartAcquisition.Series.Add(series19);
            this.chartAcquisition.Series.Add(series20);
            this.chartAcquisition.Size = new System.Drawing.Size(811, 492);
            this.chartAcquisition.TabIndex = 4;
            this.chartAcquisition.Text = "chart1";
            title10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title10.Name = "Title1";
            title10.Text = "Acquired Data vs Time";
            this.chartAcquisition.Titles.Add(title10);
            this.chartAcquisition.AxisViewChanged += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ViewEventArgs>(this.chartAcquisition_AxisViewChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblTagSerie2);
            this.groupBox2.Controls.Add(this.lblTagSerie1);
            this.groupBox2.Controls.Add(this.btnColor2);
            this.groupBox2.Controls.Add(this.btnColor1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.lblUnitSerie2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lblUnitSerie1);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cboDevice2);
            this.groupBox2.Controls.Add(this.cboDevice1);
            this.groupBox2.Controls.Add(this.cboReadDevice2);
            this.groupBox2.Controls.Add(this.cboReadDevice1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(817, 99);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plot";
            // 
            // lblTagSerie2
            // 
            this.lblTagSerie2.AutoSize = true;
            this.lblTagSerie2.Location = new System.Drawing.Point(55, 55);
            this.lblTagSerie2.Name = "lblTagSerie2";
            this.lblTagSerie2.Size = new System.Drawing.Size(22, 13);
            this.lblTagSerie2.TabIndex = 21;
            this.lblTagSerie2.Text = "(...)";
            // 
            // lblTagSerie1
            // 
            this.lblTagSerie1.AutoSize = true;
            this.lblTagSerie1.Location = new System.Drawing.Point(55, 11);
            this.lblTagSerie1.Name = "lblTagSerie1";
            this.lblTagSerie1.Size = new System.Drawing.Size(22, 13);
            this.lblTagSerie1.TabIndex = 20;
            this.lblTagSerie1.Text = "(...)";
            // 
            // btnColor2
            // 
            this.btnColor2.Location = new System.Drawing.Point(353, 70);
            this.btnColor2.Name = "btnColor2";
            this.btnColor2.Size = new System.Drawing.Size(43, 21);
            this.btnColor2.TabIndex = 19;
            this.btnColor2.Text = "Color";
            this.btnColor2.UseVisualStyleBackColor = true;
            this.btnColor2.Click += new System.EventHandler(this.btnColor2_Click);
            // 
            // btnColor1
            // 
            this.btnColor1.Location = new System.Drawing.Point(353, 29);
            this.btnColor1.Name = "btnColor1";
            this.btnColor1.Size = new System.Drawing.Size(40, 23);
            this.btnColor1.TabIndex = 18;
            this.btnColor1.Text = "Color";
            this.btnColor1.UseVisualStyleBackColor = true;
            this.btnColor1.Click += new System.EventHandler(this.btnColor1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(322, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Unit : ";
            // 
            // lblUnitSerie2
            // 
            this.lblUnitSerie2.AutoSize = true;
            this.lblUnitSerie2.Location = new System.Drawing.Point(354, 55);
            this.lblUnitSerie2.Name = "lblUnitSerie2";
            this.lblUnitSerie2.Size = new System.Drawing.Size(16, 13);
            this.lblUnitSerie2.TabIndex = 16;
            this.lblUnitSerie2.Text = "---";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(322, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Unit : ";
            // 
            // lblUnitSerie1
            // 
            this.lblUnitSerie1.AutoSize = true;
            this.lblUnitSerie1.Location = new System.Drawing.Point(354, 11);
            this.lblUnitSerie1.Name = "lblUnitSerie1";
            this.lblUnitSerie1.Size = new System.Drawing.Size(16, 13);
            this.lblUnitSerie1.TabIndex = 14;
            this.lblUnitSerie1.Text = "---";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Device 2 : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Device 1 : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(202, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "ReadDevice 2 : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "ReadDevice 1 : ";
            // 
            // cboDevice2
            // 
            this.cboDevice2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice2.FormattingEnabled = true;
            this.cboDevice2.Location = new System.Drawing.Point(6, 71);
            this.cboDevice2.Name = "cboDevice2";
            this.cboDevice2.Size = new System.Drawing.Size(142, 21);
            this.cboDevice2.TabIndex = 3;
            this.cboDevice2.SelectedIndexChanged += new System.EventHandler(this.cboDevice2_SelectedIndexChanged);
            // 
            // cboDevice1
            // 
            this.cboDevice1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice1.FormattingEnabled = true;
            this.cboDevice1.Location = new System.Drawing.Point(6, 31);
            this.cboDevice1.Name = "cboDevice1";
            this.cboDevice1.Size = new System.Drawing.Size(142, 21);
            this.cboDevice1.TabIndex = 2;
            this.cboDevice1.SelectedIndexChanged += new System.EventHandler(this.cboDevice1_SelectedIndexChanged);
            // 
            // cboReadDevice2
            // 
            this.cboReadDevice2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReadDevice2.FormattingEnabled = true;
            this.cboReadDevice2.Location = new System.Drawing.Point(205, 71);
            this.cboReadDevice2.Name = "cboReadDevice2";
            this.cboReadDevice2.Size = new System.Drawing.Size(142, 21);
            this.cboReadDevice2.TabIndex = 1;
            this.cboReadDevice2.SelectedIndexChanged += new System.EventHandler(this.cboReadDevice2_SelectedIndexChanged);
            // 
            // cboReadDevice1
            // 
            this.cboReadDevice1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReadDevice1.FormattingEnabled = true;
            this.cboReadDevice1.Location = new System.Drawing.Point(205, 31);
            this.cboReadDevice1.Name = "cboReadDevice1";
            this.cboReadDevice1.Size = new System.Drawing.Size(142, 21);
            this.cboReadDevice1.TabIndex = 0;
            this.cboReadDevice1.SelectedIndexChanged += new System.EventHandler(this.cboReadDevice1_SelectedIndexChanged);
            // 
            // GraphTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox5);
            this.Name = "GraphTabControl";
            this.Size = new System.Drawing.Size(817, 619);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAcquisition)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAcquisition;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboDevice2;
        private System.Windows.Forms.ComboBox cboDevice1;
        private System.Windows.Forms.ComboBox cboReadDevice2;
        private System.Windows.Forms.ComboBox cboReadDevice1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblUnitSerie1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblUnitSerie2;
        private System.Windows.Forms.ColorDialog colorDialogTimeAcq;
        private System.Windows.Forms.Button btnColor2;
        private System.Windows.Forms.Button btnColor1;
        private System.Windows.Forms.Label lblTagSerie2;
        private System.Windows.Forms.Label lblTagSerie1;
    }
}
