﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FieldTest.TimedAcquisitionClass.Class;
using FieldTest.TimedAcquisitionClass.Controls;
using FieldTest.DataClass;


namespace FieldTest.Read_Device.Controls
{
    public partial class TimedAcquisitionControl : UserControl
    {
        private TimedAcquisition timeAcq;
        private int indexSeletedTab;

        private const string NONE = "None";

        public List<GraphTabControl> ListGraphNormal { get; set; } 

        private void fillDeviceListReading()
        {
            this.lstReading.Items.Clear();

            foreach (string device in timeAcq.GetListMappedPort().Keys)
            {
                foreach (string readDevice in timeAcq.GetListReadDevice(device).Keys)
                {
                    ListViewItem lvi = new ListViewItem(device + "-" + readDevice + " (" + timeAcq.GetListReadDevice(device)[readDevice].GetConcreteUnit() + ")");
                    lvi.SubItems.Add(String.Empty);
                    this.lstReading.Items.Add(lvi);
                    this.lstReading.Items[this.lstReading.Items.Count - 1].Name = device + "-" + readDevice;
                }
            }
        }

        private void fillDataListReading(SortedList<string, SortedList<string, Coordinate>> listData)
        {
            foreach (string device in listData.Keys)
            {
                foreach (string readDevice in listData[device].Keys)
                {
                    this.lstReading.Items[device + "-" + readDevice].SubItems[1].Text = Math.Round(listData[device][readDevice].Data, 4).ToString();
                }
            }
                
        }

        public TimedAcquisitionControl()
        {
            this.InitializeComponent();
            this.ListGraphNormal = new List<GraphTabControl>();

            DataPublisher dataPublisher = new DataPublisher();
            dataPublisher.RaiseDataEvent += UpdateListReadingListReading;

            this.timeAcq = new TimedAcquisition(this, dataPublisher);

            this.addTabGraph();
            this.fillDeviceListReading();
            

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            this.clearChart();
            this.timeAcq.StartRead();
            this.btnStart.Enabled = false;
            this.btnStop.Enabled = true;

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            this.timeAcq.StopRead();
           
            this.btnStop.Enabled = false;
            this.btnStart.Enabled = true;
        }

        private void numTimeDelay_ValueChanged(object sender, EventArgs e)
        {
            this.timeAcq.ChangeIntervalBetweenData((double)numTimeDelay.Value);
        }

        private void clearChart()
        {
            foreach (GraphTabControl control in ListGraphNormal)
            {
                
                control.ClearChart();
            }
        }


        private void btnClear_Click(object sender, EventArgs e)
        {
            this.clearChart();
        }

        public void UpdateListReadingListReading(object sender, DataEventArgs e)
        {
            this.lstReading.Invoke((MethodInvoker)(() => this.fillDataListReading(e.ListData)));

        }

        private void cbSave_CheckedChanged(object sender, EventArgs e)
        {
            this.timeAcq.EnableWritting = this.cbSave.Checked;
        }

        private void numSample_ValueChanged(object sender, EventArgs e)
        {
            foreach (GraphTabControl graphNor in ListGraphNormal)
            {
                graphNor.GraphNormal.NumberSample = (int)numSample.Value;
            }
            
        }

        private void cbNew_CheckedChanged(object sender, EventArgs e)
        {
            this.timeAcq.IsNewDaily = cbNew.Checked;
        }

        private void addTab()
        {
            string title = "Graph " + txtName.Text;
            TabPage myTabPage = new TabPage(title);
            GraphTabControl control = new GraphTabControl(this.timeAcq.GetListMappedPort(), this.timeAcq.DataPublisher);
            control.Dock = DockStyle.Fill;
            myTabPage.Controls.Add(control);
            tabCtrlGraph.TabPages.Add(myTabPage);


            this.ListGraphNormal.Add(control);
        }

        private void addTabGraph()
        {
            //Arret des threads rapidement pour ajouter un tab en plein acquisition (Meilleur solution que j'ai trouvé)
            if (!this.btnStart.Enabled)
            {
                this.timeAcq.StopReadWithoutWriter();

                this.addTab();

                this.timeAcq.StartReadWithoutWriter();
            }
            else
            { 
                this.addTab();
            }

            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.addTabGraph();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            //Arret des threads rapidement pour enlever le tab en plein acquisition (Meilleur solution que j'ai trouvé)
            if (!this.btnStart.Enabled)
            {
                this.timeAcq.StopReadWithoutWriter();

                this.ListGraphNormal.RemoveAt(indexSeletedTab);
                this.tabCtrlGraph.TabPages.RemoveAt(indexSeletedTab);

                this.timeAcq.StartReadWithoutWriter();
            }
            else
            {
                this.ListGraphNormal.RemoveAt(indexSeletedTab);
                this.tabCtrlGraph.TabPages.RemoveAt(indexSeletedTab); 
            }
        }

        private void tabCtrlGraph_Selected(object sender, TabControlEventArgs e)
        {
            indexSeletedTab = e.TabPageIndex;
        }

        private void tabCtrlGraph_ControlRemoved(object sender, ControlEventArgs e)
        {
            //L'event se déclenche avant la suppression du tab d'où le -1
            if (this.tabCtrlGraph.TabCount - 1 == 1)
                this.btnRemove.Enabled = false;
            else
                this.btnRemove.Enabled = true;
        }

        private void tabCtrlGraph_ControlAdded(object sender, ControlEventArgs e)
        {
            //L'event se déclenche avant l'ajout du tab d'où le +1
            if (this.tabCtrlGraph.TabCount + 1 > 1)
                this.btnRemove.Enabled = true;
            else
                this.btnRemove.Enabled = false;
        }

    }
}
