﻿namespace FieldTest.Read_Device.Controls
{
    partial class TimedAcquisitionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimedAcquisitionControl));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstReading = new System.Windows.Forms.ListView();
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnClear = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numSample = new System.Windows.Forms.NumericUpDown();
            this.numTimeDelay = new System.Windows.Forms.NumericUpDown();
            this.cbNew = new System.Windows.Forms.CheckBox();
            this.cbSave = new System.Windows.Forms.CheckBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tabCtrlGraph = new System.Windows.Forms.TabControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.lstReading);
            this.groupBox3.Location = new System.Drawing.Point(6, 310);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(297, 251);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reading";
            // 
            // lstReading
            // 
            this.lstReading.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colType,
            this.colValue});
            this.lstReading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstReading.FullRowSelect = true;
            this.lstReading.GridLines = true;
            this.lstReading.Location = new System.Drawing.Point(3, 16);
            this.lstReading.MultiSelect = false;
            this.lstReading.Name = "lstReading";
            this.lstReading.Size = new System.Drawing.Size(291, 232);
            this.lstReading.TabIndex = 0;
            this.lstReading.UseCompatibleStateImageBehavior = false;
            this.lstReading.View = System.Windows.Forms.View.Details;
            // 
            // colType
            // 
            this.colType.Text = "read Devices";
            this.colType.Width = 165;
            // 
            // colValue
            // 
            this.colValue.Text = "Values";
            this.colValue.Width = 120;
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClear.BackgroundImage")));
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Location = new System.Drawing.Point(190, 19);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(53, 67);
            this.btnClear.TabIndex = 23;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(158, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Samples on Graph";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Time Delay (sec)";
            // 
            // numSample
            // 
            this.numSample.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numSample.Location = new System.Drawing.Point(161, 148);
            this.numSample.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numSample.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numSample.Name = "numSample";
            this.numSample.Size = new System.Drawing.Size(120, 20);
            this.numSample.TabIndex = 20;
            this.numSample.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSample.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numSample.ValueChanged += new System.EventHandler(this.numSample_ValueChanged);
            // 
            // numTimeDelay
            // 
            this.numTimeDelay.DecimalPlaces = 2;
            this.numTimeDelay.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numTimeDelay.Location = new System.Drawing.Point(18, 148);
            this.numTimeDelay.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numTimeDelay.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numTimeDelay.Name = "numTimeDelay";
            this.numTimeDelay.Size = new System.Drawing.Size(120, 20);
            this.numTimeDelay.TabIndex = 19;
            this.numTimeDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numTimeDelay.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numTimeDelay.ValueChanged += new System.EventHandler(this.numTimeDelay_ValueChanged);
            // 
            // cbNew
            // 
            this.cbNew.AutoSize = true;
            this.cbNew.Location = new System.Drawing.Point(161, 102);
            this.cbNew.Name = "cbNew";
            this.cbNew.Size = new System.Drawing.Size(93, 17);
            this.cbNew.TabIndex = 18;
            this.cbNew.Text = "New Daily File";
            this.cbNew.UseVisualStyleBackColor = true;
            this.cbNew.CheckedChanged += new System.EventHandler(this.cbNew_CheckedChanged);
            // 
            // cbSave
            // 
            this.cbSave.AutoSize = true;
            this.cbSave.Checked = true;
            this.cbSave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSave.Location = new System.Drawing.Point(20, 102);
            this.cbSave.Name = "cbSave";
            this.cbSave.Size = new System.Drawing.Size(77, 17);
            this.cbSave.TabIndex = 17;
            this.cbSave.Text = "Save Data";
            this.cbSave.UseVisualStyleBackColor = true;
            this.cbSave.CheckedChanged += new System.EventHandler(this.cbSave_CheckedChanged);
            // 
            // btnStop
            // 
            this.btnStop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStop.BackgroundImage")));
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Location = new System.Drawing.Point(118, 19);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(53, 67);
            this.btnStop.TabIndex = 16;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStart.BackgroundImage")));
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.Location = new System.Drawing.Point(45, 19);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(53, 67);
            this.btnStart.TabIndex = 15;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemove.BackgroundImage")));
            this.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRemove.Enabled = false;
            this.btnRemove.Location = new System.Drawing.Point(226, 20);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(53, 67);
            this.btnRemove.TabIndex = 25;
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.BackgroundImage")));
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdd.Location = new System.Drawing.Point(150, 19);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(53, 67);
            this.btnAdd.TabIndex = 24;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tabCtrlGraph
            // 
            this.tabCtrlGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlGraph.Location = new System.Drawing.Point(3, 16);
            this.tabCtrlGraph.Name = "tabCtrlGraph";
            this.tabCtrlGraph.SelectedIndex = 0;
            this.tabCtrlGraph.Size = new System.Drawing.Size(715, 545);
            this.tabCtrlGraph.TabIndex = 8;
            this.tabCtrlGraph.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabCtrlGraph_Selected);
            this.tabCtrlGraph.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.tabCtrlGraph_ControlAdded);
            this.tabCtrlGraph.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.tabCtrlGraph_ControlRemoved);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox4.Location = new System.Drawing.Point(721, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(309, 564);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Configs";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnStop);
            this.groupBox7.Controls.Add(this.cbNew);
            this.groupBox7.Controls.Add(this.btnStart);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.cbSave);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.btnClear);
            this.groupBox7.Controls.Add(this.numSample);
            this.groupBox7.Controls.Add(this.numTimeDelay);
            this.groupBox7.Location = new System.Drawing.Point(7, 120);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(293, 184);
            this.groupBox7.TabIndex = 27;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Operations";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.txtName);
            this.groupBox6.Controls.Add(this.btnRemove);
            this.groupBox6.Controls.Add(this.btnAdd);
            this.groupBox6.Location = new System.Drawing.Point(9, 16);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(291, 98);
            this.groupBox6.TabIndex = 26;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tabs Options";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "DeviceName :";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(6, 44);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(130, 20);
            this.txtName.TabIndex = 26;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabCtrlGraph);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(721, 564);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Graphs";
            // 
            // TimedAcquisitionControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Name = "TimedAcquisitionControl";
            this.Size = new System.Drawing.Size(1030, 564);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numSample;
        private System.Windows.Forms.NumericUpDown numTimeDelay;
        private System.Windows.Forms.CheckBox cbNew;
        private System.Windows.Forms.CheckBox cbSave;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lstReading;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colValue;
        private System.Windows.Forms.TabControl tabCtrlGraph;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtName;
    }
}
