﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FieldTest.DevicePortMapClass;
using FieldTest.TimedAcquisitionClass.Class;
using FieldTest.DataClass;

namespace FieldTest.TimedAcquisitionClass.Controls
{
    public partial class GraphTabControl : UserControl
    {
        private double position;
        private double oldMaximum;
        private SortedList<string, ADevicePortMapped> listMappedPort;

        private const string NONE = "None";

        public GraphTab GraphNormal { get; set; }

        public GraphTabControl(SortedList<string, ADevicePortMapped> listMapPort, DataPublisher pub)
        {
            this.InitializeComponent();
            this.GraphNormal = new GraphTab(this);

            this.listMappedPort = listMapPort;

            this.fillCboDevice1();
            this.fillCboDevice2();

            pub.RaiseDataEvent += UpdateSerie1;
            pub.RaiseDataEvent += UpdateSerie2;

        }

        private void fillCboDevice1()
        {
            this.cboDevice1.Items.Add(NONE);
            foreach (string key in listMappedPort.Keys)
                this.cboDevice1.Items.Add(key);

            this.cboDevice1.SelectedIndex = 0;
        }

        private void fillCboDevice2()
        {
            this.cboDevice2.Items.Add(NONE);
            foreach (string key in listMappedPort.Keys)
                this.cboDevice2.Items.Add(key);

            this.cboDevice2.SelectedIndex = 0;
        }

        private void cboDevice1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboReadDevice1.Items.Clear();

            this.cboReadDevice1.Items.Add(NONE);

            if (cboDevice1.SelectedItem.ToString() != NONE)
            {
                foreach (string key in listMappedPort[cboDevice1.SelectedItem.ToString()].ListDeviceReading.Keys)
                    this.cboReadDevice1.Items.Add(key);

                this.lblTagSerie1.Text = "(" + this.listMappedPort[cboDevice1.SelectedItem.ToString()].DeviceDescription + ")";
            }
            else
            {
                this.lblTagSerie1.Text = "(---)";
            }

            this.cboReadDevice1.SelectedIndex = 0;
        }

        private void cboDevice2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboReadDevice2.Items.Clear();

            this.cboReadDevice2.Items.Add(NONE);


            if (cboDevice2.SelectedItem.ToString() != NONE)
            {
                foreach (string key in listMappedPort[cboDevice2.SelectedItem.ToString()].ListDeviceReading.Keys)
                    this.cboReadDevice2.Items.Add(key);

                this.lblTagSerie2.Text = "(" + this.listMappedPort[cboDevice2.SelectedItem.ToString()].DeviceDescription + ")";
            }
            else
            {
                this.lblTagSerie2.Text = "(---)";
            }

            this.cboReadDevice2.SelectedIndex = 0;
        }

        private void cboReadDevice1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GraphNormal.CountDataSerie1 = 0;
            this.GraphNormal.SetPlot1(this.cboDevice1.SelectedItem.ToString(), this.cboReadDevice1.SelectedItem.ToString());

            this.chartAcquisition.Series[0].Points.Clear();
            this.chartAcquisition.Series[0].LegendText = this.cboDevice1.SelectedItem.ToString() + "-" + this.cboReadDevice1.SelectedItem.ToString();

            if (cboReadDevice1.SelectedItem.ToString() != NONE)
            {
                this.chartAcquisition.ChartAreas[0].Axes[1].Title = "Data (" + this.GraphNormal.GetReadDevice(cboDevice1.SelectedItem.ToString(), cboReadDevice1.SelectedItem.ToString()).GetConcreteUnit() + ")";
                this.lblUnitSerie1.Text = this.GraphNormal.GetReadDevice(cboDevice1.SelectedItem.ToString(), cboReadDevice1.SelectedItem.ToString()).GetConcreteUnit();
            }
            else
            {
                this.lblUnitSerie1.Text = "---";
            }
        }

        private void cboReadDevice2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GraphNormal.CountDataSerie2 = 0;
            this.GraphNormal.SetPlot2(this.cboDevice2.SelectedItem.ToString(), this.cboReadDevice2.SelectedItem.ToString());
            this.chartAcquisition.Series[1].Points.Clear();
            this.chartAcquisition.Series[1].LegendText = this.cboDevice2.SelectedItem.ToString() + "-" + this.cboReadDevice2.SelectedItem.ToString();

            if (cboReadDevice2.SelectedItem.ToString() != NONE)
            {
                this.chartAcquisition.ChartAreas[0].Axes[3].Title = "Data (" + this.GraphNormal.GetReadDevice(cboDevice2.SelectedItem.ToString(), cboReadDevice2.SelectedItem.ToString()).GetConcreteUnit() + ")";
                this.lblUnitSerie2.Text = this.GraphNormal.GetReadDevice(cboDevice2.SelectedItem.ToString(), cboReadDevice2.SelectedItem.ToString()).GetConcreteUnit();
            }
            else
            {
                this.lblUnitSerie2.Text = "---";
            }
        }

        public void ClearChart()
        {
            this.GraphNormal.CountDataSerie1 = 0;
            this.GraphNormal.CountDataSerie2 = 0;
            this.chartAcquisition.Series[0].Points.Clear();
            this.chartAcquisition.Series[1].Points.Clear();
        }

        private void chartAcquisition_AxisViewChanged(object sender, System.Windows.Forms.DataVisualization.Charting.ViewEventArgs e)
        {
            //Permet de garder seulement le min et le max du zoom en x (L'event se déclenche aussi en y)
            if (e.Axis.AxisName == System.Windows.Forms.DataVisualization.Charting.AxisName.X)
            {
                position = e.Axis.ScaleView.Position;
                oldMaximum = e.Axis.ScaleView.ViewMaximum;
            }
        }

        public void UpdateSerie1(object sender, DataEventArgs e)
        {

            if (GraphNormal.Serie1 != null && e.ListData.ContainsKey(this.GraphNormal.Serie1.TagDevice) && e.ListData[this.GraphNormal.Serie1.TagDevice].ContainsKey(this.GraphNormal.Serie1.TagReadDevice))
            {
                //Check si la limite de points est arrivé sur le graph et remove les points de trop
                GraphNormal.CountDataSerie1++;

                if (GraphNormal.IsLimitSampleSerie1())
                {
                    int countToRemove = GraphNormal.GetCountToRemoveSerie1();
                    for (int i = 0; i < countToRemove; i++)
                        this.chartAcquisition.Invoke((MethodInvoker)(() => this.removeSerie1()));
                }

              
                this.chartAcquisition.Invoke((MethodInvoker)(() => this.addPointSerie1(e.ListData)));
                this.chartAcquisition.Invoke((MethodInvoker)(() => this.changeScaleSerie1()));
            }
        }

        //Fonction permettant de garder le même scale tout en acquisitionnant des données
        private void changeScaleSerie1()
        {
            if (GraphNormal.Serie1 != null)
            {
                double scale = this.chartAcquisition.ChartAreas[0].AxisX.ScaleView.ViewMaximum - oldMaximum;

                if (oldMaximum != 0)
                    this.chartAcquisition.ChartAreas[0].AxisX.ScaleView.Zoom(position + scale, this.chartAcquisition.ChartAreas[0].AxisX.ScaleView.ViewMaximum);
            }
        }

        //Fonction permettant de garder le même scale tout en acquisitionnant des données
        private void changeScaleSerie2()
        {
            if (GraphNormal.Serie2 != null)
            {
                double scale = this.chartAcquisition.ChartAreas[0].AxisX.ScaleView.ViewMaximum - oldMaximum;

                if (oldMaximum != 0)
                    this.chartAcquisition.ChartAreas[0].AxisX.ScaleView.Zoom(position + scale, this.chartAcquisition.ChartAreas[0].AxisX.ScaleView.ViewMaximum);
            }
        }

         private void removeSerie1()
         {
            if (GraphNormal.Serie1 != null)
            {
                this.chartAcquisition.Series[0].Points.RemoveAt(0);
            }
         }

         private void removeSerie2()
         {
             if (GraphNormal.Serie2 != null)
             {
                 this.chartAcquisition.Series[1].Points.RemoveAt(0);
             }
         }

        private void addPointSerie1(SortedList<string, SortedList<string, Coordinate>> listAllData)
        {
            if (GraphNormal.Serie1 != null)
            {
                this.chartAcquisition.Series[0].Points.AddXY(listAllData[GraphNormal.Serie1.TagDevice][GraphNormal.Serie1.TagReadDevice].RunningTime, listAllData[GraphNormal.Serie1.TagDevice][GraphNormal.Serie1.TagReadDevice].Data);
            }
        }

        private void addPointSerie2(SortedList<string, SortedList<string, Coordinate>> listAllData)
        {
            if (GraphNormal.Serie2 != null)
            {
                this.chartAcquisition.Series[1].Points.AddXY(listAllData[GraphNormal.Serie2.TagDevice][GraphNormal.Serie2.TagReadDevice].RunningTime, listAllData[GraphNormal.Serie2.TagDevice][GraphNormal.Serie2.TagReadDevice].Data);
            }
        }

        public void UpdateSerie2(object sender, DataEventArgs e)
        {

            if (GraphNormal.Serie2 != null && e.ListData.ContainsKey(this.GraphNormal.Serie2.TagDevice) && e.ListData[this.GraphNormal.Serie2.TagDevice].ContainsKey(this.GraphNormal.Serie2.TagReadDevice))
            {
                //Check si la limite de points est arrivé sur le graph et remove les points de trop
                GraphNormal.CountDataSerie2++;

                if (GraphNormal.IsLimitSampleSerie2())
                {
                    int countToRemove = GraphNormal.GetCountToRemoveSerie2();
                    for (int i = 0; i < countToRemove; i++)
                        this.chartAcquisition.Invoke((MethodInvoker)(() => this.removeSerie2()));
                }

                this.chartAcquisition.Invoke((MethodInvoker)(() => this.addPointSerie2(e.ListData)));
                this.chartAcquisition.Invoke((MethodInvoker)(() => this.changeScaleSerie2()));
            }
        }

        private void btnColor1_Click(object sender, EventArgs e)
        {
            if (this.colorDialogTimeAcq.ShowDialog() == DialogResult.OK)
            {
                this.chartAcquisition.Series[0].Color = this.colorDialogTimeAcq.Color;
            } 
        }

        private void btnColor2_Click(object sender, EventArgs e)
        {
            if (this.colorDialogTimeAcq.ShowDialog() == DialogResult.OK)
            {
                this.chartAcquisition.Series[1].Color = this.colorDialogTimeAcq.Color;
            } 
        }

    }
}
