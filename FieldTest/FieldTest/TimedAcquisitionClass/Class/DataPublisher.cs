﻿using FieldTest.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.TimedAcquisitionClass.Class
{
    public class DataPublisher
    {

        public event EventHandler<DataEventArgs> RaiseDataEvent;

        public DataPublisher()
        {
        }

        public void PublishData(SortedList<string, SortedList<string, Coordinate>> listAllData)
        {
            onRaiseDataEvent(new DataEventArgs(listAllData));
        }

        protected virtual void onRaiseDataEvent(DataEventArgs e)
        {
            RaiseDataEvent?.Invoke(this, e);
        }
    }
}
