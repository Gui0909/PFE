﻿
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.MainClass.Class;
using FieldTest.Read_Device.Controls;
using FieldTest.TimedAcquisitionClass.Controls;
using FieldTest.WriterClass;
using System;
using System.Collections.Generic;
using System.Timers;

namespace FieldTest.TimedAcquisitionClass.Class
{
    public class TimedAcquisition
    {
        private Timer ticker;
        private WriterNormalData writer;
        private DateTime lastTimeData;
        private bool isFirstTime;

        public bool EnableWritting { get; set;}
        public bool IsNewDaily { get; set; }
        public DataPublisher DataPublisher { get; set; }

        private const string NONE = "None";
        private const double DEFAULT_INTERVAL = 50;

        public TimedAcquisition(TimedAcquisitionControl controlA, DataPublisher datapublisher)
        {
            ticker = new Timer(DEFAULT_INTERVAL);
            ticker.Elapsed += new ElapsedEventHandler(Read);
            EnableWritting = true;
            IsNewDaily = false;

            this.DataPublisher = datapublisher;
            lastTimeData = DateTime.Now;
        }

        public SortedList<string, ADevicePortMapped> GetListMappedPort()
        {
            return ApplicationManager.Instance.ListPortMapping;
        }

        public SortedList<string, ADeviceReading> GetListReadDevice (string device)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading;
        }

        public void StopReadWithoutWriter()
        {
            ticker.Stop();

            ApplicationManager.Instance.StopReadThread();
        }

        public void StopRead()
        {

            ticker.Stop();

            if (writer != null)
                writer.StopWritting();

            ApplicationManager.Instance.StopReadThread();
        }

        public void StartReadWithoutWriter()
        {
            ApplicationManager.Instance.StartReadThread();

            ticker.Start();
        }

        public void StartRead()
        {

            ApplicationManager.Instance.StartReadThread();

            isFirstTime = true;

            if (EnableWritting)
                writer = new WriterNormalData(ApplicationManager.Instance.ListPortMapping, ApplicationManager.Instance.FileSetting, Acquisition.TYPE.NORMAL);
            else
                writer = null;

            ticker.Start();

        }

        private void Read(object sender, ElapsedEventArgs e)
        {
     
            SortedList<string, SortedList<string, Coordinate>> listAllData = ApplicationManager.Instance.ExtractAllData();
           
            this.DataPublisher.PublishData(listAllData);
                  

            if (isFirstTime)
            {
                
                if (listAllData.Count != 0)
                {
                    lastTimeData = listAllData.Values[0].Values[0].RunningTime;
                    isFirstTime = false;
                }
            }

            if (writer != null && IsNewDaily && listAllData.Count != 0 && listAllData.Values[0].Values[0].RunningTime.Day != lastTimeData.Day)
            {
                writer = new WriterNormalData(ApplicationManager.Instance.ListPortMapping, ApplicationManager.Instance.FileSetting, Acquisition.TYPE.NORMAL);
                lastTimeData = listAllData.Values[0].Values[0].RunningTime;
            }

            writer.WriteLine(listAllData);
         
        }

        public void ChangeIntervalBetweenData(double timeIntervalle)
        {
            this.ticker.Interval = timeIntervalle * 1000;
        }

    }
}
