﻿
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.MainClass.Class;
using FieldTest.TimedAcquisitionClass.Controls;
using System;
using System.Collections.Generic;

namespace FieldTest.TimedAcquisitionClass.Class
{
    public class GraphTab
    {
        public Plot Serie1 { get; set; }
        public Plot Serie2 { get; set; }

        private const string NONE = "None";

        public int NumberSample { get; set; }
        public int CountDataSerie1 { get; set; }
        public int CountDataSerie2 { get; set; }

        public GraphTab(GraphTabControl controlNormal)
        {
            NumberSample = 100;

        }

        public void SetPlot1(string device, string readDevice)
        {
            if (readDevice != NONE)
                Serie1 = new Plot(device, readDevice);
            else
                Serie1 = null;
        }

        public void SetPlot2(string device, string readDevice)
        {
            if (readDevice != NONE)
                Serie2 = new Plot(device, readDevice);
            else
                Serie2 = null;
        }

        public SortedList<string, ADeviceReading> GetListReadDevice(string device)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading;
        }

        public ADeviceReading GetReadDevice(string device, string readDevice)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading[readDevice];
        }

        //Retourne le nombre de donnée à enlever sur le graph
        public int GetCountToRemoveSerie1()
        {
            int dataToRemove = CountDataSerie1 - NumberSample;
            CountDataSerie1 = NumberSample;

            return dataToRemove;
        }

        //Retourne le nombre de donnée à enlever sur le graph
        public int GetCountToRemoveSerie2()
        {
            int dataToRemove = CountDataSerie2 - NumberSample;
            CountDataSerie2 = NumberSample;

            return dataToRemove;
        }

        //Vérifie si la limite de donnée est dépassé sur le graph
        public bool IsLimitSampleSerie1()
        {
            if (CountDataSerie1 > NumberSample)
            {
                return true;
            }
            else
                return false;
        }

        //Vérifie si la limite de donnée est dépassé sur le graph
        public bool IsLimitSampleSerie2()
        {
            if (CountDataSerie2 > NumberSample)
            {
                return true;
            }
            else
                return false;
        }


//public void PublishData(SortedList<string, SortedList<string, Coordinate>> listAllData)
        //
           // NotifyPlots(new DataEventArgs(listAllData));
            //NotifyPlot2(new DataEventArgs(listAllData));

//}
        
       // public void NotifyPlots(DataEventArgs e)
        //{
            //RaiseDataEvent?.Invoke(this, e);
        //

       // public void NotifyPlot2(DataEventArgs e)
        //{
            //if (this.Serie2 != null)
                //RaiseDataEvent?.Invoke(this, e);
        //}

    }
}
