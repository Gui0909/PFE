﻿using FieldTest.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.TimedAcquisitionClass.Class
{
   
    public class DataEventArgs : EventArgs
    {

        private SortedList<string, SortedList<string, Coordinate>> listData;

        public DataEventArgs(SortedList<string, SortedList<string, Coordinate>> listAllData)
        {
            this.listData = listAllData;
        }

        public SortedList<string, SortedList<string, Coordinate>> ListData
        {
            get { return listData; }
            set { listData = value; }
        }


    }
   

}
