﻿
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.XMLConfigClass;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FieldTest.WriterClass
{
    public class WriterHighData
    {
        private StreamWriter file;
        private const string TAB = "\t";

        //Construteur pour l'acquisition haute fréquence de données
        public WriterHighData(SortedList<string, ADevicePortMapped> listPort, FileSetting setting, Acquisition.TYPE type)
        {
            int numFile = 1;

            //Check si le numero du nom du fichier existe
            while (File.Exists(setting.FolderPath + @"\" + setting.HighAcquisition + "_" + numFile + ".txt"))
            {
                numFile++;
            }

            file = new StreamWriter(setting.FolderPath + @"\" + setting.HighAcquisition + "_" + numFile + ".txt");
            writeHighHead(listPort, type);

        }

        //Construteur pour l'exportation
        public WriterHighData(SortedList<string, List<Coordinate>> listData, SortedList<string, string> listUnit, SortedList<string, string> listTag, string path, Acquisition.TYPE type)
        {

            file = new StreamWriter(path);
            writeHighHead(listData, listUnit, listTag, type);
        }

        //Écriture de l'entête lors de l'acquisition haute fréquence de données
        private void writeHighHead(SortedList<string, ADevicePortMapped> listPort, Acquisition.TYPE type)
        {
            string line1 = type.ToString();
            string line2 = string.Empty;
            string line3 = " " + TAB + " ";
            string line4 = TAB + TAB + TAB;
            string line5 = "Time" + TAB + "Frequence";

            foreach (ADevicePortMapped port in listPort.Values)
            {
                line2 += TAB + "(" + port.DeviceDescription + ")" + "-" + port.DeviceName;

                foreach (ADeviceReading readDevice in port.ListDeviceReading.Values)
                {
                    if (readDevice.IsEnableHighAcquisition)
                    {
                        line3 += TAB + port.DeviceName + "-" + readDevice.Name;
                        line4 += TAB + readDevice.GetConcreteUnit();
                        line5 += TAB + "Y" + TAB + "Amplitude";
                    }
                }
            }

            file.WriteLine(line1);
            file.WriteLine(line2);
            file.WriteLine(line3);
            file.WriteLine(line4);
            file.WriteLine(line5);

        }

        //Écriture de l'entête lors de l'exportation de données dans le menu de l'analyse
        private void writeHighHead(SortedList<string, List<Coordinate>> listData, SortedList<string, string> listUnit, SortedList<string, string> listTag, Acquisition.TYPE type)
        {
            string line1 = type.ToString();
            string line2 = string.Empty;
            string line3 = type.ToString() + TAB + " ";
            string line4 = TAB + TAB + TAB;
            string line5 = "Time" + TAB + "Frequence";

            foreach (string tag in listTag.Keys)
            {
                line2 += TAB + listTag[tag] + "-" + tag;
            }

            foreach (string device in listData.Keys)
            {

                line3 += TAB + device;
                line4 += TAB + listUnit[device];
                line5 += TAB + "Y" + TAB + "Amplitude";

            }

            file.WriteLine(line1);
            file.WriteLine(line2);
            file.WriteLine(line3);
            file.WriteLine(line4);
            file.WriteLine(line5);

        }

        //Fonction d'écriture appeler lors de l'exportation (Gestion du zoom)
        public void WriteHigh(SortedList<string, List<Coordinate>> listDataNormal, SortedList<string, List<Coordinate>> listHighData)
        {
            int count = 0;

            //Définit le count selon la plus grosse plage de donnée sélectionné entre (amp vs freq) et (data vs time)
            if (listDataNormal.ElementAt(0).Value.Count >= listHighData.ElementAt(0).Value.Count)
            {
                count = listDataNormal.ElementAt(0).Value.Count;
            }
            else
            {
                count = listHighData.ElementAt(0).Value.Count;
            }

            for (int i = 0; i < count; i++)
            {
                string line = string.Empty;
                bool first = true;

                foreach (string readDevice in listDataNormal.Keys)
                {

                    //Écrit la fréquence et le time pour la ligne une seule fois
                    if (first)
                    {
                        if (i < listDataNormal.ElementAt(0).Value.Count)
                        {
                            line += listDataNormal[readDevice].ElementAt(i).TimeStamp;
                        }
                        else
                            line += "--";

                        if (i < listHighData.ElementAt(0).Value.Count)
                        {
                            line += TAB + listHighData[readDevice].ElementAt(i).Frequency;
                        }
                        else
                            line += TAB + "--";

                        first = false;

                    }

                    if (i < listDataNormal.ElementAt(0).Value.Count)
                        line += TAB + listDataNormal[readDevice].ElementAt(i).Data;
                    else
                        line += TAB + "--";

                    if (i < listHighData.ElementAt(0).Value.Count)
                        line += TAB + listHighData[readDevice].ElementAt(i).Amplitude;
                    else
                        line += TAB + "--";
                }

                file.WriteLine(line);
            }

        }

        //Fonction d'écriture lors de l'acquisition de donnée haute fréquence
        public void WriteLine(SortedList<string, SortedList<string, Coordinate>> listData)
        {

            string line = string.Empty;
            bool first = true;

            foreach (string device in listData.Keys)
            {
                foreach (string readDevice in listData[device].Keys)
                {
                    //Écrit la fréquence et le time pour la ligne une seule fois
                    if (first)
                    {
                     
                        line += listData[device][readDevice].TimeStamp;

                        if (!double.IsNaN(listData[device][readDevice].Frequency))
                        {
                            line += TAB + listData[device][readDevice].Frequency;
                        }
                        else
                        {
                            line += TAB + "--";
                        }

                        first = false;

                    }

                    line += TAB + listData[device][readDevice].Data.ToString();

                    if (!double.IsNaN(listData[device][readDevice].Amplitude))
                    {
                        line += TAB + listData[device][readDevice].Amplitude.ToString();
                    }
                    else
                    {
                        line += TAB + "--";
                    }
                }
            }

            file.WriteLine(line);

        }

        public void StopWritting()
        {
            file.Close();
        }

    }
}
