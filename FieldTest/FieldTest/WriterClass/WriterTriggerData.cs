﻿using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.WriterClass
{
    public class WriterTriggerData
    {
        private StreamWriter file;
        private const string TAB = "\t";
        private bool isFirstTemp;

        public bool IsWritting { get; set; }
        public int NumberLine { get; set; }

        //Écrit les données avant l'erreur produite
        private void setLastData(List<Coordinate> listLastAllData)
        {
            foreach (Coordinate coord in listLastAllData)
            {
                string line = string.Empty;
                bool first = true;

                if (first)
                {
                    line += coord.RunningTime.ToString("HH:mm:ss.fff");

                    first = false;
                }

                line += TAB + coord.Data;

                if (IsWritting)
                    file.WriteLine(line);
            }

        }

        private void setFile(FileSetting setting, string device, string tag, string unit, double threshold)
        {

            file = new System.IO.StreamWriter(setting.FolderPath + @"\" + "Log_Trigger" + "_" + device + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + ".txt");

            IsWritting = true;
            this.NumberLine = 0;

            writeHead(device, tag, unit, threshold);
        }

        public WriterTriggerData()
        {
            this.IsWritting = false;
            this.NumberLine = 0;
            this.isFirstTemp = true;
        }

        //Écrit l'entête
        private void writeHead(string key, string tag, string unit, double threshold)
        {
            string line0 = "Trigger";
            string line1 = " " + TAB + " " + TAB + "(" + tag + ")" + "-" + key.Split('-')[0];
            string line2 = " " + TAB + "Threshold : " + threshold;
            string line3 = "Time" + TAB + " ";

            line2 += TAB + key;
            line3 += TAB + unit;

            file.WriteLine(line0);
            file.WriteLine(line1);
            file.WriteLine(line2);
            file.WriteLine(line3);

        }

        //Écrit la ligne représentant l'erreur
        public void WriteSeuilLine(FileSetting setting, string key, string tag, string unit, List<Coordinate> listLastAllData, Coordinate coord, double threshold)
        {
            string line = string.Empty;

            this.NumberLine = 0;

            if (isFirstTemp)
            {
                this.setFile(setting, key, tag, unit, threshold);
                isFirstTemp = false;
            }

            this.setLastData(listLastAllData);

            line += coord.RunningTime.ToString("HH:mm:ss.fff");
            line += TAB + coord.Data + " > " + threshold;

            if (IsWritting)
                file.WriteLine(line);
        }

        //Écrit une ligne normal de data
        public void WriteDataLine(Coordinate coord)
        {

            string line = string.Empty;

            line += coord.RunningTime.ToString("HH:mm:ss.fff");

            line += TAB + coord.Data;

            if (IsWritting)
                file.WriteLine(line);

            this.NumberLine++;

        }

        public void StopWritting()
        {
            IsWritting = false;
            file.Close();
        }
    }
}
