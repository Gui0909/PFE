﻿
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FieldTest.WriterClass
{
    public class WriterNormalData
    {
        private StreamWriter file;

        private long xTemp = 0;
        private double xSecondTemp = 0;
        private bool isFirstTemp;
        private const string TAB = "\t";

        public bool IsWritting { get; set; }

        //Construteur pour l'acquisition de données
        public WriterNormalData(SortedList<string, ADevicePortMapped> listPort, FileSetting setting, Acquisition.TYPE type)
        {
            int numFile = 1;

            //Check si le numero du nom du fichier existe
            while (File.Exists(setting.FolderPath + @"\" + setting.TimeAcquisition + "_" + numFile + ".txt"))
            {
                numFile++;
            }

            file = new StreamWriter(setting.FolderPath + @"\" + setting.TimeAcquisition + "_" + numFile + ".txt");
            writeHead(listPort, type);

            isFirstTemp = true;
            IsWritting = true;
            xSecondTemp = 0;
            xTemp = 0;

        }

        //Construteur pour l'exportation
        public WriterNormalData(SortedList<string, List<Coordinate>> listData, SortedList<string, string> listUnit, SortedList<string, string> listTag, string path, Acquisition.TYPE type)
        {
           
            file = new StreamWriter(path);
            writeHead(listData, listUnit, listTag, type);
      
            isFirstTemp = true;
            xSecondTemp = 0;
            xTemp = 0;

        }

        //Écriture de l'entête lors de l'exportation de données dans le menu de l'analyse
        private void writeHead(SortedList<string, List<Coordinate>> listData, SortedList<string, string> listUnit, SortedList<string, string> listTag, Acquisition.TYPE type)
        {
            string line1 = type.ToString();
            string line2 = string.Empty;
            string line3 = " " + TAB + " ";
            string line4 = "Time" + TAB + "Interval";

            foreach (string tag in listTag.Keys)
            {
                line2 += TAB + listTag[tag] + "-" + tag;
            }

            foreach (string device in listData.Keys)
            {

                line3 += TAB + device;
                line4 += TAB + listUnit[device].ToString();
            
            }

            file.WriteLine(line1);
            file.WriteLine(line2);
            file.WriteLine(line3);
            file.WriteLine(line4);
        }

        //Écriture de l'entête lors de l'acquisition de données
        private void writeHead(SortedList<string, ADevicePortMapped> listPort, Acquisition.TYPE type)
        {
            string line1 = type.ToString();
            string line2 = string.Empty;
            string line3 = " " + TAB + " ";
            string line4 = "Time" + TAB + "Interval";

            foreach (ADevicePortMapped port in listPort.Values)
            {
                line2 += TAB + "(" + port.DeviceDescription + ")" + "-" + port.DeviceName;
 
                foreach (ADeviceReading device in port.ListDeviceReading.Values)
                {
                    line3 += TAB + port.DeviceName + "-" + device.Name;
                    line4 += TAB + device.GetConcreteUnit().ToString();

                }
            }

            file.WriteLine(line1);
            file.WriteLine(line2);
            file.WriteLine(line3);
            file.WriteLine(line4);
        }

        //Fonction d'écriture appeler lors de l'exportation
        public void WriteNormal(SortedList<string, List<Coordinate>> listData)
        {
            
            for (int i = 0; i < listData.ElementAt(0).Value.Count; i++)
            {
                bool first = true;
                string line = string.Empty;

                foreach (string readDevice in listData.Keys)
                {

                    //Pour setter la premier valeur pour le calcul de l'interval
                    if (isFirstTemp)
                    {
                        xTemp = listData[readDevice].ElementAt(i).RunningTime.ToFileTime();
                        isFirstTemp = false;
                    }

                    //Écrit le time et l'interval une fois par ligne seulement
                    if (first)
                    {
                        xSecondTemp += (DateTime.FromFileTime(listData[readDevice].ElementAt(i).RunningTime.ToFileTime()) - DateTime.FromFileTime(xTemp)).TotalMilliseconds;
                        line += listData[readDevice].ElementAt(i).RunningTime.ToString("HH:mm:ss.fff") + TAB + (xSecondTemp / 1000);
                        first = false;

                    }

                    line += TAB + listData[readDevice].ElementAt(i).Data.ToString();
                
                    xTemp = listData[readDevice].ElementAt(i).RunningTime.ToFileTime();

                }

                file.WriteLine(line);
            }

        }

        //Fonction d'écriture lors de l'acquisition de donnée
        public void WriteLine(SortedList<string, SortedList<string, Coordinate>> listData)
        {
            string line = string.Empty;
            bool first = true;

            foreach (string device in listData.Keys)
            {
                foreach (string readDevice in listData[device].Keys)
                {
                    //Pour setter la premier valeur pour le calcul de l'interval
                    if (isFirstTemp)
                    {
                        xTemp = listData[device][readDevice].RunningTime.ToFileTime();
                        isFirstTemp = false;
                    }

                    //Écrit le time et l'interval une fois par ligne seulement
                    if (first)
                    {
                        xSecondTemp += (DateTime.FromFileTime(listData[device][readDevice].RunningTime.ToFileTime()) - DateTime.FromFileTime(xTemp)).TotalMilliseconds;
                        line += listData[device][readDevice].RunningTime.ToString("HH:mm:ss.fff") + TAB + (xSecondTemp / 1000);
                        first = false;
                    }

                    line += TAB + listData[device][readDevice].Data.ToString();
                 
                    xTemp = listData[device][readDevice].RunningTime.ToFileTime();
                }
            }

            if (IsWritting)
                file.WriteLine(line);
        }

      
        public void StopWritting()
        {
            IsWritting = false;
            file.Close();
        }

    }
}
