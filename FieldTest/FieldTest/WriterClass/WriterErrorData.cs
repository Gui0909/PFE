﻿
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO;

namespace FieldTest.WriterClass
{
    public class WriterErrorData
    {
        private StreamWriter file;
        private const string TAB = "\t";
        private bool isFirstTemp;

        public bool IsWritting { get; set; }
        public int NumberLine { get; set; }

        //Écrit les données avant l'erreur produite
        private void setLastData(ADevicePortMapped portMap, List<SortedList<string, SortedList<string, Coordinate>>> listLastAllData)
        {
           
            for (int i = 0; i < listLastAllData.Count; i++)
            {
                string line = string.Empty;
                bool first = true;

                foreach (Coordinate coord in listLastAllData[i][portMap.DeviceName].Values)
                {
                    
                    if (first)
                    {
                        line += coord.RunningTime.ToString("HH:mm:ss.fff") + TAB;

                        first = false;
                    }

                    line += TAB + coord.Data;

                }

                if (IsWritting)
                    file.WriteLine(line);
            }
        
        }

        private void setFile(FileSetting setting, ADevicePortMapped portMap)
        {
            string path = setting.FolderPath + @"\" + "Log_Error" + "_" + portMap.DeviceName + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + ".txt";
            file = new System.IO.StreamWriter(path);

            IsWritting = true;

            this.NumberLine = 0;

            writeHead(portMap);
        }

        public WriterErrorData()
        {
            this.IsWritting = false;
            this.NumberLine = 0;
            this.isFirstTemp = true;
        }

        //Écrit l'entête
        private void writeHead(ADevicePortMapped portMap)
        {
            string line0 = "Error";
            string line1 = " " + TAB + " " + TAB + "(" + portMap.DeviceDescription + ")" + "-" + portMap.DeviceName;
            string line2 = " " + TAB + "Error";
            string line3 = "Time" + TAB + " ";

            foreach (ADeviceReading readDevice in portMap.ListDeviceReading.Values)
            {
                line2 += TAB + portMap.DeviceName + "-" + readDevice.Name;
                line3 += TAB + readDevice.GetConcreteUnit();
            }

            file.WriteLine(line0); 
            file.WriteLine(line1);
            file.WriteLine(line2);
            file.WriteLine(line3);
        }

        //Écrit la ligne représentant l'erreur
        public void WriteErrorLine(FileSetting setting, ADevicePortMapped portMap, List<SortedList<string, SortedList<string, Coordinate>>> listLastAllData, SortedList<string, Coordinate> listRawData, List<string> listError)
        {
            string line = string.Empty;
            bool first = true;
            this.NumberLine = 0;

            if (isFirstTemp)
            {
                this.setFile(setting, portMap);
                this.isFirstTemp = false;
            }

            this.setLastData(portMap, listLastAllData);

            foreach (Coordinate coord in listRawData.Values)
            {

                if (first)
                {

                    line += coord.RunningTime.ToString("HH:mm:ss.fff") + TAB;

                    foreach (string error in listError)
                    {
                        line += error + "|";
                    }

                    first = false;
                }

                line += TAB + coord.Data;

            }

            if (IsWritting)
                file.WriteLine(line);
        }

        //Écrit une ligne normal de data
        public void WriteDataLine(FileSetting setting, ADevicePortMapped portMap, SortedList<string, Coordinate> listRawData)
        {
            string line = string.Empty;
            bool first = true;

            foreach (Coordinate coord in listRawData.Values)
            {

                if (first)
                {

                    line += coord.RunningTime.ToString("HH:mm:ss.fff") + TAB + " ";

                    first = false;
                }

                line += TAB + coord.Data;

            }

            if (IsWritting)
                file.WriteLine(line);

            this.NumberLine++;
        }

        public void StopWritting()
        {
            IsWritting = false;
            file.Close();
        }
    }
}
