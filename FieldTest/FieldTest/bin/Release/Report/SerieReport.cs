﻿using System.Drawing;

namespace FieldTest.AnalysisClass.Report
{
    public class SerieReport
    {
        public string NameSerie1 { get; set; }
        public string NameSerie2 { get; set; }
        public string TagSerie1 { get; set; }
        public string TagSerie2 { get; set; }
        public Color Color1 { get; set; }
        public Color Color2 { get; set; }

        public SerieReport(string nameSerie1, string tagSerie1, Color color1, string nameSerie2, string tagSerie2, Color color2)
        {
            this.NameSerie1 = nameSerie1;
            this.NameSerie2 = nameSerie2;
            this.TagSerie1 = tagSerie1;
            this.TagSerie2 = tagSerie2;
            this.Color1 = color1;
            this.Color2 = color2;
        }
    }
}
