﻿using System;

namespace FieldTest.UtilitaryByte
{
    //Classe de conversion et traitement sur byte
    static class ByteConverter
    {
        public static byte[] TrimBufferByte(byte[] bufferByte, int offSet, int length)
        {
            byte[] newBufferByte = new byte[length];
            

            for (int i = 0; i < length; i++)
            {
                newBufferByte[i] = bufferByte[offSet];
                offSet++;
            }

            Array.Reverse(newBufferByte);

            return newBufferByte;
        }

        public static byte GetBits(byte bit, int offset)
        {
            byte mask = (byte)(Math.Pow(2, offset));
            return (byte)(((UInt16)(bit & mask)) >> offset);
        }
    }
}
