﻿
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.MainClass.Class;
using FieldTest.XMLConfigClass;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace FieldTest.SetupClass
{
    //Gère les interface de setup et wrap ManagerPortDevice
    public class Setup
    {

        private DeviceSettingManager setting;

        public DeviceSettingManager Setting
        {
            get
            {
                return this.setting;
            }
            set
            {
                this.setting = value;
            }
        }

        public Setup()
        {

        }

        public void SerializerSetting()
        {
            XmlSerializer xmlSerial = new XmlSerializer(setting.GetType());
            StreamWriter streamWriter = new StreamWriter("Setting\\Setting.xml", false);
            xmlSerial.Serialize(streamWriter, setting);
            streamWriter.Close();
            
        }

        public void DeSerializerSetting()
        {
            setting = new DeviceSettingManager();
            StreamReader streamReader = new StreamReader("Setting\\Setting.xml", false);
            XmlSerializer xmlSerial = new XmlSerializer(setting.GetType());
            setting = (DeviceSettingManager)xmlSerial.Deserialize(streamReader);
            streamReader.Close();
        }

        public bool IfPortMapped()
        {
            if (ApplicationManager.Instance.GetCountPortMapped() != 0)
                return true;
            else
                return false;
        }

        public void InitialiserSetup()
        {
            //Creer fichier xml si n'existe pas
            if (!File.Exists("Setting.xml"))
            {
               setting = new DeviceSettingManager(true);
               SerializerSetting();
            }

            DeSerializerSetting();

            //Check if all are close before the mapped driver
            if (ApplicationManager.Instance.ListPortMapping != null && ApplicationManager.Instance.ListPortMapping.Count != 0)
                ApplicationManager.Instance.ClosePortDevice();

            //Check port connecter et map ces derniers
            ApplicationManager.Instance.ListPortMapping = PortScanner.FindAllFTDIDevise(ref setting);
            ApplicationManager.Instance.FileSetting = setting.File;

            //Permet de sérialiser la descritption window des port (COMxxx) et les changement détecté
            SerializerSetting();

            //Ouvre tous les ports et start l'acquisition de donnée en background
            ApplicationManager.Instance.OpenPortDevice();
            ApplicationManager.Instance.StartReadThread();
        }

        public void StartReadThread()
        {
            ApplicationManager.Instance.StartReadThread();
        }

        public void StopReadThread()
        {
            ApplicationManager.Instance.StopReadThread();
        }

        public void UpdateDeviceSetting(string deviceTag)
        {
           
            ApplicationManager.Instance.ListPortMapping[deviceTag].DeviceSetting = setting.GetDeviceSetting(deviceTag);
            ApplicationManager.Instance.ListPortMapping[deviceTag].DeviceDescription = setting.GetDeviceSetting(deviceTag).InstrumentTag;

            SerializerSetting();

        }

        public void UpdateFileSetting()
        {

            ApplicationManager.Instance.FileSetting = setting.File;

            SerializerSetting();

        }

        public void ClosePorts()
        {
            ApplicationManager.Instance.ClosePortDevice();
        }

        public bool IfContainsDevice(string device)
        {
            return ApplicationManager.Instance.IfContainsDevice(device);
        }

        public SortedList<string, SortedList<string, Coordinate>> GetListData()
        {
            return ApplicationManager.Instance.ExtractAllData();
        }


        public SortedList<string, ADevicePortMapped> GetListMappedPort()
        {
            return ApplicationManager.Instance.ListPortMapping;
        }

        public ADevicePortMapped GetPortMapped(string device)
        {
            return ApplicationManager.Instance.ListPortMapping[device];
        }

        public SortedList<string, ADeviceReading> GetListReadDevice(string device)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading;
        }

        public ADeviceReading GetReadDevice(string device, string readDevice)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading[readDevice];
        }

       
    }
}
