﻿using System;
using System.Windows.Forms;
using FieldTest.XMLConfigClass;


namespace FieldTest.SetupClass.Controls
{
    public partial class DeviceSettingControl : UserControl
    {
        public ADeviceSetting Setting { get; set; }
        public bool IsValueChange { get; set; }
        public string TagDevice { get; set; }

        public DeviceSettingControl(ADeviceSetting config, string tag)
        {
            InitializeComponent();
            Setting = config;
            IsValueChange = false;
            this.TagDevice = tag;
        }

        private void ConfigControl_Load(object sender, EventArgs e)
        {
            propGrid.SelectedObject = Setting;
        }

        private void propGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            Setting = (ADeviceSetting)propGrid.SelectedObject;
            IsValueChange = true;
            
        }

    }
}
