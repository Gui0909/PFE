﻿namespace FieldTest.SetupClass.Controls
{
    partial class StatutsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstReading = new System.Windows.Forms.ListView();
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lstReading
            // 
            this.lstReading.BackColor = System.Drawing.Color.White;
            this.lstReading.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colType,
            this.colValue,
            this.columnHeader1});
            this.lstReading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstReading.FullRowSelect = true;
            this.lstReading.Location = new System.Drawing.Point(0, 0);
            this.lstReading.MultiSelect = false;
            this.lstReading.Name = "lstReading";
            this.lstReading.Size = new System.Drawing.Size(734, 556);
            this.lstReading.TabIndex = 1;
            this.lstReading.UseCompatibleStateImageBehavior = false;
            this.lstReading.View = System.Windows.Forms.View.Details;
            // 
            // colType
            // 
            this.colType.Text = "Devices";
            this.colType.Width = 116;
            // 
            // colValue
            // 
            this.colValue.Text = "State";
            this.colValue.Width = 114;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Data received";
            this.columnHeader1.Width = 700;
            // 
            // StatutsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lstReading);
            this.Name = "StatutsControl";
            this.Size = new System.Drawing.Size(734, 556);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstReading;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colValue;
        private System.Windows.Forms.ColumnHeader columnHeader1;

    }
}
