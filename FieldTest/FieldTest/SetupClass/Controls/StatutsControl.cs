﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Windows.Forms;
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.TimedAcquisitionClass.Class;

namespace FieldTest.SetupClass.Controls
{
    public partial class StatutsControl : UserControl
    {
        private Setup setup;
        private System.Timers.Timer ticker;

        private const double DEFAULT_INTERVAL = 500;

        public StatutsControl(Setup set)
        {
            InitializeComponent();

            ticker = new System.Timers.Timer(DEFAULT_INTERVAL);
            ticker.Elapsed += new ElapsedEventHandler(checkTransmittingPort);

            this.setup = set;
            fillStatuts();

            ticker.Enabled = true;

        }

        //Function du threadPortDectecting vérifiant le statut des ports
        private void checkTransmittingPort(object sender, ElapsedEventArgs e)
        {
            //Je laisse le try catch là, car le tt20k va peut-être causer problème sa va aider le debug (Sinon tout est géré, il est donc inutile)
            try
            {
                SortedList<string, SortedList<string, Coordinate>> listData = setup.GetListData();

               foreach (ADevicePortMapped value in setup.GetListMappedPort().Values)
               {
                    //Garde la dernière transmission de donnée
                    string lastData = "---";
                    this.lstReading.Invoke((MethodInvoker)(() => lastData = this.lstReading.Items[value.DeviceName].SubItems[2].Text));
                    this.lstReading.Invoke((MethodInvoker)(() => this.lstReading.Items[value.DeviceName].SubItems[2].Text = string.Empty));

                    //Beacoup de validations de transmission de port et de présence de données
                    if (listData.Count != 0 && setup.IfContainsDevice(value.DeviceName) && setup.GetPortMapped(value.DeviceName).IsTransmittingData() && listData.ContainsKey(value.DeviceName))
                    {
                        this.lstReading.Invoke((MethodInvoker)(() => this.lstReading.Items[value.DeviceName].SubItems[1].Text = "Transmitting News Datas"));

                        foreach (string readDevice in setup.GetListReadDevice(value.DeviceName).Keys)
                        {
                            this.lstReading.Invoke((MethodInvoker)(() => this.lstReading.Items[value.DeviceName].SubItems[2].Text += "(" + readDevice + " : " + listData[value.DeviceName][readDevice].Data.ToString() + ")   "));
                        }
                    }
                    else
                    {
                        //Si ne transmet pas remet la dernière donnée reçue et signifie que le device ne transmet pas
                        this.lstReading.Invoke((MethodInvoker)(() => this.lstReading.Items[value.DeviceName].SubItems[1].Text = "Not Transmitting News Datas"));
                        this.lstReading.Invoke((MethodInvoker)(() => this.lstReading.Items[value.DeviceName].SubItems[2].Text = lastData));
                    }

                    this.lstReading.Invoke((MethodInvoker)(() => this.lstReading.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)));
               }
            }
            catch
            {

            }
        }

        //Function qui set le listView avant l'exécution de la fonction du threadPortDectecting checkTransmittingPort et fait un premier check du statut des port
        private void fillStatuts()
        {
            //Je laisse le try catch là, car le tt20k va peut-être causer problème sa va aider le debug (Sinon tout est géré, il est donc inutile)
            try
            {
                SortedList<string, SortedList<string, Coordinate>> listData = setup.GetListData();

                foreach (ADevicePortMapped value in setup.GetListMappedPort().Values)
                {
                    ListViewItem lvi = new ListViewItem(value.DeviceName);
                    lvi.SubItems.Add(String.Empty);
                    lvi.SubItems.Add(String.Empty);
                    this.lstReading.Items.Add(lvi);
                    this.lstReading.Items[this.lstReading.Items.Count - 1].Name = value.DeviceName;

                    if (listData.Count != 0 && setup.IfContainsDevice(value.DeviceName) && setup.GetPortMapped(value.DeviceName).IsTransmittingData() && listData.ContainsKey(value.DeviceName))
                    {
                        this.lstReading.Items[value.DeviceName].SubItems[1].Text = "Transmitting";

                        foreach (string readDevice in setup.GetListReadDevice(value.DeviceName).Keys)
                        {
                            this.lstReading.Items[value.DeviceName].SubItems[2].Text += "(" + readDevice + " : " + listData[value.DeviceName][readDevice].Data.ToString() + ")   ";
                        }
                    }
                    else
                    {
                        this.lstReading.Items[value.DeviceName].SubItems[1].Text = "Not transmitting";
                        this.lstReading.Items[value.DeviceName].SubItems[2].Text = "---";
                    }
                }

                this.lstReading.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            catch
            {
            
            }
            
        }

        public void StopTicker()
        {
            ticker.Enabled = false;
           
        }

    }
}
