﻿using System;
using System.Windows.Forms;
using System.Threading;
using FieldTest.XMLConfigClass;

namespace FieldTest.SetupClass.Controls
{
    public partial class DeviceControl : UserControl
    {

        private Setup setup;
        private Thread thread;
        private DeviceSettingControl ctlConfig;
        private Panel panConfig;
        private bool isChecking;
        private string tagDevice;

        public DeviceControl(ref Panel panel, ref Setup set, ADeviceSetting setting, string device)
        {
            InitializeComponent();
            ctlConfig = new DeviceSettingControl(setting, device);
            this.panConfig = panel;
            this.lblDevice.Text = device;
            this.setup = set;
            this.tagDevice = device;
        }

        //Function du threadPortDectecting permettant de vérifier s'il y a un changement dans les porpriétés device
        //Ensuite, applique tous les changements dans les objets correspondant et sérialise le tout
        private void checkConfig()
        {
            while (isChecking)
            {
                if (ctlConfig.IsValueChange == true)
                {
                    lock (ctlConfig.Setting)
                    {
                        setup.Setting.SetDeviceSetting(this.tagDevice, ctlConfig.Setting);
                        setup.UpdateDeviceSetting(this.tagDevice);

                        ctlConfig.IsValueChange = false;
                    }
                }
            }
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            panConfig.Controls.Clear();
            ctlConfig.Dock = DockStyle.Fill;
            panConfig.Controls.Add(ctlConfig);
            isChecking = true;
            thread = new Thread(checkConfig);
            
            thread.Start();
           
        }
    }
}
