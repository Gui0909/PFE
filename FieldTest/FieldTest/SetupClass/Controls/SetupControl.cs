﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FieldTest.DevicePortMapClass;

namespace FieldTest.SetupClass.Controls
{
    public partial class SetupControl : UserControl
    {
        private Setup setup;
        private bool isLoading;
        private StatutsControl ctlStatuts;
        private List<DeviceControl> listSetting;

        private void setStatutsControl()
        {
            ctlStatuts = new StatutsControl(setup);
            panConfig.Controls.Clear();
            ctlStatuts.Dock = DockStyle.Fill;
            panConfig.Controls.Add(ctlStatuts);
        }

        private void fillPanelSettingDevice()
        {
            if (setup.IfPortMapped())
            {
                SortedList<string, ADevicePortMapped> listPortMapped = setup.GetListMappedPort();

                foreach (string tag in listPortMapped.Keys)
                {
                    DeviceControl settingControl = new DeviceControl(ref this.panConfig, ref setup, listPortMapped[tag].DeviceSetting, tag);
                    settingControl.Dock = DockStyle.Top;

                    listSetting.Add(settingControl);
                    panSetting.Controls.Add(settingControl);

                }

            }
        }

        private void initializeConfig()
        {
            //Permet de ne pas activer les events de contrôle lors du loading
            isLoading = true;

            listSetting = new List<DeviceControl>();
            panSetting.Controls.Clear();

            setup.InitialiserSetup();

            this.fillPanelSettingDevice();
            this.setFileConfig();

            //Lors de la première initialisation ajoute un status contrôle dans le panel
            this.setStatutsControl();

            //Reactive les events
            isLoading = false;

        }

        public SetupControl()
        {
            InitializeComponent();
            setup = new Setup();
            initializeConfig(); 
        }

      
        private void setFileConfig()
        {
            this.txtTimeAcquisition.Text = setup.Setting.File.TimeAcquisition;
            this.txtHighAcquisition.Text = setup.Setting.File.HighAcquisition;
            this.txtFolderPath.Text = setup.Setting.File.FolderPath;
           
        }

        private void btnCheckDevice_Click(object sender, EventArgs e)
        {
            panConfig.Controls.Clear();
            setup.ClosePorts();
            setup = new Setup();
            initializeConfig(); 
        }

       

        private void btnPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog.ShowDialog();
            string folderName = txtFolderPath.Text;

            if( result == DialogResult.OK )
            {
                folderName = folderBrowserDialog.SelectedPath;
                txtFolderPath.Text = folderName;

                setup.Setting.File.FolderPath = folderName;
                setup.UpdateFileSetting();
            }

        }

        private void txtTimeAcquisition_TextChanged(object sender, EventArgs e)
        {
            if (!isLoading)
            {
                setup.Setting.File.TimeAcquisition = txtTimeAcquisition.Text;
                setup.UpdateFileSetting();
            }
        }

        private void txtHighAcquisition_TextChanged(object sender, EventArgs e)
        {
            if (!isLoading)
            {
                setup.Setting.File.HighAcquisition = txtHighAcquisition.Text;
                setup.UpdateFileSetting();
            }
        }

        public void StopAllThread()
        {
            ctlStatuts.StopTicker();
            setup.StopReadThread();
        }

        private void panConfig_ControlRemoved(object sender, ControlEventArgs e)
        {
            StopAllThread();
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            panConfig.Controls.Clear();
            setup = new Setup();
            initializeConfig(); 
        }

    }
}
