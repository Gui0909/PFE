﻿namespace FieldTest.SetupClass.Controls
{
    partial class SetupControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupControl));
            this.btnCheckDevice = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPath = new System.Windows.Forms.Button();
            this.txtFolderPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHighAcquisition = new System.Windows.Forms.TextBox();
            this.txtTimeAcquisition = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panSetting = new System.Windows.Forms.Panel();
            this.btnStatus = new System.Windows.Forms.Button();
            this.panConfig = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCheckDevice
            // 
            this.btnCheckDevice.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCheckDevice.BackgroundImage")));
            this.btnCheckDevice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCheckDevice.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCheckDevice.Location = new System.Drawing.Point(3, 16);
            this.btnCheckDevice.Name = "btnCheckDevice";
            this.btnCheckDevice.Size = new System.Drawing.Size(184, 90);
            this.btnCheckDevice.TabIndex = 3;
            this.btnCheckDevice.UseVisualStyleBackColor = true;
            this.btnCheckDevice.Click += new System.EventHandler(this.btnCheckDevice_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPath);
            this.groupBox1.Controls.Add(this.txtFolderPath);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtHighAcquisition);
            this.groupBox1.Controls.Add(this.txtTimeAcquisition);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Location = new System.Drawing.Point(793, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(237, 564);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setting files";
            // 
            // btnPath
            // 
            this.btnPath.Location = new System.Drawing.Point(146, 287);
            this.btnPath.Name = "btnPath";
            this.btnPath.Size = new System.Drawing.Size(75, 23);
            this.btnPath.TabIndex = 8;
            this.btnPath.Text = "Choose";
            this.btnPath.UseVisualStyleBackColor = true;
            this.btnPath.Click += new System.EventHandler(this.btnPath_Click);
            // 
            // txtFolderPath
            // 
            this.txtFolderPath.Enabled = false;
            this.txtFolderPath.Location = new System.Drawing.Point(9, 186);
            this.txtFolderPath.Multiline = true;
            this.txtFolderPath.Name = "txtFolderPath";
            this.txtFolderPath.Size = new System.Drawing.Size(212, 95);
            this.txtFolderPath.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Data Folder Path";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Timed Acquisition File DeviceName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "High Frequency Acquisition File DeviceName";
            // 
            // txtHighAcquisition
            // 
            this.txtHighAcquisition.Location = new System.Drawing.Point(9, 108);
            this.txtHighAcquisition.Name = "txtHighAcquisition";
            this.txtHighAcquisition.Size = new System.Drawing.Size(215, 20);
            this.txtHighAcquisition.TabIndex = 2;
            this.txtHighAcquisition.TextChanged += new System.EventHandler(this.txtHighAcquisition_TextChanged);
            // 
            // txtTimeAcquisition
            // 
            this.txtTimeAcquisition.Location = new System.Drawing.Point(9, 32);
            this.txtTimeAcquisition.Name = "txtTimeAcquisition";
            this.txtTimeAcquisition.Size = new System.Drawing.Size(215, 20);
            this.txtTimeAcquisition.TabIndex = 1;
            this.txtTimeAcquisition.TextChanged += new System.EventHandler(this.txtTimeAcquisition_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panSetting);
            this.groupBox2.Controls.Add(this.btnStatus);
            this.groupBox2.Controls.Add(this.btnCheckDevice);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(190, 564);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Instrument Setup";
            // 
            // panSetting
            // 
            this.panSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panSetting.Location = new System.Drawing.Point(3, 106);
            this.panSetting.Name = "panSetting";
            this.panSetting.Size = new System.Drawing.Size(184, 365);
            this.panSetting.TabIndex = 22;
            // 
            // btnStatus
            // 
            this.btnStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStatus.BackgroundImage")));
            this.btnStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnStatus.Location = new System.Drawing.Point(3, 471);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(184, 90);
            this.btnStatus.TabIndex = 11;
            this.btnStatus.UseVisualStyleBackColor = true;
            this.btnStatus.Click += new System.EventHandler(this.btnStatus_Click);
            // 
            // panConfig
            // 
            this.panConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panConfig.Location = new System.Drawing.Point(3, 16);
            this.panConfig.Name = "panConfig";
            this.panConfig.Size = new System.Drawing.Size(597, 545);
            this.panConfig.TabIndex = 6;
            this.panConfig.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.panConfig_ControlRemoved);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.panConfig);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(190, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(603, 564);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Status";
            // 
            // SetupControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "SetupControl";
            this.Size = new System.Drawing.Size(1030, 564);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCheckDevice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFolderPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHighAcquisition;
        private System.Windows.Forms.TextBox txtTimeAcquisition;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panConfig;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnPath;
        private System.Windows.Forms.Button btnStatus;
        private System.Windows.Forms.Panel panSetting;
    }
}
