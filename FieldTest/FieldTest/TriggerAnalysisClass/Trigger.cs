﻿using FieldTest.DataClass;
using FieldTest.ReaderClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FieldTest.TriggerAnalysisClass
{
    public class Trigger
    {
        public string FolderPath { get; set; }

        public List<ThresholdData> ListData { get; set; }
        public List<string> ListHead { get; set; }
        public SortedList<string, string> ListUnit { get; set; }
        public SortedList<string, string> ListTag { get; set; }
        public double Threshold { get; set; }

        public Trigger()
        {

        }

        //S'occupe d'ouvrir les fichier d'erreurs et en cas d'erreur de traitement envoie une erreur
        public bool OpenDataFile()
        {
            try
            {

                ReaderTrigger reader = new ReaderTrigger(FolderPath);

                this.ListData = reader.GetListData();
                this.ListHead = reader.GetListHead();
                this.ListUnit = reader.GetListUnit();
                this.ListTag = reader.GetListTag();
                this.Threshold = reader.GetThreshold();

                return true;

            }
            catch
            {
                MessageBox.Show("This file is not valid.");
                return false;
            }
        }
    }
}
