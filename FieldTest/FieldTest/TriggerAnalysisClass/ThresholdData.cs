﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.TriggerAnalysisClass
{
    public class ThresholdData
    {
        public string Time { get; set; }
        public string Data { get; set; }

        public ThresholdData(string time, string data)
        {
            this.Time = time;
            this.Data = data;
        }

    }
}
