﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace FieldTest.TriggerAnalysisClass
{
    public partial class TriggerControl : UserControl
    {
        private Trigger trigger;
        private const string THRESHOLD = "Threshold";

        public TriggerControl()
        {
            InitializeComponent();
            trigger = new Trigger();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            this.loadFile();
        }

        private void clearData()
        {
            this.lstTrigger.Items.Clear();
            int nbrColumsToRemove = this.lstTrigger.Columns.Count - 1;

            for (int i = 0; i < nbrColumsToRemove; i++)
            {
                this.lstTrigger.Columns.RemoveAt(1);
            }

            this.grpBThreshold.Text = "Threshold exceeded : ";
        }
        private void setColumns()
        {
            foreach (string head in this.trigger.ListHead)
            {
                if (!head.Contains(THRESHOLD))
                {
                    string name = trigger.ListTag[head.Split('-')[0]] + " " + head + " (" + trigger.ListUnit[head] + ")";
                    this.lstTrigger.Columns.Add(name);
                }
            }
        }

        private void setTriggerData()
        {
            foreach (ThresholdData data in this.trigger.ListData)
            {
                ListViewItem lvi = new ListViewItem(data.Time);

                string message = string.Empty;

                lvi.SubItems.Add(data.Data);

                this.lstTrigger.Items.Add(lvi);

                if (double.Parse(data.Data, NumberStyles.Any) > trigger.Threshold)
                {
                    lvi.BackColor = Color.OrangeRed;
                }

            }
        }

        private void loadFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            DialogResult result = openFileDialog.ShowDialog();
            string folderName = string.Empty;
            this.clearData();


            if (result == DialogResult.OK)
            {
                folderName = openFileDialog.FileName;
                txtFileName.Text = folderName;
                trigger.FolderPath = folderName;

                if (this.trigger.OpenDataFile())
                {
                    this.setColumns();
                    this.setTriggerData();
                    this.grpBThreshold.Text = "Threshold exceeded : " + trigger.Threshold;

                    this.lstTrigger.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                }
            }

        }
    }
}
