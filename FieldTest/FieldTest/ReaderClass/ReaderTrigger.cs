﻿using FieldTest.DataClass;
using FieldTest.TriggerAnalysisClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.ReaderClass
{
    public class ReaderTrigger
    {
        private StreamReader file;

        private List<ThresholdData> listData;
        private List<string> listHead;
        private SortedList<string, string> listUnit;
        private SortedList<string, string> listTag;
        private double threshold;

        private void setListTag()
        {
            string line = file.ReadLine();
            string[] tabTag = line.Split('\t');

            for (int i = 2; i < tabTag.Length; i++)
            {
                string[] tabKey = tabTag[i].Split('-');
                this.listTag.Add(tabKey[1], tabKey[0]);
            }
        }

        private void setListHead()
        {
            string line = file.ReadLine();
            string[] tabHead = line.Split('\t');

            for (int i = 1; i < tabHead.Length; i++)
            {
                this.listHead.Add(tabHead[i]);

                if (i == 1)
                {
                    this.threshold = double.Parse(tabHead[i].Split(':')[1]);
                }

                if (i > 1)
                    this.listUnit.Add(tabHead[i], string.Empty);
            }
        }

        private void setListUnit()
        {
            string line = file.ReadLine();
            string[] tabUnit = line.Split('\t');

            for (int i = 2; i < tabUnit.Length; i++)
            {
                this.listUnit[this.listUnit.ElementAt(i - 2).Key] = tabUnit[i];
            }
        }

        private void setListDataError()
        {
            string line = string.Empty;

            while ((line = file.ReadLine()) != null)
            {
                string[] tabLine = line.Split('\t');

                this.listData.Add(new ThresholdData(tabLine[0], tabLine[1].Trim(' ').Split('>')[0]));
            }
        }

        public ReaderTrigger(string path)
        {

            this.listData = new List<ThresholdData>();
            this.listHead = new List<string>();
            this.listUnit = new SortedList<string, string>();
            this.listTag = new SortedList<string, string>();

            this.file = new StreamReader(path);

            //Pour lire Error dans l'entête qui ne sert à rien (j'utilise un try catch pour savoir si le fichier est valide)
            string line = file.ReadLine();

            this.setListTag();
            this.setListHead();
            this.setListUnit();
            this.setListDataError();

            this.file.Close();

        }

        public SortedList<string, string> GetListTag()
        {
            return this.listTag;
        }

        public SortedList<string, string> GetListUnit()
        {
            return this.listUnit;
        }

        public List<string> GetListHead()
        {
            return this.listHead;
        }

        public List<ThresholdData> GetListData()
        {
            return this.listData;
        }

        public double GetThreshold()
        {
            return this.threshold;
        }

    }
}
