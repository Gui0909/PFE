﻿
using FieldTest.AnalysisClass.Report;
using FieldTest.DataClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FieldTest.ReaderClass
{
    public class ReaderData
    {
        private StreamReader file;
       
        private SortedList<string, List<Coordinate>> listNormalData;
        private SortedList<string, List<Coordinate>> listHighData;
        private SortedList<string, string> listUnit;
        private SortedList<string, string> listTag;
        private Acquisition.TYPE acqui;
        private SortedList<string, AverageReport> listAverage;

        private void setTypeAcquisition()
        {
            string line = file.ReadLine();

            if (line.Trim() == Acquisition.TYPE.NORMAL.ToString())
                this.acqui = Acquisition.TYPE.NORMAL;
            else
                this.acqui = Acquisition.TYPE.HIGH;
        }

        private void setListTag()
        {
            string line = file.ReadLine();
            string[] tabLine = line.Split('\t');

            for (int i = 1; i < tabLine.Length; i++)
            {
                string[] tabTag = tabLine[i].Split('-');
                this.listTag.Add(tabTag[1], tabTag[0]);
            }
        }

        private void setInfoListData()
        {
            string line = file.ReadLine();
            string[] tabLine = line.Split('\t');

            for (int i = 2; i < tabLine.Length; i++)
            {
                string device = tabLine[i];

                this.listNormalData.Add(device, new List<Coordinate>());
                this.listHighData.Add(device, new List<Coordinate>());
                this.listAverage.Add(device, new AverageReport(device, this.listTag[device.Split('-')[0]], 0, string.Empty));

                if (!this.listUnit.ContainsKey(device))
                {
                    this.listUnit.Add(device, string.Empty);
                }

            }
        }

        private void setListUnit()
        {
            string line = file.ReadLine();
            string[] tabLine = line.Split('\t');
            int index = 0;

            for (int i = 2; i < tabLine.Length; i++)
            {
                if (tabLine[i] != string.Empty)
                {

                    this.listUnit[this.listUnit.ElementAt(index).Key] = tabLine[i];
                    this.listAverage[this.listUnit.ElementAt(index).Key].Unit = tabLine[i];
                    index++;
                }

            }
        }

        public ReaderData(string path)
        {
            this.listNormalData = new SortedList<string, List<Coordinate>>();
            this.listHighData = new SortedList<string, List<Coordinate>>();
            this.listUnit = new SortedList<string, string>();
            this.listAverage = new SortedList<string, AverageReport>();
            this.listTag = new SortedList<string, string>();

            this.file = new StreamReader(path);

            this.setTypeAcquisition();
            this.setListTag();
            this.setInfoListData();
            this.setListUnit();

            //Remplit les listes de données selon le type d'acquisition
            this.readData();
            
        }

        private void calculAverage()
        {
            foreach (string key in listNormalData.Keys)
            {
                double sum = 0;
                int count = 0;

                for (int i = 0; i < listNormalData[key].Count; i++)
                {
                    count++;
                    sum += listNormalData[key][i].Data;
                }

                listAverage[key].Average = Math.Round(sum / count, 2);
            }
        }

        private void readNormalData()
        {
            string line;

            while((line = file.ReadLine()) != null)
            {
                string[] tabLine = line.Split('\t');
                DateTime x = Convert.ToDateTime(tabLine[0]);

                for (int i = 2; i < tabLine.Length; i++)
                {
                    listNormalData.ElementAt(i-2).Value.Add(new Coordinate(x, double.Parse(tabLine[i])));
                }
            }

            calculAverage();

            this.file.Close();
        }

        private void readHighData()
        {
            string line;

            //Skip la ligne de d'identification des colonne frequence, time ...
            file.ReadLine();

            while ((line = file.ReadLine()) != null)
            {
                string[] tabLine = line.Split('\t');
                int index = 0;
                double x = 0;
                double freq = 0;

                if (tabLine[0] != "--")
                    x = double.Parse(tabLine[0]);

                if (tabLine[1] != "--")
                    freq = double.Parse(tabLine[1]);

                for (int i = 2; i < tabLine.Length; i+=2)
                {

                    if (tabLine[i] != "--")
                    {
                        listNormalData.ElementAt(index).Value.Add(new Coordinate(x, double.Parse(tabLine[i]), Acquisition.TYPE.NORMAL));
                    }

                    //La donnée haute fréquence ets toujours un index plus loin que la donnée normal
                    if (tabLine[i + 1] != "--")
                    {
                        listHighData.ElementAt(index).Value.Add(new Coordinate(freq, double.Parse(tabLine[i + 1]), Acquisition.TYPE.HIGH));
                    }

                    index++;
                }

            }

            calculAverage();

            this.file.Close();
        }

        public SortedList<string, List<Coordinate>> GetNormalData()
        {
            return this.listNormalData;
        }

        public SortedList<string, List<Coordinate>> GetHighData()
        {
            return this.listHighData;
        }

        public SortedList<string, string> GetUnitData()
        {
            return this.listUnit;
        }

        public SortedList<string, string> GetTagData()
        {
            return this.listTag;
        }

        public Acquisition.TYPE GetTypeAcquisition()
        {
            return this.acqui;
        }

        public SortedList<string, AverageReport> GetAverageData()
        {
            return this.listAverage;
        }

        private void readData()
        {
            if (acqui == Acquisition.TYPE.NORMAL)
                readNormalData();
            else
                readHighData();
        }

    }
}
