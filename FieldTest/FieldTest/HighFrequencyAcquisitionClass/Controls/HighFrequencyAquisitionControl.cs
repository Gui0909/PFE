﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FieldTest.HighFrequencyAcquisitionClass.Class;
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;

namespace FieldTest.HighFrequencyAcquisitionClass.Controls
{
    public partial class HighFrequencyAquisitionControl : UserControl
    {

        private HighFrequencyAcquisition hiFreqAqi;

        private const string NONE = "None";

        private void fillDeviceListReading()
        {
            this.lstReading.Items.Clear();

            foreach (string device in hiFreqAqi.GetListMappedPort().Keys)
            {
                
                foreach (string readDevice in hiFreqAqi.GetListReadDevice(device).Keys)
                {
                    if (hiFreqAqi.ValidHighAcquisition(device, readDevice))
                    {
                        ListViewItem lvi = new ListViewItem(device + "-" + readDevice + " (" + this.hiFreqAqi.GetReadDevice(device, readDevice).GetConcreteUnit() + ")");
                        lvi.SubItems.Add(String.Empty);
                        this.lstReading.Items.Add(lvi);
                        this.lstReading.Items[this.lstReading.Items.Count - 1].Name = device + "-" + readDevice;
                    }
                }
               
            }
        }

        private void fillDataListReading(SortedList<string, double> listData)
        {
            foreach (string readDevice in listData.Keys)
            {

                this.lstReading.Items[readDevice].SubItems[1].Text = Math.Round(listData[readDevice], 2).ToString();
                   
            }

        }

        private void fillCboDevice1()
        {
            this.cboDevice1.Items.Add(NONE);
            foreach (string device in this.hiFreqAqi.GetListMappedPort().Keys)
            {
                foreach (string readDevice in this.hiFreqAqi.GetMappedPort(device).ListDeviceReading.Keys)
                {
                    if (this.hiFreqAqi.ValidHighAcquisition(device, readDevice))
                    {
                        this.cboDevice1.Items.Add(device);
                        break;
                    }
                }
                
            }
              
            this.cboDevice1.SelectedIndex = 0;
        }

        private void fillCboDevice2()
        {
            this.cboDevice2.Items.Add(NONE);
            foreach (string device in this.hiFreqAqi.GetListMappedPort().Keys)
            {
                foreach (string readDevice in this.hiFreqAqi.GetMappedPort(device).ListDeviceReading.Keys)
                {
                    if (this.hiFreqAqi.ValidHighAcquisition(device, readDevice))
                    {
                        this.cboDevice2.Items.Add(device);
                        break;
                    }
                }

            }

            this.cboDevice2.SelectedIndex = 0;
        }

        public HighFrequencyAquisitionControl()
        {
            InitializeComponent();

            HighFrequencyDataPublisher dataPublisher = new HighFrequencyDataPublisher();

            dataPublisher.RaiseDataEvent += UpdateHighSerie1;
            dataPublisher.RaiseDataEvent += UpdateHighSerie2;

            dataPublisher.RaiseDataEvent += UpdateSerie1;
            dataPublisher.RaiseDataEvent += UpdateSerie2;

            dataPublisher.RaiseDataEvent += UpdateListReadingListReading;

            hiFreqAqi = new HighFrequencyAcquisition(this, dataPublisher);

            fillCboDevice1();
            fillCboDevice2();
            fillDeviceListReading();
            this.cboMeasurment.DataSource = Enum.GetValues(typeof(HighFrequencyAcquisition.MEASUREMENT));
            this.cboMeasurment.SelectedIndex = 0;
            this.cboSample.SelectedIndex = 5;

            


        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            this.hiFreqAqi.StartRead();
            this.btnStart.Enabled = false;
            this.btnStop.Enabled = true;
        }

        private void updateChartAcquisition(int serie, List<Coordinate> listData)
        {
            this.chartAcquisition.Series[serie].Points.DataBind(listData, "TimeStamp", "Data", "Tooltip=Data"); 
        }

        private void updateHighChartAcquisition(int serie, List<Coordinate> listData)
        {
            listData.RemoveRange(listData.Count / 2, listData.Count / 2);
            this.chartHighAcquisition.Series[serie].Points.DataBind(listData, "Frequency", "Amplitude", "Tooltip=Amplitude");
        }

        public void UpdateSerie1(object sender, HighFrequencyDataEventArgs e)
        {
            if (this.hiFreqAqi.Serie1 != null)
            {
                this.chartAcquisition.Invoke((MethodInvoker)(() => updateChartAcquisition(0, e.ListDataSerie1)));
            }
        }

        public void UpdateSerie2(object sender, HighFrequencyDataEventArgs e)
        {
            if (this.hiFreqAqi.Serie2 != null)
            {
                this.chartAcquisition.Invoke((MethodInvoker)(() => updateChartAcquisition(1, e.ListDataSerie2)));
            }
        }

        public void UpdateListReadingListReading(object sender, HighFrequencyDataEventArgs e)
        {
            this.chartHighAcquisition.Invoke((MethodInvoker)(() => this.fillDataListReading(e.ListMoyenne)));
        }

        private void cbSave_CheckedChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.EnableWritting = this.cbSave.Checked;
        }

        private void cboDevice1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboReadDevice1.Items.Clear();

            this.cboReadDevice1.Items.Add(NONE);

            if (this.cboDevice1.SelectedItem.ToString() != NONE)
            {

                foreach (string key in hiFreqAqi.GetListReadDevice(this.cboDevice1.SelectedItem.ToString()).Keys)
                {
                    if (this.hiFreqAqi.ValidHighAcquisition(this.cboDevice1.SelectedItem.ToString(), key))
                    {
                        this.cboReadDevice1.Items.Add(key);
                    }
                }

                this.lblTagSerie1.Text = "(" + this.hiFreqAqi.GetMappedPort(this.cboDevice1.SelectedItem.ToString()).DeviceDescription + ")";
            }
            else
            {
                this.lblTagSerie1.Text = "(---)";
            }

            this.cboReadDevice1.SelectedIndex = 0;
        }

        private void cboDevice2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboReadDevice2.Items.Clear();

            this.cboReadDevice2.Items.Add(NONE);

            if (this.cboDevice2.SelectedItem.ToString() != NONE)
            {
                foreach (string key in hiFreqAqi.GetListReadDevice(this.cboDevice2.SelectedItem.ToString()).Keys)
                {
                    if (this.hiFreqAqi.ValidHighAcquisition(this.cboDevice2.SelectedItem.ToString(), key))
                    {
                        this.cboReadDevice2.Items.Add(key);
                    }
                }

                this.lblTagSerie2.Text = "(" + this.hiFreqAqi.GetMappedPort(this.cboDevice2.SelectedItem.ToString()).DeviceDescription + ")";
            }
            else
            {
                this.lblTagSerie2.Text = "(---)";
            }

            this.cboReadDevice2.SelectedIndex = 0;
        }

        private void cboReadDevice1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.SetPlot1(this.cboDevice1.SelectedItem.ToString(), this.cboReadDevice1.SelectedItem.ToString());

            this.chartAcquisition.Series[0].Points.Clear();
            this.chartHighAcquisition.Series[0].Points.Clear();

            this.chartAcquisition.Series[0].LegendText = this.cboDevice1.SelectedItem.ToString() + "-" + this.cboReadDevice1.SelectedItem.ToString();
            this.chartHighAcquisition.Series[0].LegendText = this.cboDevice1.SelectedItem.ToString() + "-" + this.cboReadDevice1.SelectedItem.ToString();

            if (cboReadDevice1.SelectedItem.ToString() != NONE)
            {
                this.chartAcquisition.ChartAreas[0].Axes[1].Title = "Data (" + this.hiFreqAqi.GetReadDevice(this.cboDevice1.SelectedItem.ToString(), this.cboReadDevice1.SelectedItem.ToString()).GetConcreteUnit() + ")";
                this.lblUnitSerie1.Text = this.hiFreqAqi.GetReadDevice(this.cboDevice1.SelectedItem.ToString(), this.cboReadDevice1.SelectedItem.ToString()).GetConcreteUnit();
            }
            else
            {
                this.lblUnitSerie1.Text = "---";
            }

            //Gestion si une série sur deux est présente
            if (this.cboReadDevice1.SelectedItem.ToString() != NONE || (this.cboReadDevice2.SelectedItem != null && this.cboReadDevice2.SelectedItem.ToString() != NONE))
            {
                this.btnStart.Enabled = true;
                this.btnClear.Enabled = true;
            }
            else
            {
                this.btnStart.Enabled = false;
                this.btnClear.Enabled = false;
            }
        }

        private void cboReadDevice2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.SetPlot2(this.cboDevice2.SelectedItem.ToString(), this.cboReadDevice2.SelectedItem.ToString());

            this.chartAcquisition.Series[1].Points.Clear();
            this.chartHighAcquisition.Series[1].Points.Clear();

            this.chartAcquisition.Series[1].LegendText = this.cboDevice2.SelectedItem.ToString() + "-" + this.cboReadDevice2.SelectedItem.ToString();
            this.chartHighAcquisition.Series[1].LegendText = this.cboDevice2.SelectedItem.ToString() + "-" + this.cboReadDevice2.SelectedItem.ToString();

            if (cboReadDevice2.SelectedItem.ToString() != NONE)
            {
                this.chartAcquisition.ChartAreas[0].Axes[3].Title = "Data (" + this.hiFreqAqi.GetReadDevice(this.cboDevice2.SelectedItem.ToString(), this.cboReadDevice2.SelectedItem.ToString()).GetConcreteUnit() + ")";
                this.lblUnitSerie2.Text = this.hiFreqAqi.GetReadDevice(this.cboDevice2.SelectedItem.ToString(), this.cboReadDevice2.SelectedItem.ToString()).GetConcreteUnit();
            }
            else
            {
                this.lblUnitSerie2.Text = "---";
            }

            //Gestion si une série sur deux est présente
            if ((this.cboReadDevice1.SelectedItem != null && this.cboReadDevice1.SelectedItem.ToString() != NONE) || this.cboReadDevice2.SelectedItem.ToString() != NONE)
            {
                this.btnStart.Enabled = true;
                this.btnClear.Enabled = true;
            }
            else
            {
                this.btnStart.Enabled = false;
                this.btnClear.Enabled = false;
            }
        }

        public void UpdateHighSerie1(object sender, HighFrequencyDataEventArgs e)
        {
            if (this.hiFreqAqi.Serie1 != null)
            {
                this.chartHighAcquisition.Invoke((MethodInvoker)(() => updateHighChartAcquisition(0, e.ListDataSerie1)));
            }
            
        }

        public void UpdateHighSerie2(object sender, HighFrequencyDataEventArgs e)
        {
            if (this.hiFreqAqi.Serie2 != null)
            {
                this.chartHighAcquisition.Invoke((MethodInvoker)(() => updateHighChartAcquisition(1, e.ListDataSerie2)));
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            this.hiFreqAqi.StopRead();
            this.btnStop.Enabled = false;

            if (this.cboReadDevice1.SelectedItem.ToString() != NONE || this.cboReadDevice2.SelectedItem.ToString() != NONE)
            {
                this.btnStart.Enabled = true;
            }
        }

        private void clearChartAcquisition()
        {
            this.chartAcquisition.Series[0].Points.Clear();
            this.chartAcquisition.Series[1].Points.Clear();
        }

        private void clearChartHighAcquisition()
        {
            this.chartHighAcquisition.Series[0].Points.Clear();
            this.chartHighAcquisition.Series[1].Points.Clear();
        }

        public void ClearPoints()
        {
            this.chartAcquisition.Invoke((MethodInvoker)(() => this.clearChartAcquisition()));
            this.chartHighAcquisition.Invoke((MethodInvoker)(() => this.clearChartHighAcquisition()));

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.chartAcquisition.Series[0].Points.Clear();
            this.chartAcquisition.Series[1].Points.Clear();

            this.chartHighAcquisition.Series[0].Points.Clear();
            this.chartHighAcquisition.Series[1].Points.Clear();
        }

        private void cboSample_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.NumberSample = int.Parse(this.cboSample.SelectedItem.ToString());
        }

        private void enableStartStop()
        {
            this.btnStart.Enabled = true;
            this.btnStop.Enabled = false;
        }

        public void UpdateControl()
        {
            this.btnStop.Invoke((MethodInvoker)(() => this.enableStartStop()));
        }

        private void cboCont_CheckedChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.IsContinuous = this.cboCont.Checked;
        }

        private void cboMeasurment_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.Measurement = (HighFrequencyAcquisition.MEASUREMENT)cboMeasurment.SelectedItem;
        }

        private void btnColor1_Click(object sender, EventArgs e)
        {
            if (this.colorDialogHighAcq.ShowDialog() == DialogResult.OK)
            {
                this.chartAcquisition.Series[0].Color = this.colorDialogHighAcq.Color;
                this.chartHighAcquisition.Series[0].Color = this.colorDialogHighAcq.Color;
            } 
        }

        private void btnColor2_Click(object sender, EventArgs e)
        {
            if (this.colorDialogHighAcq.ShowDialog() == DialogResult.OK)
            {
                this.chartAcquisition.Series[1].Color = this.colorDialogHighAcq.Color;
                this.chartHighAcquisition.Series[1].Color = this.colorDialogHighAcq.Color;
            } 
        }

        private void cboTrigger_CheckedChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.EnableTriggerError = this.cboTrigger.Checked;
            this.hiFreqAqi.ThresholdTrigger = (double)this.numTrigger.Value;
            this.hiFreqAqi.NumberSampleTrigger = (int)this.numSampleTrigger.Value;

            this.numTrigger.Enabled = this.cboTrigger.Checked;
            this.numSampleTrigger.Enabled = this.cboTrigger.Checked;
        }

        private void numTrigger_ValueChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.ThresholdTrigger = (double)this.numTrigger.Value;
        }

        private void numSampleTrigger_ValueChanged(object sender, EventArgs e)
        {
            this.hiFreqAqi.NumberSampleTrigger = (int)this.numSampleTrigger.Value;
        }

      
    }
}
