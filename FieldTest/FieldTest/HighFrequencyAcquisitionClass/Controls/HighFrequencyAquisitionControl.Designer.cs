﻿namespace FieldTest.HighFrequencyAcquisitionClass.Controls
{
    partial class HighFrequencyAquisitionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HighFrequencyAquisitionControl));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numSampleTrigger = new System.Windows.Forms.NumericUpDown();
            this.numTrigger = new System.Windows.Forms.NumericUpDown();
            this.cboTrigger = new System.Windows.Forms.CheckBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.cboSample = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.cboMeasurment = new System.Windows.Forms.ComboBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.cboCont = new System.Windows.Forms.CheckBox();
            this.cbSave = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstReading = new System.Windows.Forms.ListView();
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTagSerie2 = new System.Windows.Forms.Label();
            this.lblTagSerie1 = new System.Windows.Forms.Label();
            this.btnColor2 = new System.Windows.Forms.Button();
            this.btnColor1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblUnitSerie2 = new System.Windows.Forms.Label();
            this.lblUnitSerie1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboDevice2 = new System.Windows.Forms.ComboBox();
            this.cboDevice1 = new System.Windows.Forms.ComboBox();
            this.cboReadDevice2 = new System.Windows.Forms.ComboBox();
            this.cboReadDevice1 = new System.Windows.Forms.ComboBox();
            this.chartHighAcquisition = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartAcquisition = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.colorDialogHighAcq = new System.Windows.Forms.ColorDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSampleTrigger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTrigger)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartHighAcquisition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAcquisition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Location = new System.Drawing.Point(666, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(364, 564);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Config Graph";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.numSampleTrigger);
            this.groupBox4.Controls.Add(this.numTrigger);
            this.groupBox4.Controls.Add(this.cboTrigger);
            this.groupBox4.Controls.Add(this.btnStop);
            this.groupBox4.Controls.Add(this.cboSample);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnStart);
            this.groupBox4.Controls.Add(this.cboMeasurment);
            this.groupBox4.Controls.Add(this.btnClear);
            this.groupBox4.Controls.Add(this.cboCont);
            this.groupBox4.Controls.Add(this.cbSave);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Location = new System.Drawing.Point(7, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(350, 202);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Operations";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(262, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Sample :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(148, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Trigger :";
            // 
            // numSampleTrigger
            // 
            this.numSampleTrigger.Enabled = false;
            this.numSampleTrigger.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSampleTrigger.Location = new System.Drawing.Point(240, 136);
            this.numSampleTrigger.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numSampleTrigger.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numSampleTrigger.Name = "numSampleTrigger";
            this.numSampleTrigger.Size = new System.Drawing.Size(98, 20);
            this.numSampleTrigger.TabIndex = 21;
            this.numSampleTrigger.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numSampleTrigger.ValueChanged += new System.EventHandler(this.numSampleTrigger_ValueChanged);
            // 
            // numTrigger
            // 
            this.numTrigger.Enabled = false;
            this.numTrigger.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numTrigger.Location = new System.Drawing.Point(123, 136);
            this.numTrigger.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numTrigger.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numTrigger.Name = "numTrigger";
            this.numTrigger.Size = new System.Drawing.Size(100, 20);
            this.numTrigger.TabIndex = 20;
            this.numTrigger.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numTrigger.ValueChanged += new System.EventHandler(this.numTrigger_ValueChanged);
            // 
            // cboTrigger
            // 
            this.cboTrigger.AutoSize = true;
            this.cboTrigger.Location = new System.Drawing.Point(179, 96);
            this.cboTrigger.Name = "cboTrigger";
            this.cboTrigger.Size = new System.Drawing.Size(109, 17);
            this.cboTrigger.TabIndex = 18;
            this.cboTrigger.Text = "Threshold Trigger";
            this.cboTrigger.UseVisualStyleBackColor = true;
            this.cboTrigger.CheckedChanged += new System.EventHandler(this.cboTrigger_CheckedChanged);
            // 
            // btnStop
            // 
            this.btnStop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStop.BackgroundImage")));
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(151, 19);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(53, 67);
            this.btnStop.TabIndex = 1;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // cboSample
            // 
            this.cboSample.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSample.FormattingEnabled = true;
            this.cboSample.Items.AddRange(new object[] {
            "65536",
            "32768",
            "16384",
            "8192",
            "4096",
            "2048",
            "1024",
            "512",
            "256",
            "128",
            "64"});
            this.cboSample.Location = new System.Drawing.Point(209, 175);
            this.cboSample.Name = "cboSample";
            this.cboSample.Size = new System.Drawing.Size(120, 21);
            this.cboSample.TabIndex = 17;
            this.cboSample.SelectedIndexChanged += new System.EventHandler(this.cboSample_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Measurement";
            // 
            // btnStart
            // 
            this.btnStart.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStart.BackgroundImage")));
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStart.Enabled = false;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.Location = new System.Drawing.Point(73, 19);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(53, 67);
            this.btnStart.TabIndex = 0;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cboMeasurment
            // 
            this.cboMeasurment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeasurment.FormattingEnabled = true;
            this.cboMeasurment.Items.AddRange(new object[] {
            "dB",
            "Metric"});
            this.cboMeasurment.Location = new System.Drawing.Point(18, 175);
            this.cboMeasurment.Name = "cboMeasurment";
            this.cboMeasurment.Size = new System.Drawing.Size(147, 21);
            this.cboMeasurment.TabIndex = 15;
            this.cboMeasurment.SelectedIndexChanged += new System.EventHandler(this.cboMeasurment_SelectedIndexChanged);
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClear.BackgroundImage")));
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Enabled = false;
            this.btnClear.Location = new System.Drawing.Point(222, 19);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(53, 67);
            this.btnClear.TabIndex = 14;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cboCont
            // 
            this.cboCont.AutoSize = true;
            this.cboCont.Location = new System.Drawing.Point(19, 130);
            this.cboCont.Name = "cboCont";
            this.cboCont.Size = new System.Drawing.Size(79, 17);
            this.cboCont.TabIndex = 3;
            this.cboCont.Text = "Continuous";
            this.cboCont.UseVisualStyleBackColor = true;
            this.cboCont.CheckedChanged += new System.EventHandler(this.cboCont_CheckedChanged);
            // 
            // cbSave
            // 
            this.cbSave.AutoSize = true;
            this.cbSave.Checked = true;
            this.cbSave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSave.Location = new System.Drawing.Point(19, 105);
            this.cbSave.Name = "cbSave";
            this.cbSave.Size = new System.Drawing.Size(77, 17);
            this.cbSave.TabIndex = 2;
            this.cbSave.Text = "Save Data";
            this.cbSave.UseVisualStyleBackColor = true;
            this.cbSave.CheckedChanged += new System.EventHandler(this.cbSave_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(206, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Samples on Graph";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.lstReading);
            this.groupBox3.Location = new System.Drawing.Point(7, 330);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(350, 225);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reading";
            // 
            // lstReading
            // 
            this.lstReading.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colType,
            this.colValue});
            this.lstReading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstReading.FullRowSelect = true;
            this.lstReading.GridLines = true;
            this.lstReading.Location = new System.Drawing.Point(3, 16);
            this.lstReading.MultiSelect = false;
            this.lstReading.Name = "lstReading";
            this.lstReading.Size = new System.Drawing.Size(344, 206);
            this.lstReading.TabIndex = 0;
            this.lstReading.UseCompatibleStateImageBehavior = false;
            this.lstReading.View = System.Windows.Forms.View.Details;
            // 
            // colType
            // 
            this.colType.Text = "read Devices";
            this.colType.Width = 195;
            // 
            // colValue
            // 
            this.colValue.Text = "Values";
            this.colValue.Width = 144;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblTagSerie2);
            this.groupBox2.Controls.Add(this.lblTagSerie1);
            this.groupBox2.Controls.Add(this.btnColor2);
            this.groupBox2.Controls.Add(this.btnColor1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lblUnitSerie2);
            this.groupBox2.Controls.Add(this.lblUnitSerie1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cboDevice2);
            this.groupBox2.Controls.Add(this.cboDevice1);
            this.groupBox2.Controls.Add(this.cboReadDevice2);
            this.groupBox2.Controls.Add(this.cboReadDevice1);
            this.groupBox2.Location = new System.Drawing.Point(7, 227);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(350, 103);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plot";
            // 
            // lblTagSerie2
            // 
            this.lblTagSerie2.AutoSize = true;
            this.lblTagSerie2.Location = new System.Drawing.Point(54, 60);
            this.lblTagSerie2.Name = "lblTagSerie2";
            this.lblTagSerie2.Size = new System.Drawing.Size(22, 13);
            this.lblTagSerie2.TabIndex = 22;
            this.lblTagSerie2.Text = "(...)";
            // 
            // lblTagSerie1
            // 
            this.lblTagSerie1.AutoSize = true;
            this.lblTagSerie1.Location = new System.Drawing.Point(54, 16);
            this.lblTagSerie1.Name = "lblTagSerie1";
            this.lblTagSerie1.Size = new System.Drawing.Size(22, 13);
            this.lblTagSerie1.TabIndex = 21;
            this.lblTagSerie1.Text = "(...)";
            // 
            // btnColor2
            // 
            this.btnColor2.Location = new System.Drawing.Point(302, 76);
            this.btnColor2.Name = "btnColor2";
            this.btnColor2.Size = new System.Drawing.Size(45, 23);
            this.btnColor2.TabIndex = 20;
            this.btnColor2.Text = "Color";
            this.btnColor2.UseVisualStyleBackColor = true;
            this.btnColor2.Click += new System.EventHandler(this.btnColor2_Click);
            // 
            // btnColor1
            // 
            this.btnColor1.Location = new System.Drawing.Point(302, 32);
            this.btnColor1.Name = "btnColor1";
            this.btnColor1.Size = new System.Drawing.Size(45, 21);
            this.btnColor1.TabIndex = 19;
            this.btnColor1.Text = "Color";
            this.btnColor1.UseVisualStyleBackColor = true;
            this.btnColor1.Click += new System.EventHandler(this.btnColor1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(281, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Unit : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(281, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Unit : ";
            // 
            // lblUnitSerie2
            // 
            this.lblUnitSerie2.AutoSize = true;
            this.lblUnitSerie2.Location = new System.Drawing.Point(313, 60);
            this.lblUnitSerie2.Name = "lblUnitSerie2";
            this.lblUnitSerie2.Size = new System.Drawing.Size(16, 13);
            this.lblUnitSerie2.TabIndex = 12;
            this.lblUnitSerie2.Text = "---";
            // 
            // lblUnitSerie1
            // 
            this.lblUnitSerie1.AutoSize = true;
            this.lblUnitSerie1.Location = new System.Drawing.Point(313, 16);
            this.lblUnitSerie1.Name = "lblUnitSerie1";
            this.lblUnitSerie1.Size = new System.Drawing.Size(16, 13);
            this.lblUnitSerie1.TabIndex = 11;
            this.lblUnitSerie1.Text = "---";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(156, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "ReadDevice 2 : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(156, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "ReadDevice 1 : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Device 2 : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Device 1 : ";
            // 
            // cboDevice2
            // 
            this.cboDevice2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice2.FormattingEnabled = true;
            this.cboDevice2.Location = new System.Drawing.Point(6, 76);
            this.cboDevice2.Name = "cboDevice2";
            this.cboDevice2.Size = new System.Drawing.Size(147, 21);
            this.cboDevice2.TabIndex = 3;
            this.cboDevice2.SelectedIndexChanged += new System.EventHandler(this.cboDevice2_SelectedIndexChanged);
            // 
            // cboDevice1
            // 
            this.cboDevice1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice1.FormattingEnabled = true;
            this.cboDevice1.Location = new System.Drawing.Point(6, 32);
            this.cboDevice1.Name = "cboDevice1";
            this.cboDevice1.Size = new System.Drawing.Size(147, 21);
            this.cboDevice1.TabIndex = 2;
            this.cboDevice1.SelectedIndexChanged += new System.EventHandler(this.cboDevice1_SelectedIndexChanged);
            // 
            // cboReadDevice2
            // 
            this.cboReadDevice2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReadDevice2.FormattingEnabled = true;
            this.cboReadDevice2.Location = new System.Drawing.Point(159, 76);
            this.cboReadDevice2.Name = "cboReadDevice2";
            this.cboReadDevice2.Size = new System.Drawing.Size(142, 21);
            this.cboReadDevice2.TabIndex = 1;
            this.cboReadDevice2.SelectedIndexChanged += new System.EventHandler(this.cboReadDevice2_SelectedIndexChanged);
            // 
            // cboReadDevice1
            // 
            this.cboReadDevice1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReadDevice1.FormattingEnabled = true;
            this.cboReadDevice1.Location = new System.Drawing.Point(159, 32);
            this.cboReadDevice1.Name = "cboReadDevice1";
            this.cboReadDevice1.Size = new System.Drawing.Size(142, 21);
            this.cboReadDevice1.TabIndex = 0;
            this.cboReadDevice1.SelectedIndexChanged += new System.EventHandler(this.cboReadDevice1_SelectedIndexChanged);
            // 
            // chartHighAcquisition
            // 
            chartArea1.AxisX.LabelStyle.Format = "N4";
            chartArea1.AxisX.Title = "Frequence (kHz)";
            chartArea1.AxisX2.LabelStyle.Format = "N4";
            chartArea1.AxisX2.Title = "Frequence (kHz)";
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.AxisY.LabelStyle.Format = "N2";
            chartArea1.AxisY.Title = "Amplitude";
            chartArea1.AxisY2.IsStartedFromZero = false;
            chartArea1.AxisY2.LabelStyle.Format = "N2";
            chartArea1.AxisY2.Title = "Amplitude";
            chartArea1.CursorX.Interval = 0.001D;
            chartArea1.CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.CursorY.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorY.IsUserEnabled = true;
            chartArea1.CursorY.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            this.chartHighAcquisition.ChartAreas.Add(chartArea1);
            this.chartHighAcquisition.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Alignment = System.Drawing.StringAlignment.Far;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend1.Name = "Legend1";
            this.chartHighAcquisition.Legends.Add(legend1);
            this.chartHighAcquisition.Location = new System.Drawing.Point(0, 0);
            this.chartHighAcquisition.Name = "chartHighAcquisition";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Series2";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chartHighAcquisition.Series.Add(series1);
            this.chartHighAcquisition.Series.Add(series2);
            this.chartHighAcquisition.Size = new System.Drawing.Size(660, 270);
            this.chartHighAcquisition.TabIndex = 8;
            this.chartHighAcquisition.Text = "chart1";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title1.Name = "Title1";
            title1.Text = "High Frequency Torque Spectrum";
            this.chartHighAcquisition.Titles.Add(title1);
            // 
            // chartAcquisition
            // 
            chartArea2.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea2.AxisX.LabelStyle.Format = "N4";
            chartArea2.AxisX.Title = "Time";
            chartArea2.AxisX2.LabelStyle.Format = "N4";
            chartArea2.AxisX2.Title = "Time";
            chartArea2.AxisY.IsStartedFromZero = false;
            chartArea2.AxisY.LabelStyle.Format = "N2";
            chartArea2.AxisY.Title = "Data";
            chartArea2.AxisY2.IsStartedFromZero = false;
            chartArea2.AxisY2.LabelStyle.Format = "N2";
            chartArea2.AxisY2.Title = "Data";
            chartArea2.CursorX.Interval = 0.001D;
            chartArea2.CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea2.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea2.CursorX.IsUserEnabled = true;
            chartArea2.CursorX.IsUserSelectionEnabled = true;
            chartArea2.CursorY.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea2.CursorY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea2.CursorY.IsUserEnabled = true;
            chartArea2.CursorY.IsUserSelectionEnabled = true;
            chartArea2.Name = "ChartArea1";
            this.chartAcquisition.ChartAreas.Add(chartArea2);
            this.chartAcquisition.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Alignment = System.Drawing.StringAlignment.Far;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend2.Name = "Legend1";
            this.chartAcquisition.Legends.Add(legend2);
            this.chartAcquisition.Location = new System.Drawing.Point(0, 0);
            this.chartAcquisition.Name = "chartAcquisition";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "Series2";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series4.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series4.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chartAcquisition.Series.Add(series3);
            this.chartAcquisition.Series.Add(series4);
            this.chartAcquisition.Size = new System.Drawing.Size(660, 271);
            this.chartAcquisition.TabIndex = 7;
            this.chartAcquisition.Text = "chart1";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title2.Name = "Title1";
            title2.Text = "High Frequency Measurement";
            this.chartAcquisition.Titles.Add(title2);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 16);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chartHighAcquisition);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chartAcquisition);
            this.splitContainer1.Size = new System.Drawing.Size(660, 545);
            this.splitContainer1.SplitterDistance = 270;
            this.splitContainer1.TabIndex = 12;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.splitContainer1);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(666, 564);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Graph";
            // 
            // HighFrequencyAquisitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Name = "HighFrequencyAquisitionControl";
            this.Size = new System.Drawing.Size(1030, 564);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSampleTrigger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTrigger)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartHighAcquisition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAcquisition)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lstReading;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colValue;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboDevice2;
        private System.Windows.Forms.ComboBox cboDevice1;
        private System.Windows.Forms.ComboBox cboReadDevice2;
        private System.Windows.Forms.ComboBox cboReadDevice1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cboCont;
        private System.Windows.Forms.CheckBox cbSave;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboMeasurment;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartHighAcquisition;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAcquisition;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox cboSample;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblUnitSerie2;
        private System.Windows.Forms.Label lblUnitSerie1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnColor1;
        private System.Windows.Forms.Button btnColor2;
        private System.Windows.Forms.ColorDialog colorDialogHighAcq;
        private System.Windows.Forms.Label lblTagSerie1;
        private System.Windows.Forms.Label lblTagSerie2;
        private System.Windows.Forms.CheckBox cboTrigger;
        private System.Windows.Forms.NumericUpDown numTrigger;
        private System.Windows.Forms.NumericUpDown numSampleTrigger;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}
