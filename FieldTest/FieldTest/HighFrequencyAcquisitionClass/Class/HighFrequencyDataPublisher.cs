﻿using FieldTest.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.HighFrequencyAcquisitionClass.Class
{
    public class HighFrequencyDataPublisher
    {

        public event EventHandler<HighFrequencyDataEventArgs> RaiseDataEvent;


        public HighFrequencyDataPublisher()
        { }

        public void PublishData(List<Coordinate> listSerie1, List<Coordinate> listSerie2, SortedList<string, double> listMoy)
        {
            onRaiseDataEvent(new HighFrequencyDataEventArgs(listSerie1, listSerie2, listMoy));
        }

        protected virtual void onRaiseDataEvent(HighFrequencyDataEventArgs e)
        {
            RaiseDataEvent?.Invoke(this, e);
        }

    }

}

