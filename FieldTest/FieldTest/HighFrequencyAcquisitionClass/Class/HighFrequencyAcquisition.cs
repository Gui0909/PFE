﻿using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.FFTClass;
using FieldTest.HighFrequencyAcquisitionClass.Controls;
using FieldTest.MainClass.Class;
using FieldTest.WriterClass;
using System;
using System.Collections.Generic;
using System.Threading;

namespace FieldTest.HighFrequencyAcquisitionClass.Class
{
    public class HighFrequencyAcquisition
    {
        private Thread thread;
        private WriterHighData writer;
        private bool isReading;
        private List<SortedList<string, SortedList<string, Coordinate>>> listLastAllData;
        private SortedList<string, WriterTriggerData> listWriterError;
        private HighFrequencyAquisitionControl control;

        public Plot Serie1 { get; set; }
        public Plot Serie2 { get; set; }
        public MEASUREMENT Measurement { get; set; }
        public bool EnableWritting { get; set; }
        public bool EnableTriggerError { get; set; }
        public bool IsContinuous { get; set; }
        public int NumberSample { get; set; }
        public double ThresholdTrigger { get; set; }
        public int NumberSampleTrigger { get; set; }

        public HighFrequencyDataPublisher DataPublisher { get; set; }


        public enum MEASUREMENT : int { DB = 0, METRIC = 1}

        private const string NONE = "None";

        public HighFrequencyAcquisition(HighFrequencyAquisitionControl controlP, HighFrequencyDataPublisher dataPublisher)
        {
            this.NumberSample = 2048;
            this.IsContinuous = false;
            this.EnableWritting = true;
            this.EnableTriggerError = false;
            this.control = controlP;
            this.DataPublisher = dataPublisher;
        }

        public void SetPlot1(string device, string readDevice)
        {
            if (readDevice != NONE)
                Serie1 = new Plot(device, readDevice);
            else
                Serie1 = null;
        }

        public void SetPlot2(string device, string readDevice)
        {
            if (readDevice != NONE)
                Serie2 = new Plot(device, readDevice);
            else
                Serie2 = null;
        }

        public SortedList<string, ADevicePortMapped> GetListMappedPort()
        {
            return ApplicationManager.Instance.ListPortMapping;
        }

        public ADevicePortMapped GetMappedPort(string device)
        {
            return ApplicationManager.Instance.ListPortMapping[device];
        }

        public ADeviceReading GetReadDevice(string device, string readDevice)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading[readDevice];
        }

        public SortedList<string, ADeviceReading> GetListReadDevice(string device)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading;
        }


        private void closeWriterErrorDevice()
        {
            foreach (WriterTriggerData writerError in this.listWriterError.Values)
            {
                if (writerError.IsWritting)
                {
                    writerError.StopWritting();
                }
            }

            this.listWriterError.Clear();
            this.listLastAllData.Clear();
        }

        public void StopRead()
        {

            if (writer != null)
                writer.StopWritting();

            this.closeWriterErrorDevice();

            isReading = false;
           // threadPortDectecting.Join();

            ApplicationManager.Instance.StopReadThread();
        }

        //Set les key de la sortedlist pour contenir tous les writers des trigger error
        private void setWriterThresholdError()
        {
            listLastAllData = new List<SortedList<string, SortedList<string, Coordinate>>>();
            listWriterError = new SortedList<string, WriterTriggerData>();

            foreach (string device in ApplicationManager.Instance.ListPortMapping.Keys)
            {
                foreach (string readDevice in ApplicationManager.Instance.GetPortMapped(device).ListDeviceReading.Keys)
                {
                    if (ApplicationManager.Instance.GetAReadDevice(device, readDevice).IsEnableHighAcquisition)
                    {
                        this.listWriterError.Add(device + "-" + readDevice, new WriterTriggerData());
                    }
                }
              
            }
        }

        public void StartRead()
        {

            ApplicationManager.Instance.StartReadThread();

            setWriterThresholdError();

            thread = new Thread(read);
            isReading = true;
            thread.Start();

            if (EnableWritting)
                writer = new WriterHighData(ApplicationManager.Instance.ListPortMapping, ApplicationManager.Instance.FileSetting, Acquisition.TYPE.HIGH);
            else
                writer = null;
        }

        public bool ValidHighAcquisition(string device, string readDevice)
        {
            return ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading[readDevice].IsEnableHighAcquisition;
        }

        //Set les key de la sortedlist pour contenir tous les moyennes de données
        private void setDevicesListMoyenne(ref SortedList<string, double> listMoyenne)
        {
            foreach (string device in ApplicationManager.Instance.ListPortMapping.Keys)
            {
               
                foreach (string readDevice in ApplicationManager.Instance.ListPortMapping[device].ListDeviceReading.Keys)
                {
                    if (ValidHighAcquisition(device, readDevice))
                    {
                        listMoyenne.Add(device + "-" + readDevice, 0);
                    }
                }
            }
        }

        //Retourne une liste contenant seulement les données des lectures à haute fréquence
        private SortedList<string, SortedList<string, Coordinate>> getListHighAcquisition(SortedList<string, SortedList<string, Coordinate>> listAllData)
        {
            SortedList<string, SortedList<string, Coordinate>> listHighAcqui = new SortedList<string, SortedList<string, Coordinate>>();

            foreach (string device in listAllData.Keys)
            {
                SortedList<string, Coordinate> listCoord = new SortedList<string, Coordinate>();

                foreach (string readDevice in listAllData[device].Keys)
                {
                    if (ApplicationManager.Instance.GetAReadDevice(device, readDevice).IsEnableHighAcquisition)
                        listCoord.Add(readDevice, listAllData[device][readDevice]);
                }

                if (listCoord.Count > 0)
                    listHighAcqui.Add(device, listCoord);
            }
            return listHighAcqui;
        }

        //Ecrit les lignes de trigger error
        private void writeTriggerError(SortedList<string, SortedList<string, Coordinate>> listHighAcqui)
        {

            foreach (string device in listHighAcqui.Keys)
            {
                string tag = ApplicationManager.Instance.GetPortMapped(device).DeviceDescription;

                foreach (string readDevice in listHighAcqui[device].Keys)
                {
                    Coordinate coord = listHighAcqui[device][readDevice];
                    string key = device + "-" + readDevice;
                    string unit = ApplicationManager.Instance.GetAReadDevice(device, readDevice).GetConcreteUnit();

                    if (this.EnableTriggerError && this.ValidHighAcquisition(device, readDevice))
                    {
                        if (coord.Data >= this.ThresholdTrigger)
                        {
                            List<Coordinate> listLastCoord = new List<Coordinate>();

                            foreach (SortedList<string, SortedList<string, Coordinate>> listData in this.listLastAllData)
                            {
                                listLastCoord.Add(listData[device][readDevice]);
                            }

                            this.listWriterError[key].WriteSeuilLine(ApplicationManager.Instance.FileSetting, key, tag, unit, listLastCoord, coord, this.ThresholdTrigger);

                        }
                        else if (this.listWriterError[key].IsWritting && this.listWriterError[key].NumberLine <= this.NumberSampleTrigger)
                        {
                            this.listWriterError[key].WriteDataLine(listHighAcqui[device][readDevice]);
                        }
                    }
                }
            }

            fillListAllData(listHighAcqui);

        }

        private void fillListAllData(SortedList<string, SortedList<string, Coordinate>> listHighAcqui)
        {
            if (this.EnableTriggerError)
            {
                this.listLastAllData.Add(listHighAcqui);

                if (this.listLastAllData.Count > this.NumberSampleTrigger)
                {
                    int countToRemove = this.listLastAllData.Count - this.NumberSampleTrigger;

                    for (int i = 0; i < countToRemove; i++)
                    {
                        this.listLastAllData.RemoveAt(0);
                    }
                }
            }
        }

        private void read()
        {
            
            int index = -1;

            SortedList<int, SortedList<string, SortedList<string, Coordinate>>> dataBuffer = new SortedList<int, SortedList<string, SortedList<string, Coordinate>>>();
            SortedList<string, double> listMoyenne = new SortedList<string, double>();

            setDevicesListMoyenne(ref listMoyenne);

            while (isReading)
            {
               
                SortedList<string, SortedList<string, Coordinate>> listAllData = ApplicationManager.Instance.ExtractAllData();
                SortedList<string, SortedList<string, Coordinate>> listHighAcqui = this.getListHighAcquisition(listAllData);

                if (listHighAcqui.Count != 0)
                {

                    this.writeTriggerError(listHighAcqui);

                    index++;
                    dataBuffer.Add(index, listHighAcqui);

                    if (dataBuffer.Count == NumberSample)
                    {
            
                        SortedList<string, double> listRms = new SortedList<string, double>();
                        List<Coordinate> listDataSerie1 = new List<Coordinate>();
                        List<Coordinate> listDataSerie2 = new List<Coordinate>();

                        control.ClearPoints();

                        performFFT(dataBuffer, listRms);

                        performDataReScale(dataBuffer, listMoyenne, listRms, listDataSerie1, listDataSerie2);

                        this.DataPublisher.PublishData(listDataSerie1, listDataSerie2, listMoyenne);

                        if (!IsContinuous)
                        {
                            control.UpdateControl();
                            StopRead();
                            
                        }

                        index = -1;
                        dataBuffer = new SortedList<int, SortedList<string, SortedList<string, Coordinate>>>();
                    }
                }
            }
        }

        //Set le timeStamp, les frequences, etc des donnees (Dans le fond change les donnée selon les paramètres demandés db, metric, etc)
        private void performDataReScale(SortedList<int, SortedList<string, SortedList<string, Coordinate>>> dataBuffer, SortedList<string, double> listMoyenne, SortedList<string, double> listRms, List<Coordinate> listDataSerie1, List<Coordinate> listDataSerie2)
        {
            double freqTmp = 0;

            for (int ind = 0; ind < dataBuffer.Count; ind++)
            {

                foreach (string device in dataBuffer[ind].Keys)
                {

                    freqTmp = (((double)(int)ApplicationManager.Instance.ListPortMapping[device].DeviceSetting.SampleRate) / (double)NumberSample) / 1000;

                    foreach (string readDevice in dataBuffer[ind][device].Keys)
                    {
                        if (ValidHighAcquisition(device, readDevice))
                        {
                            listMoyenne[device + "-" + readDevice] += (dataBuffer[ind][device][readDevice].Data / dataBuffer.Count);
                            dataBuffer[ind][device][readDevice].Frequency = ind * freqTmp;

                            dataBuffer[ind][device][readDevice].Amplitude = dataBuffer[ind][device][readDevice].Amplitude / listRms[device + "|" + readDevice];

                            if (Measurement == MEASUREMENT.DB)
                            {
                                if (dataBuffer[ind][device][readDevice].Amplitude != 0)
                                    dataBuffer[ind][device][readDevice].Amplitude = 20 * Math.Log10(Math.Abs(dataBuffer[ind][device][readDevice].Amplitude));
                            }

                            if (ind >= (dataBuffer.Count / 2))
                            {
                                dataBuffer[ind][device][readDevice].Amplitude = double.NaN;
                                dataBuffer[ind][device][readDevice].Frequency = double.NaN;
                            }

                            if (dataBuffer[ind][device][readDevice].Frequency != 0)
                                dataBuffer[ind][device][readDevice].TimeStamp = ind * (double)(1 / (double)(NumberSample / 2));
                            else
                                dataBuffer[ind][device][readDevice].TimeStamp = 0;

                            if (Serie1 != null && device == Serie1.TagDevice && readDevice == Serie1.TagReadDevice)
                            {
                                listDataSerie1.Add(dataBuffer[ind][device][readDevice]);
                            }

                            if (Serie2 != null && device == Serie2.TagDevice && readDevice == Serie2.TagReadDevice)
                            {
                                listDataSerie2.Add(dataBuffer[ind][device][readDevice]);
                            }
                        }
                    }
                }


                if (writer != null)
                {
                    writer.WriteLine(dataBuffer[ind]);
                }
            }

        }

        private static void performFFT(SortedList<int, SortedList<string, SortedList<string, Coordinate>>> dataBuffer, SortedList<string, double> listRms)
        {
            foreach (string device in dataBuffer[0].Keys)
            {
                foreach (string readDevice in dataBuffer[0][device].Keys)
                {
                    float[] tabData = new float[dataBuffer.Count];

                    for (int i = 0; i < dataBuffer.Count; i++)
                    {
                        tabData[i] = (float)dataBuffer[i][device][readDevice].Data;
                    }

                    float[] tabFFT = FourierTransform.PerformFFT(tabData, dataBuffer.Count);

                    //RmS
                    calculRMS(dataBuffer, listRms, device, readDevice, tabFFT);
                }
            }
        }

        private static void calculRMS(SortedList<int, SortedList<string, SortedList<string, Coordinate>>> dataBuffer, SortedList<string, double> listRms, string device, string readDevice, float[] tabFFT)
        {
            double sumSquare = 0;

            for (int i = 0; i < dataBuffer.Count; i++)
            {
                dataBuffer[i][device][readDevice].Amplitude = (double)tabFFT[i];
                sumSquare += Math.Pow(dataBuffer[i][device][readDevice].Amplitude, 2);
            }

            listRms.Add(device + "|" + readDevice, Math.Sqrt(sumSquare / (dataBuffer.Count / 2)));
        }
    }
}
