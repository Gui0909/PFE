﻿using FieldTest.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.HighFrequencyAcquisitionClass.Class
{
    public class HighFrequencyDataEventArgs : EventArgs
    {
        private SortedList<string, double> listMoyenne;
        private List<Coordinate> listDataSerie1;
        private List<Coordinate> listDataSerie2;

        public HighFrequencyDataEventArgs(List<Coordinate> listSerie1, List<Coordinate> listSerie2, SortedList<string, double> listMoy)
        {
            this.listMoyenne = listMoy;
            this.listDataSerie1 = listSerie1;
            this.listDataSerie2 = listSerie2;
        }

        public SortedList<string, double> ListMoyenne
        {
            get { return listMoyenne; }
            set { listMoyenne = value; }
        }

        public List<Coordinate> ListDataSerie1
        {
            get { return listDataSerie1; }
            set { listDataSerie1 = value; }
        }

        public List<Coordinate> ListDataSerie2
        {
            get { return listDataSerie2; }
            set { listDataSerie2 = value; }
        }
    }
}
