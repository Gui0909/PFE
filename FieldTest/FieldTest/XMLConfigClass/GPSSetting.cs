﻿using System.ComponentModel;
using System.IO.Ports;

namespace FieldTest.XMLConfigClass
{
    [DefaultPropertyAttribute("GPS Setting"),
     ReadOnly(true)]
    public class GPSSetting : ADeviceSetting
    {
       
        public GPSSetting(DeviceSettingManager.DEVICE instrumentName, string instrumentTag, string descriptionPort, string comPort, DeviceSettingManager.BAUDRATE baudRate, DeviceSettingManager.SAMPLERATE sampRate, int dataBits, Parity parity, StopBits stopBits, DeviceSettingManager.UNIT unit)
            : base(instrumentName, instrumentTag, descriptionPort, comPort, baudRate, sampRate, dataBits, parity, stopBits, unit)
        {
        }

        public GPSSetting()
        { }
   
    }
}
