﻿using System.ComponentModel;
using System.IO.Ports;

namespace FieldTest.XMLConfigClass
{
    [DefaultPropertyAttribute("RPM Setting"),
    ReadOnly(true)]
    public class RPMSetting : ADeviceSetting
    {
        [CategoryAttribute("Device config"),
       DescriptionAttribute("RPMFactor device"),
        ReadOnly(false)]
       public int RPMFactor { get; set; }

        public RPMSetting(DeviceSettingManager.DEVICE instrumentName, string instrumentTag, string descriptionPort, string comPort, DeviceSettingManager.BAUDRATE baudRate, DeviceSettingManager.SAMPLERATE sampRate, int dataBits, Parity parity, StopBits stopBits, int rpmFactor, DeviceSettingManager.UNIT unit)
            : base(instrumentName, instrumentTag, descriptionPort, comPort, baudRate, sampRate, dataBits, parity, stopBits, unit)
       {
           this.RPMFactor = rpmFactor;
       }

       public RPMSetting()
       { }

    }
}
