﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace FieldTest.XMLConfigClass.ProprityGridConverter
{
    //Permet d'enlever certain élément d'enum que l'on veut pas afficher
    public class StopBitsConverter : UITypeEditor
    {
        private IWindowsFormsEditorService editorService;
        private bool cancel;

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            cancel = false;
            editorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            ListBox listBox = new ListBox();
            listBox.IntegralHeight = true;
            listBox.SelectionMode = SelectionMode.One;
            listBox.MouseClick += OnListBoxMouseClick;
            listBox.KeyDown += OnListBoxKeyDown;
            listBox.PreviewKeyDown += OnListBoxPreviewKeyDown;

            Type enumType = value.GetType();
            if (!enumType.IsEnum)
                throw new InvalidOperationException();

            foreach (FieldInfo fi in enumType.GetFields(BindingFlags.Public | BindingFlags.Static))
            {

                if ((fi.Name.ToString() == "None") || (fi.Name.ToString() == "OnePointFive"))
                    continue;

                int index = listBox.Items.Add(fi.Name);

                if (fi.Name == value.ToString())
                {
                    listBox.SetSelected(index, true);
                }
            }

            editorService.DropDownControl(listBox);

            if ((cancel) || (listBox.SelectedIndices.Count == 0))
                return value;

            return Enum.Parse(enumType, (string)listBox.SelectedItem);
        }

        private void OnListBoxPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                cancel = true;
                editorService.CloseDropDown();
            }
        }

        private void OnListBoxMouseClick(object sender, MouseEventArgs e)
        {
            int index = ((ListBox)sender).IndexFromPoint(e.Location);
            if (index >= 0)
            {
                editorService.CloseDropDown();
            }
        }

        private void OnListBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                editorService.CloseDropDown();
            }
        }
    }
}
