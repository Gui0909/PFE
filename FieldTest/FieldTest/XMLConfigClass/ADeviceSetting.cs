﻿
using FieldTest.XMLConfigClass.ProprityGridConverter;
using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.IO.Ports;
using System.Xml.Serialization;

namespace FieldTest.XMLConfigClass
{
    [Serializable]
    [XmlInclude(typeof(RPMSetting))]
    [XmlInclude(typeof(TPM2Setting))]
    [XmlInclude(typeof(TT10KSetting))]
    [XmlInclude(typeof(AnalogSetting))]
    [XmlInclude(typeof(GPSSetting))]
    [XmlRoot(ElementName = "ADeviceSetting")]
    public class ADeviceSetting
    {
        [CategoryAttribute("Port config"),
        DescriptionAttribute("Intrument's DeviceName."),
        ReadOnly(true)]
        public DeviceSettingManager.DEVICE InstrumentName { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("Intrument's Tag.")]
        public string InstrumentTag { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("DescriptionPort's port."),
        ReadOnly(true)]
        public string DescriptionPort { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("Window's name port."), 
        ReadOnly(true)]
        public string ComPort { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("Port's baudRate."),
        TypeConverter(typeof(DeviceConverter))]
        public DeviceSettingManager.BAUDRATE BaudRate { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("Intrument's Tag."),
        TypeConverter(typeof(DeviceConverter))]
        public DeviceSettingManager.SAMPLERATE SampleRate { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("Port's dataBits."),
         ReadOnly(true)]
        public int DataBits { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("Port's parity."),
        Editor(typeof(ParityConverter), typeof(UITypeEditor)),
         ReadOnly(true)]
        public Parity Parity { get; set; }

        [CategoryAttribute("Port config"),
        DescriptionAttribute("Port's stopBits."),
        Editor(typeof(StopBitsConverter), typeof(UITypeEditor)),
         ReadOnly(true)]
        public StopBits StopBits { get; set; }

        [CategoryAttribute("Device config"),
        DescriptionAttribute("Port's unit.")]
        public DeviceSettingManager.UNIT Unit { get; set; }

        public ADeviceSetting(DeviceSettingManager.DEVICE instrumentName, string instrumentTag, string descriptionPort, string comPort, DeviceSettingManager.BAUDRATE baudRate, DeviceSettingManager.SAMPLERATE sampRate, int dataBits, Parity parity, StopBits stopBits, DeviceSettingManager.UNIT unit)
        {
            this.InstrumentName = instrumentName;
            this.InstrumentTag = instrumentTag;
            this.DescriptionPort = descriptionPort;
            this.ComPort = comPort;
            this.BaudRate = baudRate;
            this.SampleRate = sampRate;
            this.DataBits = dataBits;
            this.Parity = parity;
            this.StopBits = stopBits;
            this.Unit = unit;
        }

        public ADeviceSetting()
        {
        }
    }
}
