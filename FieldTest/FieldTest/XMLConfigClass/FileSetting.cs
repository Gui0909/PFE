﻿
namespace FieldTest.XMLConfigClass
{
    public class FileSetting
    {
        public string TimeAcquisition { get; set; }
        public string HighAcquisition { get; set; }
        public string FolderPath { get; set; }

        public FileSetting(string timeAcquisition, string highAcquisition, string folderPath)
        {
            this.TimeAcquisition = timeAcquisition;
            this.HighAcquisition = highAcquisition;
            this.FolderPath = folderPath;
        }

        public FileSetting()
        {}

    }
}
