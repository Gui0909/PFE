﻿using System.ComponentModel;
using System.IO.Ports;

namespace FieldTest.XMLConfigClass
{
    [DefaultPropertyAttribute("ADAM4017 Setting")]
    public class AnalogSetting : ADeviceSetting
    {
        [CategoryAttribute("Device config"),
        DescriptionAttribute("Volt device")]
        public DeviceSettingManager.VOLT Volt { get; set; }

         public AnalogSetting(DeviceSettingManager.DEVICE instrumentName, string instrumentTag, string descriptionPort, string comPort, DeviceSettingManager.BAUDRATE baudRate, DeviceSettingManager.SAMPLERATE sampRate, int dataBits, Parity parity, StopBits stopBits, DeviceSettingManager.VOLT volt, DeviceSettingManager.UNIT unit)
            : base(instrumentName, instrumentTag, descriptionPort, comPort, baudRate, sampRate, dataBits, parity, stopBits, unit)
        {
            this.Volt = volt;
        }

         public AnalogSetting()
        { }
    }
}
