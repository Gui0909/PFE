﻿using FieldTest.XMLConfigClass.ProprityGridConverter;
using System.ComponentModel;
using System.IO.Ports;

namespace FieldTest.XMLConfigClass
{
    [DefaultPropertyAttribute("TPM2 Setting")]
    public class TPM2Setting : ADeviceSetting
    {

        [CategoryAttribute("Device config"),
        DescriptionAttribute("Port's Gain."),
        TypeConverter(typeof(DeviceConverter))]
        public DeviceSettingManager.GAIN Gain { get; set; }

        [CategoryAttribute("Device config"),
        DescriptionAttribute("Port's Shunt.")]
        public DeviceSettingManager.SHUNT Shunt { get; set; }

        [CategoryAttribute("Device config"),
        DescriptionAttribute("Port's gauge factor.")]
        public double GaugeFactor { get; set; }

        [CategoryAttribute("Device config"),
       DescriptionAttribute("Port's modulus elasticity."),
       DisplayName("ModulusElasticity (N/mm2 or Mpsi)")]
        public int ModulusElasticity { get; set; }

        [CategoryAttribute("Device config"),
        DescriptionAttribute("Port's outside diameter."),
        DisplayName("OutsideDiameter (mm or inches)")]
        public double OutsideDiameter { get; set; }

        [CategoryAttribute("Device config"),
        DescriptionAttribute("Port's inside diameter."),
       DisplayName("InsideDiameter (mm or inches)")]
        public double InsideDiameter { get; set; }

        [CategoryAttribute("Device config"),
        DescriptionAttribute("Port's poisson ratio.")]
        public double PoissonRatio { get; set; }

        public TPM2Setting(DeviceSettingManager.DEVICE instrumentName, string instrumentTag, string descriptionPort, string comPort, DeviceSettingManager.BAUDRATE baudRate, DeviceSettingManager.SAMPLERATE sampRate, int dataBits, Parity parity, StopBits stopBits, DeviceSettingManager.GAIN gain, DeviceSettingManager.SHUNT shunt, int rpmThres, int ppr, double gf, int e, double od, double id, double v, DeviceSettingManager.UNIT unit)
            : base(instrumentName, instrumentTag, descriptionPort, comPort, baudRate, sampRate, dataBits, parity, stopBits, unit)
        {

            this.Gain = gain;
            this.Shunt = shunt;

            this.GaugeFactor = gf;
            this.ModulusElasticity = e;
            this.OutsideDiameter = od;
            this.InsideDiameter = id;
            this.PoissonRatio = v;

        }

        public TPM2Setting()
        { }

    }
}
