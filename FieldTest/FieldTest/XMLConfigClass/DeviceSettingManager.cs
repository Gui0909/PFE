﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Xml.Serialization;

namespace FieldTest.XMLConfigClass
{
    [Serializable]
    [XmlRoot(ElementName = "ConfigSetting")]
    public class DeviceSettingManager
    {

        [XmlElement("ListDeviceSetting")]
        public List<ADeviceSetting> ListDeviceSetting { get; set; }

        public FileSetting File { get; set; }

        public enum DEVICE : int { GPS = 0, RPM = 1, TPM2 = 2, TT10K_1 = 3, TT10K_2 = 4, ANALOG = 5 }
        public enum UNIT : int { NONE = 0,  METRIC = 1, ENGLISH = 2 }

        public enum BAUDRATE : int { [Description("460800")] RATE460K = 460800, [Description("230400")] RATE230K = 230400, [Description("115200")] RATE115K = 115200, [Description("57600")] RATE57K = 57600, [Description("38400")] RATE38K = 38400, [Description("28800")] RATE28K = 28800, [Description("14400")] RATE14K = 14400, [Description("9600")] RATE9600 = 9600, [Description("4800")] RATE4800 = 4800, [Description("2400")] RATE2400 = 2400, [Description("1200")] RATE1200 = 1200 };
        public enum SAMPLERATE : int { NONE = 0, [Description("4800")] RATE4800 = 4800, [Description("2400")] RATE2400 = 2400, [Description("1200")] RATE1200 = 1200, [Description("600")] RATE600 = 600, [Description("300")] RATE300 = 300, [Description("150")] RATE150 = 150, [Description("75")] RATE75 = 75, [Description("37")] RATE37 = 37, [Description("18")] RATE18 = 18, [Description("10")] RATE10 = 10, [Description("9")] RATE9 = 9, [Description("1")] RATE1 = 1 };
        public enum SHUNT : int { SHUNTOFFBOTH = 0, SHUNT1 = 1, SHUNT2 = 2 };
        public enum GAIN : int { [Description("1")] GAIN1 = 1, [Description("2")] GAIN2 = 2, [Description("4")] GAIN4 = 4, [Description("8")] GAIN8 = 8, [Description("16")] GAIN16 = 16, [Description("32")] GAIN32 = 32, [Description("64")] GAIN64 = 64, [Description("128")] GAIN128 = 128 };
        public enum VOLT : int { [Description("10V")] V10 = 0, [Description("5V")] V5 = 1, [Description("1V")] V1 = 2, [Description("500mV")] mV500 = 3, [Description("150mV")] mV150 = 4, [Description("20mA")] mA20 = 5 };

        public DeviceSettingManager(bool reset)
        {

            if (reset == true)
            {
                this.ListDeviceSetting = new List<ADeviceSetting>();

                this.ListDeviceSetting.Add(new RPMSetting(DEVICE.RPM, "RPM", string.Empty, string.Empty, BAUDRATE.RATE115K, SAMPLERATE.NONE, 8, Parity.None, StopBits.One, 1, UNIT.NONE));
                this.ListDeviceSetting.Add(new GPSSetting(DEVICE.GPS, "GPS 1", string.Empty, string.Empty, BAUDRATE.RATE4800, SAMPLERATE.RATE1, 8, Parity.None, StopBits.One, UNIT.NONE));
                this.ListDeviceSetting.Add(new TPM2Setting(DEVICE.TPM2, "TPM2 1", string.Empty, string.Empty, BAUDRATE.RATE115K, SAMPLERATE.RATE1200, 8, Parity.None, StopBits.One, GAIN.GAIN1, SHUNT.SHUNTOFFBOTH, 60, 1, 2.10, 30, 1.9, 1.7, 0.3, UNIT.ENGLISH));
                this.ListDeviceSetting.Add(new TT10KSetting(DEVICE.TT10K_1, "TT10K 1", string.Empty, string.Empty, BAUDRATE.RATE115K, SAMPLERATE.RATE2400, 8, Parity.None, StopBits.One, GAIN.GAIN1, SHUNT.SHUNTOFFBOTH, 60, 1, 2.10, 30, 1.9, 1.7, 0.3, UNIT.ENGLISH));
                this.ListDeviceSetting.Add(new TT10KSetting(DEVICE.TT10K_2, "TT10K 1", string.Empty, string.Empty, BAUDRATE.RATE115K, SAMPLERATE.RATE2400, 8, Parity.None, StopBits.One, GAIN.GAIN1, SHUNT.SHUNTOFFBOTH, 60, 1, 2.10, 30, 1.9, 1.7, 0.3, UNIT.ENGLISH));
                this.ListDeviceSetting.Add(new AnalogSetting(DEVICE.ANALOG, "ANALOG", string.Empty, string.Empty, BAUDRATE.RATE38K, SAMPLERATE.RATE10, 8, Parity.None, StopBits.One, VOLT.V1, UNIT.NONE));

                this.File = new FileSetting("Default_Time_ACQ", "Default_HF_ACQ", System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            }
        }

        //Constructeur pour sérialisation
        public DeviceSettingManager()
        {

        }

        public ADeviceSetting GetDeviceSetting(string tagDevice)
        {
            foreach (ADeviceSetting devSet in this.ListDeviceSetting)
            {
                if (tagDevice == devSet.InstrumentName.ToString())
                {
                    return devSet;
                }
            }

            return null;
        }

        public void SetDeviceSetting(string nameDevice, ADeviceSetting deviceSet)
        {
            for (int i = 0; i < this.ListDeviceSetting.Count; i++)
            {
                if (nameDevice == this.ListDeviceSetting[i].InstrumentName.ToString())
                {
                    this.ListDeviceSetting[i] = deviceSet;
                    break;
                }
            }
        }


    }
}
