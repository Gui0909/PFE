﻿using FieldTest.DataClass;
using FieldTest.DevicePortMapClass;
using FieldTest.DevicePortMapClass.TT10KPortClass;
using FieldTest.WriterClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.Threading;

namespace FieldTest.MainClass.Class
{
    public class ApplicationManager
    {

        private static ApplicationManager instance;
        private Thread thread;
        private bool isReading;
        private const int NUMBER_SAMPLE_ERROR = 20;
        private SortedList<string, WriterErrorData> listWriterError;

        public SortedList<string, ADevicePortMapped> ListPortMapping { get; set; }
        public FileSetting FileSetting { get; set; }
        public List<SortedList<string, SortedList<string, Coordinate>>> ListLastAllData { get; set; }

        //singleton
        public static ApplicationManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApplicationManager();
                }
                return instance;
            }
        }

        public int GetCountPortMapped()
        {
            return this.ListPortMapping.Count;
        }

        public ADeviceReading GetAReadDevice(string device, string readDevice)
        {
            return ListPortMapping[device].ListDeviceReading[readDevice];
        }

        public ADevicePortMapped GetPortMapped(string device)
        {
            return ListPortMapping[device];
        }

        public bool IsPortDeviceOpen(string device)
        {
            return ListPortMapping[device].IsOpenPort();
        }

        public void ClosePortDevice()
        {
            foreach (ADevicePortMapped port in ListPortMapping.Values)
            {
                if (port.IsOpenPort())
                    port.ClosePort();
            }
        }

        public bool IfContainsDevice(string device)
        {
            return ListPortMapping.ContainsKey(device);
        }

        public void OpenPortDevice()
        {
            foreach (ADevicePortMapped port in ListPortMapping.Values)
            {
                port.OpenPort();
            }
        }

        //Intialise et remplit la liste de writer device error
        private void fillListWriterDevice()
        {
            this.listWriterError = new SortedList<string, WriterErrorData>();

            foreach (ADevicePortMapped port in ListPortMapping.Values)
            {
                if (port.IsFaultDetected)
                {
                    this.listWriterError.Add(port.DeviceName, new WriterErrorData());
                }
            }
            
        }

        public void StartReadThread()
        {
            this.fillListWriterDevice();

            thread = new Thread(ReadThread);
            this.ListLastAllData = new List<SortedList<string, SortedList<string, Coordinate>>>();
            isReading = true;
            thread.Start();
        }


        private void closeWriterDevice()
        {
            foreach (WriterErrorData writer in this.listWriterError.Values)
            {
                if (writer.IsWritting)
                {
                    writer.StopWritting();
                    writer.IsWritting = false;
                }
            }
        }

        //Permet d'arreter les threads vérifiant la reconnexion de cable usb
        private void stopAllThreadPortCheck()
        {
            foreach (ADevicePortMapped devicePort in this.ListPortMapping.Values)
            {
                if (devicePort.IsCheckingPort)
                    devicePort.StopThreadPortDectection();
            }
        }

        public void StopReadThread()
        {
            this.closeWriterDevice();
            this.stopAllThreadPortCheck();
            isReading = false;
            thread.Join();
        }

        private void ReadThread()
        {
            try
            {
                while (isReading != false)
                {
                    foreach (ADevicePortMapped port in this.ListPortMapping.Values)
                    {
                        port.ReadThread();
                    }
                }
            }
            catch
            {

            }
        }

        private void writeDeviceError(ADevicePortMapped port, SortedList<string, Coordinate> listRawData, SortedList<string, SortedList<string, Coordinate>> listLastData)
        {
            if (port.IsFaultDetected)
            {
                //Écrit l'erreur
                if (port.GetListError().Count > 0)
                {
                    this.listWriterError[port.DeviceName].WriteErrorLine(this.FileSetting, port, this.ListLastAllData, listRawData, port.GetListError());

                }
                //Écrit les data suivant l'erreur
                else if (this.listWriterError[port.DeviceName].IsWritting && this.listWriterError[port.DeviceName].NumberLine < NUMBER_SAMPLE_ERROR)
                {
                    this.listWriterError[port.DeviceName].WriteDataLine(this.FileSetting, port, listRawData);
                }
                //Si le nombre de data après l'erreur est atteinte le fichier est fermer et on réinitialise la liste
                else if (this.listWriterError[port.DeviceName].IsWritting && this.listWriterError[port.DeviceName].NumberLine == NUMBER_SAMPLE_ERROR)
                {
                    this.listWriterError[port.DeviceName].WriteDataLine(this.FileSetting, port, listRawData);

                    this.closeWriterDevice();
                    this.fillListWriterDevice();
                }

                listLastData.Add(port.DeviceName, listRawData);
            }
        }

        public SortedList<string, SortedList<string, Coordinate>> ExtractAllData()
        {
           
                SortedList<string, SortedList<string, Coordinate>> listAllData = new SortedList<string, SortedList<string, Coordinate>>();
                SortedList<string, SortedList<string, Coordinate>> listLastData = new SortedList<string, SortedList<string, Coordinate>>();

                try
                {
                    
                    long x = DateTime.Now.ToFileTime();
                    double rpm1 = double.NaN;
                    double rpm2 = double.NaN;

                    foreach (ADevicePortMapped port in ListPortMapping.Values)
                    {
                        SortedList<string, Coordinate> listRawData;

                        //Gestion spéciel rpm et tt10k à revoir ou à enlever si le tt10k disparait
                        if (port.DeviceName == DeviceSettingManager.DEVICE.TT10K_1.ToString())
                        {
                            listRawData = ((TT10KPortMapped)port).ReadData(rpm1);
                            rpm1 = double.NaN;
                        }
                        else if (port.DeviceName == DeviceSettingManager.DEVICE.TT10K_2.ToString())
                        {
                            listRawData = ((TT10KPortMapped)port).ReadData(rpm2);
                            rpm2 = double.NaN;
                        }
                        //Normalement pour tous les appareils
                        else
                        {
                            listRawData = port.ExtractData();
                        }

                        //Gestion spéciel rpm et tt10k à revoir ou à enlever si le tt10k disparait
                        if (port.DeviceName == DeviceSettingManager.DEVICE.RPM.ToString())
                        {
                            rpm1 = listRawData["RPM 1"].Data;
                            rpm2 = listRawData["RPM 2"].Data;
                        }

                        if (listRawData.Count != 0)
                        {
                            foreach (Coordinate pt in listRawData.Values)
                            {
                                pt.RunningTime = DateTime.FromFileTime(x);
                            }

                            listAllData.Add(port.DeviceName, listRawData);
                        }

                        this.writeDeviceError(port, listRawData, listLastData);

                    }

                    if (this.ListLastAllData.Count == 20)
                    {
                        this.ListLastAllData.RemoveAt(0);
                    }

                    this.ListLastAllData.Add(listLastData);

                    return listAllData;
                }
                catch
                {
                    return listAllData;
                }
        }
        
  

    }
}
