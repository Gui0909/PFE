﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FTD2XX_NET;
using FieldTest.SetupClass.Controls;
using FieldTest.HighFrequencyAcquisitionClass.Controls;
using FieldTest.TimedAcquisitionClass.Controls;
using FieldTest.AnalysisClass.Controls;
using FieldTest.ErrorClass;
using FieldTest.MainClass.Class;
using FieldTest.Read_Device.Controls;
using System.Threading;
using FieldTest.TimedAcquisitionClass.Class;
using FieldTest.HighFrequencyAcquisitionClass.Class;
using FieldTest.TriggerAnalysisClass;

namespace FieldTest.MainClass.Controls
{
    public partial class MainControl : Form
    {
        private SetupControl ctlSetup;
        private HighFrequencyAquisitionControl ctlHighAcquisition;
        private TimedAcquisitionControl ctlTimedAcquisition;
        private AnalysisControl ctlAnalysis;
        private ErrorControl ctlError;
        private TriggerControl ctlTrigger;


        public MainControl()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            
        }

        private void initialiseSetup()
        {

            this.panOperation.Controls.Clear();
            this.ctlSetup = new SetupControl();
            this.ctlSetup.Dock = DockStyle.Fill;
            this.panOperation.Controls.Add(ctlSetup);
        }

        private void btnTimed_Click(object sender, EventArgs e)
        {
            if (ApplicationManager.Instance.ListPortMapping != null)
            {
                this.panOperation.Controls.Clear();
                this.ctlTimedAcquisition = new TimedAcquisitionControl();
                this.ctlTimedAcquisition.Dock = DockStyle.Fill;
                this.panOperation.Controls.Add(ctlTimedAcquisition);
            }
            else
            { 
                 MessageBox.Show("No FTDI device found.", "Device missing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            this.initialiseSetup();
        }

        private void MainControl_Load(object sender, EventArgs e)
        {
            this.initialiseSetup();
        }

        private void btnFrequency_Click(object sender, EventArgs e)
        {
            this.panOperation.Controls.Clear();
            this.ctlHighAcquisition = new HighFrequencyAquisitionControl();
            this.ctlHighAcquisition.Dock = DockStyle.Fill;
            this.panOperation.Controls.Add(ctlHighAcquisition);
        }

        private void btnAnalysis_Click(object sender, EventArgs e)
        {
            this.panOperation.Controls.Clear();
            this.ctlAnalysis = new AnalysisControl();
            this.ctlAnalysis.Dock = DockStyle.Fill;
            this.panOperation.Controls.Add(ctlAnalysis);
        }

        private void MainControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.ctlSetup.StopAllThread();
        }

        private void panOperation_ControlRemoved(object sender, ControlEventArgs e)
        {

            this.ctlSetup.StopAllThread();
        }

        private void btnError_Click(object sender, EventArgs e)
        {
            this.panOperation.Controls.Clear();
            this.ctlError = new ErrorControl();
            this.ctlError.Dock = DockStyle.Fill;
            this.panOperation.Controls.Add(ctlError);
        }

        private void btnTrigger_Click(object sender, EventArgs e)
        {
            this.panOperation.Controls.Clear();
            this.ctlTrigger = new TriggerControl();
            this.ctlTrigger.Dock = DockStyle.Fill;
            this.panOperation.Controls.Add(ctlTrigger);
        }
    }
}
