﻿namespace FieldTest.MainClass.Controls
{
    partial class MainControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainControl));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTrigger = new System.Windows.Forms.Button();
            this.btnError = new System.Windows.Forms.Button();
            this.btnAnalysis = new System.Windows.Forms.Button();
            this.btnFrequency = new System.Windows.Forms.Button();
            this.btnTimed = new System.Windows.Forms.Button();
            this.btnSetup = new System.Windows.Forms.Button();
            this.panOperation = new System.Windows.Forms.Panel();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnTrigger);
            this.groupBox2.Controls.Add(this.btnError);
            this.groupBox2.Controls.Add(this.btnAnalysis);
            this.groupBox2.Controls.Add(this.btnFrequency);
            this.groupBox2.Controls.Add(this.btnTimed);
            this.groupBox2.Controls.Add(this.btnSetup);
            this.groupBox2.Location = new System.Drawing.Point(4, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1133, 99);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // btnTrigger
            // 
            this.btnTrigger.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTrigger.BackgroundImage")));
            this.btnTrigger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTrigger.Location = new System.Drawing.Point(872, 33);
            this.btnTrigger.Name = "btnTrigger";
            this.btnTrigger.Size = new System.Drawing.Size(105, 41);
            this.btnTrigger.TabIndex = 5;
            this.btnTrigger.UseVisualStyleBackColor = true;
            this.btnTrigger.Click += new System.EventHandler(this.btnTrigger_Click);
            // 
            // btnError
            // 
            this.btnError.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnError.BackgroundImage")));
            this.btnError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnError.Location = new System.Drawing.Point(996, 33);
            this.btnError.Name = "btnError";
            this.btnError.Size = new System.Drawing.Size(105, 41);
            this.btnError.TabIndex = 4;
            this.btnError.UseVisualStyleBackColor = true;
            this.btnError.Click += new System.EventHandler(this.btnError_Click);
            // 
            // btnAnalysis
            // 
            this.btnAnalysis.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAnalysis.BackgroundImage")));
            this.btnAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAnalysis.Location = new System.Drawing.Point(750, 33);
            this.btnAnalysis.Name = "btnAnalysis";
            this.btnAnalysis.Size = new System.Drawing.Size(105, 41);
            this.btnAnalysis.TabIndex = 3;
            this.btnAnalysis.UseVisualStyleBackColor = true;
            this.btnAnalysis.Click += new System.EventHandler(this.btnAnalysis_Click);
            // 
            // btnFrequency
            // 
            this.btnFrequency.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFrequency.BackgroundImage")));
            this.btnFrequency.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnFrequency.Location = new System.Drawing.Point(622, 33);
            this.btnFrequency.Name = "btnFrequency";
            this.btnFrequency.Size = new System.Drawing.Size(105, 41);
            this.btnFrequency.TabIndex = 2;
            this.btnFrequency.UseVisualStyleBackColor = true;
            this.btnFrequency.Click += new System.EventHandler(this.btnFrequency_Click);
            // 
            // btnTimed
            // 
            this.btnTimed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTimed.BackgroundImage")));
            this.btnTimed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimed.Location = new System.Drawing.Point(500, 33);
            this.btnTimed.Name = "btnTimed";
            this.btnTimed.Size = new System.Drawing.Size(105, 41);
            this.btnTimed.TabIndex = 1;
            this.btnTimed.UseVisualStyleBackColor = true;
            this.btnTimed.Click += new System.EventHandler(this.btnTimed_Click);
            // 
            // btnSetup
            // 
            this.btnSetup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSetup.BackgroundImage")));
            this.btnSetup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSetup.Location = new System.Drawing.Point(373, 33);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(105, 41);
            this.btnSetup.TabIndex = 0;
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // panOperation
            // 
            this.panOperation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panOperation.Location = new System.Drawing.Point(4, 115);
            this.panOperation.Name = "panOperation";
            this.panOperation.Size = new System.Drawing.Size(1133, 566);
            this.panOperation.TabIndex = 5;
            this.panOperation.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.panOperation_ControlRemoved);
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 684);
            this.Controls.Add(this.panOperation);
            this.Controls.Add(this.groupBox2);
            this.Name = "MainControl";
            this.Text = "FieldTest 3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainControl_FormClosing);
            this.Load += new System.EventHandler(this.MainControl_Load);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAnalysis;
        private System.Windows.Forms.Button btnFrequency;
        private System.Windows.Forms.Button btnTimed;
        private System.Windows.Forms.Button btnSetup;
        private System.Windows.Forms.Panel panOperation;
        private System.Windows.Forms.Button btnError;
        private System.Windows.Forms.Button btnTrigger;
    }
}

