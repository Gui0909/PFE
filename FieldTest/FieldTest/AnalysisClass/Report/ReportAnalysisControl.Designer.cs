﻿namespace FieldTest.AnalysisClass.Report
{
    partial class ReportAnalysisControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.CoordinateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.AverageReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.CoordinateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AverageReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // CoordinateBindingSource
            // 
            this.CoordinateBindingSource.DataSource = typeof(FieldTest.DataClass.Coordinate);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "NormalSerie1";
            reportDataSource1.Value = this.CoordinateBindingSource;
            reportDataSource2.Name = "NormalSerie2";
            reportDataSource2.Value = this.CoordinateBindingSource;
            reportDataSource3.Name = "Average";
            reportDataSource3.Value = this.AverageReportBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "FieldTest.Analysis.ReportNormal.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1064, 610);
            this.reportViewer1.TabIndex = 0;
            // 
            // AverageReportBindingSource
            // 
            this.AverageReportBindingSource.DataSource = typeof(FieldTest.AnalysisClass.Report.AverageReport);
            // 
            // ReportAnalysisControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 610);
            this.Controls.Add(this.reportViewer1);
            this.Name = "ReportAnalysisControl";
            this.Text = "ReportAnalysisControl";
            this.Load += new System.EventHandler(this.ReportAnalysisControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CoordinateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AverageReportBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource CoordinateBindingSource;
        private System.Windows.Forms.BindingSource AverageReportBindingSource;
    }
}