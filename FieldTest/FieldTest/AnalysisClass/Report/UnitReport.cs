﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.AnalysisClass.Report
{
    public class UnitReport
    {
        public string Unit1 { get; set; }
        public string Unit2 { get; set; }

        public UnitReport(string unit1, string unit2)
        {
            this.Unit1 = unit1;
            this.Unit2 = unit2;
        }
    }
}
