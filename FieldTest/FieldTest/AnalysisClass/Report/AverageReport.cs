﻿
namespace FieldTest.AnalysisClass.Report
{
    public class AverageReport
    {
        public double Average { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }

        public AverageReport(string name, string tag, double ave, string unit)
        {
            this.Average = ave;
            this.Name = name;
            this.Tag = tag;
            this.Unit = unit;
        }
    }
}
