﻿using FieldTest.DataClass;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace FieldTest.AnalysisClass.Report
{
    public partial class ReportAnalysisControl : Form
    {
        private List<Coordinate> listCoordSerie1;
        private List<Coordinate> listCoordSerie2;
        private List<Coordinate> listCoordHighSerie1;
        private List<Coordinate> listCoordHighSerie2;
        private List<AverageReport> listAverage;
        private SerieReport series;
        private UnitReport units;
        private Acquisition.TYPE acqui;

        public ReportAnalysisControl(List<Coordinate> listCoord1, List<Coordinate> listCoord2, List<Coordinate> listHighCoord1, List<Coordinate> listHighCoord2, SerieReport ser, UnitReport unit, List<AverageReport> listAve, Acquisition.TYPE acq)
        {
            InitializeComponent();

            this.listCoordSerie1 = listCoord1;
            this.listCoordSerie2 = listCoord2;
            this.listCoordHighSerie1 = listHighCoord1;
            this.listCoordHighSerie2 = listHighCoord2;

            this.units = unit;
            this.series = ser;
            this.listAverage = listAve;
            this.acqui = acq;
        }

        public ReportAnalysisControl(List<Coordinate> listCoord1, List<Coordinate> listCoord2, SerieReport ser, UnitReport unit, List<AverageReport> listAve, Acquisition.TYPE acq)
        {
            InitializeComponent();

            this.listCoordSerie1 = listCoord1;
            this.listCoordSerie2 = listCoord2;
            this.listAverage = listAve;
            this.series = ser;
            this.acqui = acq;
            this.units = unit;
        }

        private void loadNormalRapport()
        {
            string exeFolder = Application.StartupPath;

            //Ne pas oublier de changer les path pour la version de release
            string reportPath = exeFolder + @"\Report\ReportNormal.rdlc";

            reportViewer1.LocalReport.ReportPath = reportPath;
            reportViewer1.LocalReport.DataSources.Clear();

            List<SerieReport> listSeries = new List<SerieReport>();
            listSeries.Add(series);

            List<UnitReport> listUnits = new List<UnitReport>();
            listUnits.Add(units);

            ReportDataSource dataSet1 = new ReportDataSource("NormalSerie1", listCoordSerie1);
            ReportDataSource dataSet2 = new ReportDataSource("NormalSerie2", listCoordSerie2);
            ReportDataSource dataSet3 = new ReportDataSource("Average", listAverage);
            ReportDataSource dataSet4 = new ReportDataSource("Series", listSeries);
            ReportDataSource dataSet5 = new ReportDataSource("Units", listUnits);

            dataSet1.Value = listCoordSerie1;
            dataSet2.Value = listCoordSerie2;
            dataSet3.Value = listAverage;
            dataSet4.Value = listSeries;
            dataSet5.Value = listUnits;

            this.reportViewer1.LocalReport.DataSources.Add(dataSet1);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet2);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet3);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet4);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet5);

            this.reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void loadHighRapport()
        {

            string exeFolder = Application.StartupPath;

            //Ne pas oublier de changer les path pour la version de release
            string reportPath = exeFolder + @"\Report\ReportHigh.rdlc";

            //reportViewer1.LocalReport.ReportPath = @"C:\Git Projet\fieldtest\FieldTest\FieldTest\AnalysisClass\Report\ReportHigh.rdlc";
            reportViewer1.LocalReport.ReportPath = reportPath;
            reportViewer1.LocalReport.DataSources.Clear();

            List<SerieReport> listSeries = new List<SerieReport>();
            listSeries.Add(series);

            List<UnitReport> listUnits = new List<UnitReport>();
            listUnits.Add(units);

            ReportDataSource dataSet1= new ReportDataSource("NormalSerie1", listCoordSerie1);
            ReportDataSource dataSet2 = new ReportDataSource("NormalSerie2", listCoordSerie2);
            ReportDataSource dataSet3 = new ReportDataSource("HighSerie1", listCoordHighSerie1);
            ReportDataSource dataSet4 = new ReportDataSource("HighSerie2", listCoordHighSerie2);
            ReportDataSource dataSet5 = new ReportDataSource("Average", listAverage);
            ReportDataSource dataSet6 = new ReportDataSource("Series", listSeries);
            ReportDataSource dataSet7 = new ReportDataSource("Units", listUnits);


            dataSet1.Value = listCoordSerie1;
            dataSet2.Value = listCoordSerie2;
            dataSet3.Value = listCoordHighSerie1;
            dataSet4.Value = listCoordHighSerie2;
            dataSet5.Value = listAverage;
            dataSet6.Value = listSeries;
            dataSet7.Value = listUnits;


            this.reportViewer1.LocalReport.DataSources.Add(dataSet1);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet2);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet3);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet4);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet5);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet6);
            this.reportViewer1.LocalReport.DataSources.Add(dataSet7);

            this.reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void ReportAnalysisControl_Load(object sender, EventArgs e)
        {
            if (acqui == Acquisition.TYPE.NORMAL)
                loadNormalRapport();
            else
                loadHighRapport();

        }

       
    }
}
