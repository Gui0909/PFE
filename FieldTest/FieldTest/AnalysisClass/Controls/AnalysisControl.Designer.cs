﻿namespace FieldTest.AnalysisClass.Controls
{
    partial class AnalysisControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnalysisControl));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTagSerie2 = new System.Windows.Forms.Label();
            this.lblTagSerie1 = new System.Windows.Forms.Label();
            this.btnColor2 = new System.Windows.Forms.Button();
            this.btnColor1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.lblUnitSerie2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblUnitSerie1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboDevice2 = new System.Windows.Forms.ComboBox();
            this.cboDevice1 = new System.Windows.Forms.ComboBox();
            this.cboReadDevice2 = new System.Windows.Forms.ComboBox();
            this.cboReadDevice1 = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.btnReport = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstReading = new System.Windows.Forms.ListView();
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panGraph = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.colorDialogAnalysis = new System.Windows.Forms.ColorDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Location = new System.Drawing.Point(666, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(364, 564);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Config Graph";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblTagSerie2);
            this.groupBox2.Controls.Add(this.lblTagSerie1);
            this.groupBox2.Controls.Add(this.btnColor2);
            this.groupBox2.Controls.Add(this.btnColor1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.lblUnitSerie2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lblUnitSerie1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cboDevice2);
            this.groupBox2.Controls.Add(this.cboDevice1);
            this.groupBox2.Controls.Add(this.cboReadDevice2);
            this.groupBox2.Controls.Add(this.cboReadDevice1);
            this.groupBox2.Location = new System.Drawing.Point(6, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(350, 103);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plot";
            // 
            // lblTagSerie2
            // 
            this.lblTagSerie2.AutoSize = true;
            this.lblTagSerie2.Location = new System.Drawing.Point(55, 60);
            this.lblTagSerie2.Name = "lblTagSerie2";
            this.lblTagSerie2.Size = new System.Drawing.Size(22, 13);
            this.lblTagSerie2.TabIndex = 23;
            this.lblTagSerie2.Text = "(...)";
            // 
            // lblTagSerie1
            // 
            this.lblTagSerie1.AutoSize = true;
            this.lblTagSerie1.Location = new System.Drawing.Point(55, 16);
            this.lblTagSerie1.Name = "lblTagSerie1";
            this.lblTagSerie1.Size = new System.Drawing.Size(22, 13);
            this.lblTagSerie1.TabIndex = 22;
            this.lblTagSerie1.Text = "(...)";
            // 
            // btnColor2
            // 
            this.btnColor2.Location = new System.Drawing.Point(302, 74);
            this.btnColor2.Name = "btnColor2";
            this.btnColor2.Size = new System.Drawing.Size(45, 23);
            this.btnColor2.TabIndex = 21;
            this.btnColor2.Text = "Color";
            this.btnColor2.UseVisualStyleBackColor = true;
            this.btnColor2.Click += new System.EventHandler(this.btnColor2_Click);
            // 
            // btnColor1
            // 
            this.btnColor1.Location = new System.Drawing.Point(302, 31);
            this.btnColor1.Name = "btnColor1";
            this.btnColor1.Size = new System.Drawing.Size(45, 21);
            this.btnColor1.TabIndex = 20;
            this.btnColor1.Text = "Color";
            this.btnColor1.UseVisualStyleBackColor = true;
            this.btnColor1.Click += new System.EventHandler(this.btnColor1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(284, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Unit : ";
            // 
            // lblUnitSerie2
            // 
            this.lblUnitSerie2.AutoSize = true;
            this.lblUnitSerie2.Location = new System.Drawing.Point(316, 60);
            this.lblUnitSerie2.Name = "lblUnitSerie2";
            this.lblUnitSerie2.Size = new System.Drawing.Size(16, 13);
            this.lblUnitSerie2.TabIndex = 16;
            this.lblUnitSerie2.Text = "---";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(284, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Unit : ";
            // 
            // lblUnitSerie1
            // 
            this.lblUnitSerie1.AutoSize = true;
            this.lblUnitSerie1.Location = new System.Drawing.Point(316, 16);
            this.lblUnitSerie1.Name = "lblUnitSerie1";
            this.lblUnitSerie1.Size = new System.Drawing.Size(16, 13);
            this.lblUnitSerie1.TabIndex = 14;
            this.lblUnitSerie1.Text = "---";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(156, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "ReadDevice 2 : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(156, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "ReadDevice 1 : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Device 2 : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Device 1 : ";
            // 
            // cboDevice2
            // 
            this.cboDevice2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice2.FormattingEnabled = true;
            this.cboDevice2.Location = new System.Drawing.Point(6, 76);
            this.cboDevice2.Name = "cboDevice2";
            this.cboDevice2.Size = new System.Drawing.Size(147, 21);
            this.cboDevice2.TabIndex = 3;
            this.cboDevice2.SelectedIndexChanged += new System.EventHandler(this.cboDevice2_SelectedIndexChanged);
            // 
            // cboDevice1
            // 
            this.cboDevice1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDevice1.FormattingEnabled = true;
            this.cboDevice1.Location = new System.Drawing.Point(6, 32);
            this.cboDevice1.Name = "cboDevice1";
            this.cboDevice1.Size = new System.Drawing.Size(147, 21);
            this.cboDevice1.TabIndex = 2;
            this.cboDevice1.SelectedIndexChanged += new System.EventHandler(this.cboDevice1_SelectedIndexChanged);
            // 
            // cboReadDevice2
            // 
            this.cboReadDevice2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReadDevice2.FormattingEnabled = true;
            this.cboReadDevice2.Location = new System.Drawing.Point(159, 76);
            this.cboReadDevice2.Name = "cboReadDevice2";
            this.cboReadDevice2.Size = new System.Drawing.Size(142, 21);
            this.cboReadDevice2.TabIndex = 1;
            this.cboReadDevice2.SelectedIndexChanged += new System.EventHandler(this.cboReadDevice2_SelectedIndexChanged);
            // 
            // cboReadDevice1
            // 
            this.cboReadDevice1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReadDevice1.FormattingEnabled = true;
            this.cboReadDevice1.Location = new System.Drawing.Point(159, 32);
            this.cboReadDevice1.Name = "cboReadDevice1";
            this.cboReadDevice1.Size = new System.Drawing.Size(142, 21);
            this.cboReadDevice1.TabIndex = 0;
            this.cboReadDevice1.SelectedIndexChanged += new System.EventHandler(this.cboReadDevice1_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnLoad);
            this.groupBox4.Controls.Add(this.btnExport);
            this.groupBox4.Controls.Add(this.txtFileName);
            this.groupBox4.Controls.Add(this.btnReport);
            this.groupBox4.Location = new System.Drawing.Point(6, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(350, 200);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Operations";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "File name";
            // 
            // btnLoad
            // 
            this.btnLoad.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoad.BackgroundImage")));
            this.btnLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLoad.Image = ((System.Drawing.Image)(resources.GetObject("btnLoad.Image")));
            this.btnLoad.Location = new System.Drawing.Point(56, 19);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(53, 67);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExport.BackgroundImage")));
            this.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExport.Enabled = false;
            this.btnExport.Location = new System.Drawing.Point(144, 19);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(53, 67);
            this.btnExport.TabIndex = 1;
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(28, 118);
            this.txtFileName.Multiline = true;
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(291, 61);
            this.txtFileName.TabIndex = 15;
            // 
            // btnReport
            // 
            this.btnReport.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReport.BackgroundImage")));
            this.btnReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReport.Location = new System.Drawing.Point(229, 19);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(53, 67);
            this.btnReport.TabIndex = 14;
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.lstReading);
            this.groupBox3.Location = new System.Drawing.Point(6, 327);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(350, 231);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Statistique";
            // 
            // lstReading
            // 
            this.lstReading.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colType,
            this.columnHeader1});
            this.lstReading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstReading.FullRowSelect = true;
            this.lstReading.GridLines = true;
            this.lstReading.Location = new System.Drawing.Point(3, 16);
            this.lstReading.MultiSelect = false;
            this.lstReading.Name = "lstReading";
            this.lstReading.Size = new System.Drawing.Size(344, 212);
            this.lstReading.TabIndex = 1;
            this.lstReading.UseCompatibleStateImageBehavior = false;
            this.lstReading.View = System.Windows.Forms.View.Details;
            // 
            // colType
            // 
            this.colType.Text = "read Devices";
            this.colType.Width = 259;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Moyenne";
            this.columnHeader1.Width = 81;
            // 
            // panGraph
            // 
            this.panGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panGraph.Location = new System.Drawing.Point(3, 16);
            this.panGraph.Name = "panGraph";
            this.panGraph.Size = new System.Drawing.Size(660, 545);
            this.panGraph.TabIndex = 7;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.panGraph);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(666, 564);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Graph";
            // 
            // AnalysisControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Name = "AnalysisControl";
            this.Size = new System.Drawing.Size(1030, 564);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Panel panGraph;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lstReading;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboDevice2;
        private System.Windows.Forms.ComboBox cboDevice1;
        private System.Windows.Forms.ComboBox cboReadDevice2;
        private System.Windows.Forms.ComboBox cboReadDevice1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblUnitSerie1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblUnitSerie2;
        private System.Windows.Forms.Button btnColor1;
        private System.Windows.Forms.Button btnColor2;
        private System.Windows.Forms.ColorDialog colorDialogAnalysis;
        private System.Windows.Forms.Label lblTagSerie1;
        private System.Windows.Forms.Label lblTagSerie2;
    }
}
