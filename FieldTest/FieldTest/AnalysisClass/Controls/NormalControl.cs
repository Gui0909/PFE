﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using FieldTest.AnalysisClass.Class;
using FieldTest.DataClass;

namespace FieldTest.AnalysisClass.Controls
{
    public partial class NormalControl : UserControl, IAnalysisControl
    {
        private double maximum;
        private double minimum;
        private bool isFirst;

        public NormalControl()
        {
            InitializeComponent();
            this.isFirst = true;
            this.minimum = 0;
        }

        //Numéro de serie (1 ou 2)
        public void SetTitleSerie(int serie, string readDevice)
        {
            this.chartAcquisition.Series[serie - 1].LegendText = readDevice;
        }

        public void SetTitleAxe(int index, string title)
        {
            this.chartAcquisition.ChartAreas[0].Axes[index].Title = title;
        }

        //Numéro de serie (1 ou 2)
        public void FillGraph(int serie, List<Coordinate> listData)
        {
            this.chartAcquisition.Series[serie - 1].Points.DataBind(listData, "RunningTime", "Data", "Tooltip=Data");

            if (isFirst)
            {
                maximum = this.chartAcquisition.Series[serie - 1].Points.Count -1;
                isFirst = false;
            }

        }

        private void chartAcquisition_AxisViewChanged(object sender, System.Windows.Forms.DataVisualization.Charting.ViewEventArgs e)
        {
            //Permet de prend le min et le max de la valeur du x seulement
            //Le (-2) enleve l'indice de début et de fin qui sont seulement une marge ne représentant pas une donnée
            if (e.Axis.AxisName ==  System.Windows.Forms.DataVisualization.Charting.AxisName.X)
            {
                minimum = Math.Floor(e.Axis.ScaleView.ViewMinimum);
                maximum = Math.Ceiling(e.Axis.ScaleView.ViewMaximum - 2);
            }

        }

        public double GetHighMinimum()
        {
            throw new NotImplementedException();
        }

        public double GetHighMaximum()
        {
            throw new NotImplementedException();
        }


        public void ClearPointSerie1()
        {
            this.chartAcquisition.Series[0].Points.Clear();
        }

        public void ClearPointSerie2()
        {
            this.chartAcquisition.Series[1].Points.Clear();
        }


        public double GetNormalMinimum()
        {
            return this.minimum;
        }


        public double GetNormalMaximum()
        {
            return this.maximum;
        }

        //Numéro de serie (1 ou 2)
        public void SetColorSerie(int serie, Color color)
        {
            this.chartAcquisition.Series[serie - 1].Color = color;

        }

        //Numéro de serie (1 ou 2)
        public Color GetColorSerie(int serie)
        {
            return this.chartAcquisition.Series[serie - 1].Color;
        }
    }
}
