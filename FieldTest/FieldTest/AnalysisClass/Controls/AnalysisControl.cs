﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FieldTest.AnalysisClass.Class;
using FieldTest.DataClass;
using FieldTest.AnalysisClass.Report;

namespace FieldTest.AnalysisClass.Controls
{
    public partial class AnalysisControl : UserControl
    {
        private Analysis analy;
        private const string NONE = "None";

        private IAnalysisControl control;

        private bool isUpdating;

        public AnalysisControl()
        {
            InitializeComponent();
            analy = new Analysis();
        }

        private void fillCboDevice()
        {
            this.cboDevice1.Items.Clear();
            this.cboDevice2.Items.Clear();

            this.cboDevice1.Items.Add(NONE);
            this.cboDevice2.Items.Add(NONE);

            
            foreach (string key in analy.ListNormalData.Keys)
            {
                string[] tabKey = key.Split('-');

                if (!this.cboDevice1.Items.Contains(tabKey[0]))
                {
                    this.cboDevice1.Items.Add(tabKey[0]);
                    this.cboDevice2.Items.Add(tabKey[0]);
                }
            }

            this.cboDevice1.SelectedIndex = 0;
            this.cboDevice2.SelectedIndex = 0;
        }

        private void fillListView()
        {
            List<AverageReport> listAverage = analy.GetAverageData();

            for (int i = 0; i < listAverage.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(listAverage[i].Tag + " " + listAverage[i].Name + " (" + this.analy.GetUnit(listAverage[i].Name) + ")");
                lvi.SubItems.Add(listAverage[i].Average.ToString());
                this.lstReading.Items.Add(lvi);

            }
        }

        private void setControlPanel()
        {
            if (analy.Acqui == Acquisition.TYPE.NORMAL)
            {
                control = new Controls.NormalControl();
                this.panGraph.Controls.Clear();
                ((UserControl)control).Dock = DockStyle.Fill;
                this.panGraph.Controls.Add(((UserControl)control));
            }
            else
            {
                control = new HighControl();
                this.panGraph.Controls.Clear();
                ((UserControl)control).Dock = DockStyle.Fill;
                this.panGraph.Controls.Add(((UserControl)control));
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            DialogResult result = openFileDialog.ShowDialog();
            string folderName = string.Empty;

            if (result == DialogResult.OK)
            {
                folderName = openFileDialog.FileName;
                this.txtFileName.Text = folderName;
                this.analy.FolderPath = folderName;

                if (analy.OpenDataFile())
                {
                    //Sert à empecher les contrôles d'activer leur event lors du loading
                    this.isUpdating = true;

                    this.fillCboDevice();

                    this.btnExport.Enabled = true;

                    this.setControlPanel();

                    this.clearListView();
                    this.fillListView();

                    //revient à la normal à la fin du loading
                    isUpdating = false;
                }
            }
        }

        private void clearListView()
        {
            this.lstReading.Items.Clear();
        }


        private void cboDevice1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboReadDevice1.Items.Clear();
            this.cboReadDevice1.Items.Add(NONE);

            if (this.cboDevice1.SelectedItem.ToString() != NONE)
            {
                foreach (string key in analy.ListNormalData.Keys)
                {
                    string[] tabKey = key.Split('-');

                    this.lblTagSerie1.Text = analy.ListTag[tabKey[0]];

                    if (this.cboDevice1.SelectedItem.ToString() == tabKey[0].ToString())
                    {
                        this.cboReadDevice1.Items.Add(tabKey[1]);
                    }
                }
            }
            else
            {
                this.lblTagSerie1.Text = "(---)";
            }

            this.cboReadDevice1.SelectedIndex = 0;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Tsv Files (.tsv)|*.tsv";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (analy.Acqui == Acquisition.TYPE.NORMAL)
                {
                    analy.ExportNormalDataFile(saveFileDialog.FileName, control.GetNormalMinimum(), control.GetNormalMaximum());
                }
                else
                {
                    analy.ExportHighDataFile(saveFileDialog.FileName, control.GetNormalMinimum(), control.GetNormalMaximum(), control.GetHighMinimum(), control.GetHighMaximum());
                }

            }  
        }

        private void cboDevice2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboReadDevice2.Items.Clear();
            this.cboReadDevice2.Items.Add(NONE);

            if (this.cboDevice2.SelectedItem.ToString() != NONE)
            {

                foreach (string key in analy.ListNormalData.Keys)
                {
                    string[] tabKey = key.Split('-');
                    this.lblTagSerie2.Text = analy.ListTag[tabKey[0]];

                    if (this.cboDevice2.SelectedItem.ToString() == tabKey[0].ToString())
                    {
                        this.cboReadDevice2.Items.Add(tabKey[1]);

                    }
                }
            }
            else
            {
                this.lblTagSerie2.Text = "(---)";
            }

            this.cboReadDevice2.SelectedIndex = 0;
        }

        private void cboReadDevice1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!isUpdating)
            {
                if (this.analy.Acqui == Acquisition.TYPE.NORMAL)
                    ((NormalControl)this.panGraph.Controls[0]).ClearPointSerie1();
                else
                    ((HighControl)this.panGraph.Controls[0]).ClearPointSerie1();

                if (this.cboReadDevice1.SelectedItem.ToString() != NONE)
                {
                    string key = this.cboDevice1.SelectedItem.ToString() + "-" + this.cboReadDevice1.SelectedItem.ToString();

                    this.lblUnitSerie1.Text = this.analy.GetUnit(key);

                    if (this.analy.Acqui == Acquisition.TYPE.NORMAL)
                    {
                        ((NormalControl)this.panGraph.Controls[0]).FillGraph(1, this.analy.GetNormalData(key));
                        ((NormalControl)this.panGraph.Controls[0]).SetTitleSerie(1, this.cboReadDevice1.SelectedItem.ToString());
                        ((NormalControl)this.panGraph.Controls[0]).SetTitleAxe(1, "Data (" + this.analy.GetUnit(key) + ")");


                    }
                    else
                    {
                        ((HighControl)this.panGraph.Controls[0]).FillGraph(1, analy.GetNormalData(key), analy.GetHighData(key));
                        ((HighControl)this.panGraph.Controls[0]).SetTitleSerie(1, this.cboReadDevice1.SelectedItem.ToString());
                        ((HighControl)this.panGraph.Controls[0]).SetTitleAxe(1, "Data (" + this.analy.GetUnit(key) + ")");
                    }

                }
                else
                {
                    this.lblUnitSerie1.Text = "---";
                }
            }

            
        }

        private void cboReadDevice2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!isUpdating)
            {
                if (analy.Acqui == Acquisition.TYPE.NORMAL)
                    ((NormalControl)this.panGraph.Controls[0]).ClearPointSerie2();
                else
                    ((HighControl)this.panGraph.Controls[0]).ClearPointSerie2();

                if (this.cboReadDevice2.SelectedItem.ToString() != NONE)
                {
                    string key = this.cboDevice2.SelectedItem.ToString() + "-" + this.cboReadDevice2.SelectedItem.ToString();
                    
                    this.lblUnitSerie2.Text = this.analy.GetUnit(key);

                    if (analy.Acqui == Acquisition.TYPE.NORMAL)
                    {

                        ((NormalControl)this.panGraph.Controls[0]).FillGraph(2, analy.GetNormalData(key));
                        ((NormalControl)this.panGraph.Controls[0]).SetTitleSerie(2, this.cboReadDevice2.SelectedItem.ToString());
                        ((NormalControl)this.panGraph.Controls[0]).SetTitleAxe(3, "Data (" + this.analy.GetUnit(key) + ")");
                        
                    }
                    else
                    {
                        ((HighControl)this.panGraph.Controls[0]).FillGraph(2, analy.GetNormalData(key), analy.GetHighData(key));
                        ((HighControl)this.panGraph.Controls[0]).SetTitleSerie(2, this.cboReadDevice2.SelectedItem.ToString());
                        ((HighControl)this.panGraph.Controls[0]).SetTitleAxe(3, "Data (" + this.analy.GetUnit(key) + ")");
                    }


                }
                else
                {
                    this.lblUnitSerie2.Text = "---";
                }
            }
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            string key1 = this.cboDevice1.SelectedItem.ToString() + "-" + this.cboReadDevice1.SelectedItem.ToString();
            string key2 = this.cboDevice2.SelectedItem.ToString() + "-" + this.cboReadDevice2.SelectedItem.ToString();

            ReportAnalysisControl control;
            List<AverageReport> listAverage = analy.GetAverageData();

            //Checker pour réduire le nombre de paramêtres passés. Note : toutes les informations passé son importante et nécessaire 
            //(peut-être en créant un objet contenant ces élément et en traitant ce dernier dans la classe  ReportAnalysisControl)
            if (analy.Acqui == Acquisition.TYPE.NORMAL)
                control = new ReportAnalysisControl(analy.GetNormalData(key1), analy.GetNormalData(key2), new SerieReport(key1, analy.GetTag(this.cboDevice1.SelectedItem.ToString()), ((NormalControl)this.panGraph.Controls[0]).GetColorSerie(1), key2, analy.GetTag(this.cboDevice2.SelectedItem.ToString()), ((NormalControl)this.panGraph.Controls[0]).GetColorSerie(2)), new UnitReport(this.analy.GetUnit(key1), this.analy.GetUnit(key2)), listAverage, Acquisition.TYPE.NORMAL);
            else
                control = new ReportAnalysisControl(analy.GetNormalData(key1), analy.GetNormalData(key2), analy.GetHighData(key1), analy.GetHighData(key2), new SerieReport(key1, analy.GetTag(this.cboDevice1.SelectedItem.ToString()), ((HighControl)this.panGraph.Controls[0]).GetColorSerie(1), key2, analy.GetTag(this.cboDevice2.SelectedItem.ToString()), ((HighControl)this.panGraph.Controls[0]).GetColorSerie(2)), new UnitReport(this.analy.GetUnit(key1), this.analy.GetUnit(key2)), listAverage, Acquisition.TYPE.HIGH);

            control.ShowDialog();

        }

        private void btnColor1_Click(object sender, EventArgs e)
        {
            if (this.colorDialogAnalysis.ShowDialog() == DialogResult.OK)
            {
                if (this.analy.Acqui == Acquisition.TYPE.NORMAL)
                {
                    ((NormalControl)this.panGraph.Controls[0]).SetColorSerie(1, this.colorDialogAnalysis.Color);
                }
                else
                {

                    ((HighControl)this.panGraph.Controls[0]).SetColorSerie(1, this.colorDialogAnalysis.Color);
                }

            } 
        }

        private void btnColor2_Click(object sender, EventArgs e)
        {
            if (this.colorDialogAnalysis.ShowDialog() == DialogResult.OK)
            {
                if (this.analy.Acqui == Acquisition.TYPE.NORMAL)
                {
                    ((NormalControl)this.panGraph.Controls[0]).SetColorSerie(2, this.colorDialogAnalysis.Color);
                }
                else
                {
                    
                    ((HighControl)this.panGraph.Controls[0]).SetColorSerie(2, this.colorDialogAnalysis.Color);
                }

            } 
        }

    }
}
