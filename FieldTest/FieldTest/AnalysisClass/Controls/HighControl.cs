﻿using FieldTest.AnalysisClass.Class;
using FieldTest.DataClass;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace FieldTest.AnalysisClass.Controls
{
    public partial class HighControl : UserControl, IAnalysisControl
    {
        private double maximumNormal;
        private double minimumNormal;

        private double maximumHigh;
        private double minimumHigh;

        private bool isFirst;

        public HighControl()
        {
            InitializeComponent();
            this.isFirst = true;
            this.minimumNormal = 0;
            this.maximumHigh = 0;
        }

        public void SetTitleAxe(int index, string title)
        {
            this.chartAcquisition.ChartAreas[0].Axes[index].Title = title;
        }

        //Numéro de serie (1 ou 2)
        public void SetTitleSerie(int serie, string readDevice)
        {
            this.chartAcquisition.Series[serie - 1].LegendText = readDevice;
            this.chartHighAcquisition.Series[serie - 1].LegendText = readDevice;
        }

        //Numéro de serie (1 ou 2)
        public void FillGraph(int serie, List<Coordinate> listNormalData, List<Coordinate> listHighData)
        {
            this.chartAcquisition.Series[serie - 1].Points.DataBind(listNormalData, "TimeStamp", "Data", "Tooltip=Data");
            this.chartHighAcquisition.Series[serie - 1].Points.DataBind(listHighData, "Frequency", "Amplitude", "Tooltip=Amplitude");

            if (isFirst)
            {
                this.maximumNormal = Math.Round(this.chartAcquisition.Series[serie - 1].Points[this.chartAcquisition.Series[serie - 1].Points.Count-1].XValue,4, MidpointRounding.AwayFromZero);
                this.maximumHigh = Math.Round(this.chartHighAcquisition.Series[serie - 1].Points[this.chartHighAcquisition.Series[serie - 1].Points.Count - 1].XValue, 4, MidpointRounding.AwayFromZero);
                isFirst = false;
            }

        }

        private void chartHighAcquisition_AxisViewChanged(object sender, System.Windows.Forms.DataVisualization.Charting.ViewEventArgs e)
        {
            //Permet de prend le min et le max de la valeur du x seulement
            if (e.Axis.AxisName == System.Windows.Forms.DataVisualization.Charting.AxisName.X)
            {
                minimumHigh = e.Axis.ScaleView.ViewMinimum;
                maximumHigh = Math.Round(e.Axis.ScaleView.ViewMaximum, 4, MidpointRounding.AwayFromZero);
            }

        }

        private void chartAcquisition_AxisViewChanged(object sender, System.Windows.Forms.DataVisualization.Charting.ViewEventArgs e)
        {
            //Permet de prend le min et le max de la valeur du x seulement
            if (e.Axis.AxisName == System.Windows.Forms.DataVisualization.Charting.AxisName.X)
            {
                minimumNormal = e.Axis.ScaleView.ViewMinimum;
                maximumNormal = Math.Round(e.Axis.ScaleView.ViewMaximum, 4, MidpointRounding.AwayFromZero);
            }

        }


        public double GetHighMinimum()
        {
            return minimumHigh;
        }

        public double GetHighMaximum()
        {
            return maximumHigh;
        }


        public void ClearPointSerie1()
        {
            this.chartAcquisition.Series[0].Points.Clear();
            this.chartHighAcquisition.Series[0].Points.Clear();
        }

        public void ClearPointSerie2()
        {
            this.chartAcquisition.Series[1].Points.Clear();
            this.chartHighAcquisition.Series[1].Points.Clear();
        }


        public double GetNormalMinimum()
        {
            return minimumNormal;
        }

        public double GetNormalMaximum()
        {
            return maximumNormal;
        }

        //Numéro de serie (1 ou 2)
        public void SetColorSerie(int serie, Color color)
        {
            this.chartAcquisition.Series[serie - 1].Color = color;
            this.chartHighAcquisition.Series[serie - 1].Color = color;
        }

        //Numéro de serie (1 ou 2)
        public Color GetColorSerie(int serie)
        {
            return this.chartAcquisition.Series[serie - 1].Color;
        }
    }
}
