﻿using System.Drawing;

namespace FieldTest.AnalysisClass.Class
{
    //Interface pour les contrôles d'analyse
    public interface IAnalysisControl
    {
        
        void SetTitleSerie(int serie, string readDevice);
        void SetColorSerie(int serie, Color color);
        Color GetColorSerie(int serie);

        void ClearPointSerie1();
        void ClearPointSerie2();

        double GetNormalMinimum();
        double GetNormalMaximum();

        double GetHighMinimum();
        double GetHighMaximum();

    }
}
