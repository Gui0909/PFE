﻿using FieldTest.AnalysisClass.Report;
using FieldTest.DataClass;
using FieldTest.ReaderClass;
using FieldTest.WriterClass;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FieldTest.AnalysisClass.Class
{
    public class Analysis
    {
        public SortedList<string, List<Coordinate>> ListNormalData { get; set; }
        public SortedList<string, List<Coordinate>> ListHighData { get; set; }
        public SortedList<string, string> ListUnit { get; set; }
        public SortedList<string, AverageReport> ListAverage { get; set; }
        public SortedList<string, string> ListTag { get; set; }
        
        public string FolderPath { get; set; }
        public Acquisition.TYPE Acqui { get; set; }

        public Analysis()
        { 
            
        }

        public bool OpenDataFile()
        {
            try
            {

                ReaderData reader = new ReaderData(FolderPath);
                this.ListNormalData = reader.GetNormalData();

                //Dans le cas d'un fichier d'acquisition haute fréquence on remplit la liste de donnée (amp vs freq)
                if (reader.GetTypeAcquisition() == Acquisition.TYPE.HIGH)
                    this.ListHighData = reader.GetHighData();

                this.ListAverage = reader.GetAverageData();
                this.ListUnit = reader.GetUnitData();
                this.Acqui = reader.GetTypeAcquisition();
                this.ListTag = reader.GetTagData();

                return true;

            }
            catch
            {
                MessageBox.Show("This file is not valid.");
                return false;
            }
        }

        //Export de fichier d'acquisition de donnée normal
        public void ExportNormalDataFile(string path, double min, double max)
        {
            SortedList<string, List<Coordinate>> newList = new SortedList<string, List<Coordinate>>();

            foreach (string readDevice in this.ListNormalData.Keys)
            {
                List<Coordinate> listCoord = new List<Coordinate>();

                //Ajoute les données à la listes selon le min et max du zoom
                //En raison des unité en x étant du datetime l'echelle pour le min et le max est un entier selon la position de la données sur la graph 
                for (int i = (int)min; i < (int)max; i++)
                {
                    listCoord.Add(this.ListNormalData[readDevice].ElementAt(i));
                    
                }

                newList.Add(readDevice, listCoord);
            }

            WriterNormalData writer = new WriterNormalData(newList, this.ListUnit, this.ListTag, path, this.Acqui);
            writer.WriteNormal(newList);
            writer.StopWritting();
          
        }

        //Export de fichier d'acquisition haute fréquence de donnée
        public void ExportHighDataFile(string path, double minNormal, double maxNormal, double minHigh, double maxHigh)
        {
            SortedList<string, List<Coordinate>> newListNormal = new SortedList<string, List<Coordinate>>();
            SortedList<string, List<Coordinate>> newListHigh = new SortedList<string, List<Coordinate>>();

            foreach (string readDevice in this.ListNormalData.Keys)
            {
                List<Coordinate> listNormalCoord = new List<Coordinate>();
                List<Coordinate> listHighCoord = new List<Coordinate>();

                //Ajoute les données à la listes (data vs time) selon le min et max du zoom
                //Les unités du min et du max sont les donnée de temps en x directement
                for (int i = 0; i < this.ListNormalData[readDevice].Count; i++)
                {
                    if (this.ListNormalData[readDevice].ElementAt(i).TimeStamp >= minNormal && this.ListNormalData[readDevice].ElementAt(i).TimeStamp <= maxNormal)
                        listNormalCoord.Add(this.ListNormalData[readDevice].ElementAt(i));

                }

                //Ajoute les données à la listes (amp vs freq) selon le min et max du zoom
                //Les unités du min et du max sont les donnée de frequence en x directement
                for (int i = 0; i < this.ListHighData[readDevice].Count; i++)
                {
                    if (this.ListHighData[readDevice].ElementAt(i).Frequency >= minHigh && this.ListHighData[readDevice].ElementAt(i).Frequency <= maxHigh)
                        listHighCoord.Add(this.ListHighData[readDevice].ElementAt(i));

                }

                newListNormal.Add(readDevice, listNormalCoord);
                newListHigh.Add(readDevice, listHighCoord);
            }


            WriterHighData writer = new WriterHighData(newListNormal, this.ListUnit, this.ListTag, path, this.Acqui);
            writer.WriteHigh(newListNormal, newListHigh);
            writer.StopWritting();

        }

        public string GetUnit(string key)
        {
            try
            {
                return this.ListUnit[key];
            }
            catch
            {
                return string.Empty;
            }
            
        }

        public List<AverageReport> GetAverageData()
        { 
            List<AverageReport> listAve = new List<AverageReport>();

            foreach (AverageReport ave in this.ListAverage.Values)
            {
                listAve.Add(ave);
            }

            return listAve;
        }

        public List<Coordinate> GetNormalData(string readDevice)
        {
            try
            {
                return this.ListNormalData[readDevice];
            }
            catch
            {
                return new List<Coordinate>();
            }
        }

        public List<Coordinate> GetHighData(string readDevice)
        {
            try
            {
                return this.ListHighData[readDevice];
            }
            catch 
            {
                return new List<Coordinate>();
            }
        }

        public string GetTag(string readDevice)
        {
            try
            {
                return this.ListTag[readDevice];
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
