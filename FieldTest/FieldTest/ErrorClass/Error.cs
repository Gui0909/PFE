﻿using FieldTest.ReaderClass;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FieldTest.ErrorClass
{
    public class Error
    {
        public string FolderPath { get; set; }

        public List<ErrorMessage> ListErrorMessage { get; set; }
        public List<string> ListHead { get; set; }
        public SortedList<string, string> ListUnit { get; set; }
        public SortedList<string, string> ListTag { get; set; } 

        public Error()
        { 
            
        }

        //S'occupe d'ouvrir les fichier d'erreurs et en cas d'erreur de traitement envoie une erreur
        public bool OpenDataFile()
        {
            try
            {

                ReaderError reader = new ReaderError(FolderPath);

                this.ListErrorMessage = reader.GetListErrorMessage();
                this.ListHead = reader.GetListHead();
                this.ListUnit = reader.GetListUnit();
                this.ListTag = reader.GetListTag();

                return true;

            }
            catch
            {
                MessageBox.Show("This file is not valid.");
                return false;
            }
        }
    }
}
