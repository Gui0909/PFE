﻿using System;
using System.Windows.Forms;

namespace FieldTest.ErrorClass
{
    public partial class ErrorControl : UserControl
    {
        private Error error;
        private const string ERROR = "Error";

        public ErrorControl()
        {
            InitializeComponent();
            this.error = new Error();
        }

        private void clearData()
        {
            this.lstError.Items.Clear();
            int nbrColumsToRemove = this.lstError.Columns.Count - 1;

            for (int i = 0; i < nbrColumsToRemove; i++)
            {
                this.lstError.Columns.RemoveAt(1);
            }
        }

        private void setColumns()
        {
            foreach (string head in this.error.ListHead)
            {
                if (!head.Contains(ERROR))
                {
                    string name = error.ListTag[head.Split('-')[0]] + " " + head + " (" + error.ListUnit[head] + ")";
                    this.lstError.Columns.Add(name);
                }
                else
                {
                    this.lstError.Columns.Add(head);
                }
            }
        }

        private void setDataError()
        {
            foreach (ErrorMessage errMess in this.error.ListErrorMessage)
            {
                ListViewItem lvi = new ListViewItem(errMess.Time.ToString());

                string message = string.Empty;

                foreach (string mess in errMess.ListMessage)
                {

                    message += mess + "\n";

                }

                lvi.SubItems.Add(message);

                foreach (string coord in errMess.ListCoord)
                {
                    lvi.SubItems.Add(coord);
                }

                this.lstError.Items.Add(lvi);

            }
        }

        private void loadFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            DialogResult result = openFileDialog.ShowDialog();
            string folderName = string.Empty;
            this.clearData();

            if (result == DialogResult.OK)
            {
                folderName = openFileDialog.FileName;
                txtFileName.Text = folderName;
                error.FolderPath = folderName;

                if (this.error.OpenDataFile())
                {
                    this.setColumns();
                    this.setDataError();

                    this.lstError.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                }
            }
        
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            loadFile();
        }

    }
}
