﻿using System.Collections.Generic;

namespace FieldTest.ErrorClass
{
    public class ErrorMessage
    {
        public string Time { get; set; }
        public List<string> ListMessage { get; set; }
        public List<string> ListCoord { get; set; }

        public ErrorMessage(string time, List<string> listMessage, List<string> listCoord)
        {
            this.Time = time;
            this.ListMessage = listMessage;
            this.ListCoord = listCoord;
        }
    }
}
