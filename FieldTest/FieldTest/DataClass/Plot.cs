﻿
namespace FieldTest.DataClass
{
    public class Plot
    {
        public string TagDevice { get; set; }
        public string TagReadDevice { get; set; }

        public Plot(string device, string readDevice)
        {
            TagDevice = device;
            TagReadDevice = readDevice;
        }
    }
}
