﻿using System;

namespace FieldTest.DataClass
{
    public class Coordinate
    {
        public double TimeStamp { get; set; }

        //Date RunningTime en chaine de caractère
        public string RunningTimeString { get; set; }

        public DateTime RunningTime { get; set; }
        public double Data { get; set; }

        public double Frequency { get; set; }
        public double Amplitude { get; set; } 

        public Coordinate(double y)
        {
            Data = y;
        }

        public Coordinate(DateTime x, double y)
        {
            Data = y;
            RunningTime = x;
            RunningTimeString = x.ToString("HH:mm:ss");
        }

        //Permet d'avoir un constructeur pouvant setter les valeur (x, y) des deux sorte d'acquisition avec les mêmes paramêtres
        public Coordinate(double x, double y, Acquisition.TYPE type)
        {
            if (type == Acquisition.TYPE.NORMAL)
            {
                TimeStamp = x;
                Data = y;
            }
            else
            { 
                Frequency = x;
                Amplitude = y;
            }

        }
    }
}
