﻿using System;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using Buffer = SharpDX.Direct3D11.Buffer;
using SharpDX;


namespace FieldTest.FFTClass
{

    public static class FourierTransform
    {

        private static Device m_Device = new Device(DriverType.Warp);
        private static FastFourierTransform m_FFT;
        private static DeviceContext m_Context;
       
        private static void Dispose()
        {
            m_FFT.Dispose();
            m_Context.Flush();
            m_Context.Dispose();
            m_Device.Dispose();
           
        }

        private static UnorderedAccessView CreateBufferUAV(Device device, Buffer buffer)
        {
            var uavDesc = new UnorderedAccessViewDescription
            {
                Dimension = UnorderedAccessViewDimension.Buffer,
                Buffer = new UnorderedAccessViewDescription.BufferResource
                {
                    FirstElement = 0,
                    ElementCount = buffer.Description.SizeInBytes / sizeof(float),
                    Flags = UnorderedAccessViewBufferFlags.Raw
                },
                Format = SharpDX.DXGI.Format.R32_Typeless
            };

            var bufferUAV = new UnorderedAccessView(m_Device, buffer, uavDesc);

            return bufferUAV;
        }

        private static Buffer CreateByteOrderBufferOnGPU(Device device, int count, float[] data, ResourceUsage useage)
        {
            var bufferDesc = new BufferDescription
            {
                BindFlags = BindFlags.UnorderedAccess | BindFlags.ShaderResource,
                SizeInBytes = sizeof(float) * data.Length,
                Usage = useage,
                StructureByteStride = sizeof(float),
                OptionFlags = ResourceOptionFlags.BufferAllowRawViews,
            };

            var buffer = Buffer.Create<float>(
            device,
            data,
            bufferDesc);

            return buffer;
        }
        private static void setttingFFT(int m_Length)
        {
          m_Context = m_Device.ImmediateContext;

          var fftDesc = new FastFourierTransformDescription
          {
            DimensionCount = 1,
            DataType = FastFourierTransformDataType.Real,
            Dimensions = FastFourierTransformDimensions.Dimension1D
          };

          fftDesc.ElementLengths[0] = m_Length;

          m_FFT = new FastFourierTransform(m_Context, fftDesc);
          m_FFT.ForwardScale = 1.0f;

          var tmp0In = CreateByteOrderBufferOnGPU(m_Device, 4 * m_Length, new float[4 * m_Length], ResourceUsage.Default);
          var tmp0InUAV = CreateBufferUAV(m_Device, tmp0In);

          var tmp1In = CreateByteOrderBufferOnGPU(m_Device, 4 * m_Length, new float[4 * m_Length], ResourceUsage.Default);
          var tmp1InUAV = CreateBufferUAV(m_Device, tmp1In);

          m_FFT.AttachBuffersAndPrecompute(new UnorderedAccessView[] { tmp0InUAV, tmp1InUAV }, new UnorderedAccessView[] { });

        }

        public static float[] PerformFFT(float[] dataIn, int m_Length)
        {
          
            setttingFFT(m_Length);

            var bufIn = CreateByteOrderBufferOnGPU(m_Device, m_Length, dataIn, ResourceUsage.Default);
            var bufInUAV = CreateBufferUAV(m_Device, bufIn);

            var bufOutUAV = m_FFT.ForwardTransform(bufInUAV);

            var stagingBuf = new Buffer(m_Device, new BufferDescription()
            {
                BindFlags = BindFlags.None,
                SizeInBytes = m_Length * 16,
                Usage = ResourceUsage.Staging,
                StructureByteStride = sizeof(float),
                OptionFlags = ResourceOptionFlags.None,
                CpuAccessFlags = CpuAccessFlags.Read
            });


            m_Context.CopyResource(bufOutUAV.Resource, stagingBuf);

            DataStream stream;
            m_Context.MapSubresource(stagingBuf, MapMode.Read, MapFlags.None, out stream);

            var bytes = new byte[stream.Length];
            stream.Read(bytes, 0, (int)stream.Length);

            m_Context.UnmapSubresource(stagingBuf, 0);

            var floats = new float[bytes.Length / 16];
            for (int i = 0; i < floats.Length; i++)
            {
                floats[i] = BitConverter.ToSingle(bytes, i * 4);
            }

            bufIn.Dispose();
            bufInUAV.Dispose();
            bufOutUAV.Dispose();
            stagingBuf.Dispose();
            stream.Dispose();

            m_Context.Flush();

            return floats;
        
        } 

    }
}
