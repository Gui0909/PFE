﻿
using FieldTest.DataClass;
using FieldTest.DevicePortMapClass.TPM2PortClass.StatusInfo;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace FieldTest.DevicePortMapClass.TPM2PortClass
{
    class TPM2PortMapped : ADevicePortMapped
    {
        private const int NB_BYTE_READ = 8;
        private const int NB_BYTE_WRITE = 5;
        private string stringBuffer;
        private byte[] response;
        private bool isWaitingResponse;
        private List<string> listErrors;

        public string a  { get; set; }

        public TPM2PortMapped(ADeviceSetting deviceSetting, SortedList<string, ADeviceReading> listReadDevice, bool enableError)
            : base(deviceSetting, listReadDevice, enableError)
        {
            stringBuffer = string.Empty;

            foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
            {
                readDevice.Unit = deviceSetting.Unit;
            }

        }

        public void TransmitterConfig(DeviceSettingManager.GAIN gain, DeviceSettingManager.SHUNT shunt)
        {
            byte[] dataWrite = new byte[4];
            dataWrite[0] = 0xa0;

            switch (shunt)
            {
                case DeviceSettingManager.SHUNT.SHUNTOFFBOTH:
                    dataWrite[1] = 0x00;
                    break;
                case DeviceSettingManager.SHUNT.SHUNT1:
                    dataWrite[1] = 0x01;
                    break;
                case DeviceSettingManager.SHUNT.SHUNT2:
                    dataWrite[1] = 0x02;
                    break;
               
            }

            switch (gain)
            {
                case DeviceSettingManager.GAIN.GAIN1:
                    dataWrite[2] = 0x00;
                    break;

                case DeviceSettingManager.GAIN.GAIN2:
                    dataWrite[2] = 0x01;
                    break;

                case DeviceSettingManager.GAIN.GAIN4:
                    dataWrite[2] = 0x02;
                    break;

                case DeviceSettingManager.GAIN.GAIN8:
                    dataWrite[2] = 0x03;
                    break;

                case DeviceSettingManager.GAIN.GAIN16:
                    dataWrite[2] = 0x04;
                    break;

                case DeviceSettingManager.GAIN.GAIN32:
                    dataWrite[2] = 0x05;
                    break;

                case DeviceSettingManager.GAIN.GAIN64:
                    dataWrite[2] = 0x06;
                    break;

                case DeviceSettingManager.GAIN.GAIN128:
                    dataWrite[2] = 0x07;
                    break;
            }

            dataWrite[3] = (byte)(dataWrite[0] + dataWrite[1] + dataWrite[2]);

            base.Port.Write(dataWrite, 0, 4);
        }

        private void readResponse()
        {
            int index = 0;
            response = new byte[NB_BYTE_READ];
            byte[] byteConfirm = new byte[] { 0x55, 0x01, 0x02, 0x03, 0xFE, 0xE8, 0xC4, 0x05 };
           

            while (isWaitingResponse)
            {
                if (base.Port.BytesToRead != 0)
                {
                    response[index] = (byte)base.Port.ReadByte();
                    index++;

                    if (response.SequenceEqual(byteConfirm))
                    {
                        isWaitingResponse = false;
                    }
                    else
                    {

                        if (index > 7)
                        {
                            for (int i = 0; i < 7; i++)
                            {
                                response[i] = response[i + 1];
                            }

                            index = 7;
                        }
                    }
                }
            }

        }

        public void StartConfig()
        { 
            byte[] dataWrite = new byte[NB_BYTE_WRITE];
            byte[] dataBreak = new byte[NB_BYTE_WRITE];
            byte[] dataEnd = new byte[8];
            float betweenTime = 10;

            dataWrite[0] = 0x55;
            dataWrite[1] = 0x08;
            dataWrite[2] = 0xef;
            dataWrite[3] = (byte)(dataWrite[0] + dataWrite[1] + dataWrite[2]);
            dataWrite[4] = 0;

            isWaitingResponse = true;
            Thread responseThread = new Thread(readResponse);
            responseThread.Start();

            while (isWaitingResponse)
            {
                float breakingTime = DateTime.Now.Millisecond + betweenTime;

                while (breakingTime > DateTime.Now.Millisecond)
                {
                    base.Port.Write(dataWrite, 0, NB_BYTE_WRITE);
                }
            }
        }


        public void CommunicationConfig(Parity parity, StopBits stopBits, DeviceSettingManager.BAUDRATE baudRate, DeviceSettingManager.SAMPLERATE sampleRate)
        {
            byte[] dataWrite = new byte[4];
            dataWrite[0] = 0x8a;

            switch (parity)
            {
                case Parity.None:
                    dataWrite[1] = 0;
                    break;
                case Parity.Even:
                    dataWrite[1] = 64;
                    break;
                case Parity.Odd:
                    dataWrite[1] = 128;
                    break;
            }

            //J'additione pour faire le byte total que je vais envoyer
            switch (stopBits)
            {
                case StopBits.One:
                    dataWrite[1] += 0;
                    break;
                case StopBits.Two:
                    dataWrite[1] += 32;
                    break;
            }

            //J'additione pour faire le byte total que je vais envoyer
            switch (baudRate)
            {
                case DeviceSettingManager.BAUDRATE.RATE460K:
                 dataWrite[1] += 0;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE230K:
                 dataWrite[1] += 1;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE115K:
                 dataWrite[1] += 2;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE57K:
                 dataWrite[1] += 3;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE28K:
                 dataWrite[1] += 4;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE14K:
                 dataWrite[1] += 5;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE9600:
                 dataWrite[1] += 6;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE4800:
                 dataWrite[1] += 7;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE2400:
                 dataWrite[1] += 8;
                 break;

                case DeviceSettingManager.BAUDRATE.RATE1200:
                 dataWrite[1] += 9;
                 break;
            
            }

            switch (sampleRate)
            {
                case DeviceSettingManager.SAMPLERATE.RATE4800:
                    dataWrite[2] = 0;
                    break;

                case DeviceSettingManager.SAMPLERATE.RATE2400:
                    dataWrite[2] = 1;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE1200:
                    dataWrite[2] = 2;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE600:
                    dataWrite[2] = 3;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE300:
                    dataWrite[2] = 4;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE150:
                    dataWrite[2] = 5;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE75:
                    dataWrite[2] = 6;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE37:
                    dataWrite[2] = 7;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE18:
                    dataWrite[2] = 8;
                    break;
                case DeviceSettingManager.SAMPLERATE.RATE9:
                    dataWrite[2] = 9;
                    break;
            }

            dataWrite[3] = (byte)(dataWrite[0] + dataWrite[1] + dataWrite[2]);

            base.Port.Write(dataWrite, 0, 4);

        }

        public override void ReadThread()
        {
            try
            {

                byte[] rawDataRead = new byte[NB_BYTE_READ];
                byte chkSum = 0;
                bool isValidData = false;

                while (base.Port.BytesToRead < NB_BYTE_READ);
                    base.Port.Read(rawDataRead, 0, NB_BYTE_READ);

                chkSum = (byte)(rawDataRead[0] + rawDataRead[1] + rawDataRead[2] + rawDataRead[3] + rawDataRead[4] + rawDataRead[5] + rawDataRead[6]);

                if (rawDataRead[7] == chkSum)
                {
                    isValidData = true;
                }
                else
                {
                    for (int i = 0; i < 7; i++)
                    {
                        rawDataRead[i] = rawDataRead[i + 1];
                    }
                }
              
                while (!isValidData)
                {
                    if (base.Port.BytesToRead != 0)
                    {

                        rawDataRead[7] = (byte)base.Port.ReadByte();

                        chkSum = (byte)(rawDataRead[0] + rawDataRead[1] + rawDataRead[2] + rawDataRead[3] + rawDataRead[4] + rawDataRead[5] + rawDataRead[6]);

                        if (rawDataRead[7] == chkSum)
                        {
                            isValidData = true;
                        }
                        else
                        {
                            for (int i = 0; i < 7; i++)
                            {
                                rawDataRead[i] = rawDataRead[i + 1];
                            }
                        }
                    }
                }

                StatusInfoByte0TPM2 infoByte0 = new StatusInfoByte0TPM2(rawDataRead[4]);
                StatusInfoByte1TPM2 infoByte1 = new StatusInfoByte1TPM2(rawDataRead[5]);
                StatusInfoByte2TPM2 infoByte2 = new StatusInfoByte2TPM2(rawDataRead[6]);
                this.listErrors = new List<string>();

                if (infoByte0.GetStatusInfo().Count != 0 || infoByte1.GetStatusInfo().Count != 0)
                {

                   foreach (string message in infoByte0.GetStatusInfo())
                   {
                       this.listErrors.Add(message);
                   }

                   foreach (string message in infoByte1.GetStatusInfo())
                   {
                       this.listErrors.Add(message);
                   }

                }
           
                Int16 strainGage = BitConverter.ToInt16(rawDataRead, 0);
                Int16 shaftSpeed = BitConverter.ToInt16(rawDataRead, 2);

                stringBuffer = strainGage.ToString() + "," + shaftSpeed.ToString() + "," + infoByte2.getGainFactor() + "," + infoByte0.RPM_RES;
            }
            catch (IOException e)
            {
                if (!base.IsCheckingPort)
                    base.StartThreadPortDectection();
            }

        }

        public override SortedList<string, Coordinate> ExtractData()
        {
            lock (stringBuffer)
            {
                SortedList<string, Coordinate> listPlots = new SortedList<string, Coordinate>();

                if (stringBuffer != string.Empty)
                {
                    foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
                    {
                        double y = double.NaN;

                        readDevice.ReadDataAsynchrone(stringBuffer, out y);

                        listPlots.Add(readDevice.Name, new Coordinate(y));
                    }

                    stringBuffer = string.Empty;
                }
               
                return listPlots;
            }
        }

        public override void OpenPort()
        {

            if (!base.Port.IsOpen)
            {
                base.Port.Open();
                TPM2Setting settingTPM2 = (TPM2Setting)base.DeviceSetting;
                base.BaseStream = base.Port.BaseStream;
                //StartConfig();

                this.CommunicationConfig(settingTPM2.Parity, settingTPM2.StopBits, settingTPM2.BaudRate, settingTPM2.SampleRate);
                this.TransmitterConfig(settingTPM2.Gain, settingTPM2.Shunt);

                GC.SuppressFinalize(base.BaseStream);
            }           
            
        }

        public override void UpdateDeviceSetting(ADeviceSetting deviceSetting)
        {
            base.DeviceSetting = deviceSetting;
            TPM2Setting settingTPM2 = (TPM2Setting)deviceSetting;

            foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
            {
                readDevice.Unit = deviceSetting.Unit;
            }


            this.CommunicationConfig(settingTPM2.Parity, settingTPM2.StopBits, settingTPM2.BaudRate, settingTPM2.SampleRate);
            this.TransmitterConfig(settingTPM2.Gain, settingTPM2.Shunt);

            base.Port.BaudRate = (int)deviceSetting.BaudRate;
            base.Port.Parity = deviceSetting.Parity;
            base.Port.DataBits = deviceSetting.DataBits;
            base.Port.StopBits = deviceSetting.StopBits;
        }

        public override bool IsTransmittingData()
        {
            if (base.Port.BytesToRead != 0)
                return true;
            else
                return false;
        }


        public override List<string> GetListError()
        {
            return this.listErrors;
        }
    }
}
