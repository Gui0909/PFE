﻿
using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.DevicePortMapClass.TPM2PortClass
{
    public class PowerTPM2Reading : ADeviceReading
    {
        public double GaugeFactor { get; set; }
        public double GainFactor { get; set; }
        public double E { get; set; }
        public double OD { get; set; }
        public double ID { get; set; }
        public double V { get; set; }
        public double W { get; set; }
        
        public PowerTPM2Reading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui, double gaugefactor, double gainFactor, double e, double od, double id, double v)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
            this.GaugeFactor = gaugefactor;
            this.GainFactor = gainFactor;
            this.E = e;
            this.OD = od;
            this.ID = id;
            this.V = v;

        }

        private double getTorque(ref string stringBuffer)
        {
            double kt = 0;

            if (this.Unit == DeviceSettingManager.UNIT.METRIC)
                kt = 1.6 * Math.Pow(10, 10);
            else if(this.Unit == DeviceSettingManager.UNIT.ENGLISH)
                kt = 192;
            else
                kt = 192;

            return (getStrain(ref stringBuffer) * Math.PI * this.E * (Math.Pow(this.OD, 4) - (Math.Pow(this.ID, 4))) / (kt * this.OD * (1 + this.V)));
        }

        private double getStrain(ref string stringBuffer)
        {

            string[] tabData = stringBuffer.Split(',');
            int gainFactor = int.Parse(tabData[2]);

            int value = int.Parse(tabData[0]);

            return (value * 15729) / (this.GaugeFactor * this.GainFactor * 7864.32);
        }

        private double getRPM(ref string stringBuffer)
        { 
            string[] tabData = stringBuffer.Split(',');
            int rpm = int.Parse(tabData[1]);

            if (byte.Parse(tabData[3]) == 1)
            {
                return 0.01 * rpm;
            }
            else
            {
                return rpm;
            }
        }

        private double getPower(ref string stringBuffer)
        {
            int kp = 0;

            if (this.Unit == DeviceSettingManager.UNIT.METRIC)
                kp = 60;
            else if (this.Unit == DeviceSettingManager.UNIT.ENGLISH)
                kp = 33000;
            else
                kp = 33000;

            return (getTorque(ref stringBuffer) * 2 * Math.PI * getRPM(ref stringBuffer)) / kp;
        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            y = 0;

            y = getPower(ref stringBuffer);
        }

        public override void ReadDataSynchrone(System.IO.Ports.SerialPort port, out double y)
        {
            throw new NotImplementedException();
        }

        public override string GetConcreteUnit()
        {
            if (base.Unit == DeviceSettingManager.UNIT.METRIC)
                return "watts ";
            else if (base.Unit == DeviceSettingManager.UNIT.ENGLISH)
                return "hp";
            else
                return string.Empty;
        }
    }
}
