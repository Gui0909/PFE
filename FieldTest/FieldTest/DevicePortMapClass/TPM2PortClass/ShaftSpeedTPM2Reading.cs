﻿
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.DevicePortMapClass.TPM2PortClass
{
    public class ShaftSpeedTPM2Reading : ADeviceReading
    {

        public ShaftSpeedTPM2Reading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
        }

        private double getRPM(ref string stringBuffer)
        {
            string[] tabData = stringBuffer.Split(',');
            int rpm = int.Parse(tabData[1]);

            if (byte.Parse(tabData[3]) == 1)
            {
                return 0.01 * rpm;
            }
            else
            {
                return rpm;
            }
        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {

            y = 0;
            y = getRPM(ref stringBuffer);

        }

        public override void ReadDataSynchrone(SerialPort port, out double y)
        {
            throw new NotImplementedException();
        }

        public override string GetConcreteUnit()
        {
            return "rpm";
        }
    }
}
