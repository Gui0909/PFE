﻿
using FieldTest.UtilitaryByte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.DevicePortMapClass.TPM2PortClass.StatusInfo
{
    public class StatusInfoByte0TPM2
    {
        public byte RPM_NEW {get;set;}
        public byte RPM_ERR { get; set; }
        public byte RPM_RES { get; set; }
        public byte ECOM_ACK { get; set; }
        public byte ECOM_ERR { get; set; }
        public byte STAT_PWR_ERR { get; set; }
        public byte II_AMP_TEMP_WRN { get; set; }
        public byte STAT_TEST_MODE { get; set; }

        public StatusInfoByte0TPM2(byte byte0)
        {
            RPM_NEW = ByteConverter.GetBits(byte0, 0);
            RPM_ERR = ByteConverter.GetBits(byte0, 1);
            RPM_RES = ByteConverter.GetBits(byte0, 2);
            ECOM_ACK = ByteConverter.GetBits(byte0, 3);
            ECOM_ERR = ByteConverter.GetBits(byte0, 4);
            STAT_PWR_ERR = ByteConverter.GetBits(byte0, 5);
            II_AMP_TEMP_WRN = ByteConverter.GetBits(byte0, 6);
            STAT_TEST_MODE = ByteConverter.GetBits(byte0, 7);
        }

        public List<string> GetStatusInfo()
        {
            List<string> listStatus = new List<string>();

            if (RPM_ERR == 1)
            {
                string message = "An error was detected in the RPM measurement. Check rotor-stator alignment and spacing.";
                listStatus.Add(message);
            }

            if (STAT_PWR_ERR == 1)
            {
                string message = "The Stator main regulated power supply voltage is too high or low, or an over current error exists.";
                listStatus.Add(message);
            }

            if (II_AMP_TEMP_WRN == 1)
            {
                string message = "The II power amplifier temperature is at or nearing thermal shutdown.";
                listStatus.Add(message);
            }

            return listStatus;
        }

    }
}
