﻿
using FieldTest.UtilitaryByte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.DevicePortMapClass.TPM2PortClass.StatusInfo
{
    public class StatusInfoByte1TPM2
    {
        public byte TRQ_HLD_ERR {get;set;}
        public byte TRQ_RNG_ERR { get; set; }
        public byte GAGE_DIFF_ERR { get; set; }
        public byte GAGE_COM_ERR { get; set; }
        public byte ROT_PWR_LO_ERR { get; set; }
        public byte ROT_DATA_ERR { get; set; }
        public byte ROT_DATA_GONE { get; set; }

        public StatusInfoByte1TPM2(byte byte1)
        {
            TRQ_HLD_ERR = ByteConverter.GetBits(byte1, 0);
            TRQ_RNG_ERR = ByteConverter.GetBits(byte1, 1);
            GAGE_DIFF_ERR = ByteConverter.GetBits(byte1, 2);
            GAGE_COM_ERR = ByteConverter.GetBits(byte1, 3);
            ROT_PWR_LO_ERR = ByteConverter.GetBits(byte1, 4);
            ROT_DATA_ERR = ByteConverter.GetBits(byte1, 5);
            ROT_DATA_GONE = ByteConverter.GetBits(byte1, 6);
        }

        public List<string> GetStatusInfo()
        {
            List<string> listStatus = new List<string>();
            if (TRQ_RNG_ERR == 1)
            {
                string message = "The Torque value in this sample is out of range.";
                listStatus.Add(message);
            }

            if (GAGE_DIFF_ERR == 1)
            {
                string message = "Gage differential mode input of the transmitter is out of range.";
                listStatus.Add(message);
            }

            if (GAGE_COM_ERR == 1)
            {
                string message = "Gage common mode input of the transmitter is out of range.";
                listStatus.Add(message);
            }

            if (ROT_PWR_LO_ERR == 1)
            {
                string message = "The Rotor power supply voltage is too low.";
                listStatus.Add(message);
            }

            if (ROT_DATA_ERR == 1)
            {
                string message = "An error has been detected receiving data from the Rotor (Transmitter).";
                listStatus.Add(message);
            }

            if (ROT_DATA_GONE == 1)
            {
                string message = "There is no Rotor (Transmitter) Data being received without error.";
                listStatus.Add(message);
            }

            return listStatus;
        }
    }
}
