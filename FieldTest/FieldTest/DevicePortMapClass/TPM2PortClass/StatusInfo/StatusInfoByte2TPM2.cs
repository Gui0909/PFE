﻿
using FieldTest.UtilitaryByte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest.DevicePortMapClass.TPM2PortClass.StatusInfo
{
    public class StatusInfoByte2TPM2
    {
        public byte GAIN0 { get; set; }
        public byte GAIN1 { get; set; }
        public byte GAIN2 { get; set; }
        public byte SHUNT1 { get; set; }
        public byte SHUNT2 { get; set; }

        public StatusInfoByte2TPM2(byte byte2) 
        {
            GAIN0 = ByteConverter.GetBits(byte2, 0);
            GAIN1 = ByteConverter.GetBits(byte2, 1);
            GAIN2 = ByteConverter.GetBits(byte2, 2);
            SHUNT1 = ByteConverter.GetBits(byte2, 3);
            SHUNT2 = ByteConverter.GetBits(byte2, 4);
        }

        public int getGainFactor()
        {
            if (GAIN0 == 0 && GAIN1 == 0 && GAIN2 == 0)
            {
                return 1;
            }
            else if (GAIN0 == 0 && GAIN1 == 0 && GAIN2 == 1)
            {
                return 2;
            }
            else if (GAIN0 == 0 && GAIN1 == 1 && GAIN2 == 0)
            {
                return 4;
            }
            else if (GAIN0 == 0 && GAIN1 == 1 && GAIN2 == 1)
            {
                return 8;
            }
            else if (GAIN0 == 1 && GAIN1 == 0 && GAIN2 == 0)
            {
                return 16;
            }
            else if (GAIN0 == 1 && GAIN1 == 0 && GAIN2 == 1)
            {
                return 32;
            }
            else if (GAIN0 == 1 && GAIN1 == 1 && GAIN2 == 0)
            {
                return 64;
            }
            else if (GAIN0 == 1 && GAIN1 == 1 && GAIN2 == 1)
            {
                return 128;
            }
            else 
            {
                return 0;
            }
        
        }
    }
}
