﻿
using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace FieldTest.DevicePortMapClass.RPMPortClass
{
    class RPMPortMapped : ADevicePortMapped
    {
        private const int NB_BYTE_READ = 6;
        private const int NB_BYTE_WRITE = 4;


        public RPMPortMapped(ADeviceSetting deviceSetting, SortedList<string, ADeviceReading> listReadDevice, bool enableError)
            : base(deviceSetting, listReadDevice, enableError)
        {
        }

        public override void ReadThread()
        {
            
        }

        public override SortedList<string, Coordinate> ExtractData()
        {
            SortedList<string, Coordinate> listPlots = new SortedList<string, Coordinate>();
            SerialPort sp = base.Port;

            foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
            {
               double y = double.NaN;

               readDevice.ReadDataSynchrone(sp, out y);

               listPlots.Add(readDevice.Name, new Coordinate(((RPMSetting)base.DeviceSetting).RPMFactor * y));
            }

            return listPlots;
        }

        public override void OpenPort()
        {

            if (!base.Port.IsOpen)
            {
                base.Port.Open();
                base.BaseStream = base.Port.BaseStream;

                GC.SuppressFinalize(base.BaseStream);
            }
        }

        public override void UpdateDeviceSetting(ADeviceSetting deviceSetting)
        {
            base.DeviceSetting = deviceSetting;
            base.Port.BaudRate = (int)deviceSetting.BaudRate;
            base.Port.Parity = deviceSetting.Parity;
            base.Port.DataBits = deviceSetting.DataBits;
            base.Port.StopBits = deviceSetting.StopBits;
        }

        public override bool IsTransmittingData()
        {
            if (ExtractData().Count != 0)
                return true;
            else
                return false;
        }

        public override List<string> GetListError()
        {
            throw new NotImplementedException();
        }
    }
}
