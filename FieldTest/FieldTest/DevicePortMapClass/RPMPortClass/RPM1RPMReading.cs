﻿
using FieldTest.UtilitaryByte;
using FieldTest.XMLConfigClass;
using System;
using System.IO.Ports;

namespace FieldTest.DevicePortMapClass.RPMPortClass
{
    public class RPM1RPMReading : ADeviceReading
    {
        private const int NB_BYTE_READ = 6;
        private const int NB_BYTE_WRITE = 4;

        public RPM1RPMReading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
        }

        private bool isValid(byte[] rawDataRead)
        {
            if (rawDataRead[0] == 0x01 && rawDataRead[5] == 0x0d)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private double calculRPM(UInt32 nB)
        {
            if (nB == 0)
                return double.NaN;
            else
                return (1 / (nB * 0.25 * Math.Pow(10, -6))) * 60;
        }

        private void readConfig(ref SerialPort port, out double y)
        {
            byte[] rawDataRead = new byte[NB_BYTE_READ];
            y = double.NaN;

            int ind = 0;

            while (!isValid(rawDataRead))
            {
                
                writeConfig(ref port);

                ind = 0;

                while (ind != NB_BYTE_READ)
                {
                    rawDataRead[ind] = (byte)port.ReadByte();
                    ind++;
                }

                port.DiscardInBuffer();

            }
            
            byte[] rawDataNB = ByteConverter.TrimBufferByte(rawDataRead, 1, 4);

            UInt32 nB = BitConverter.ToUInt32(rawDataNB, 0);

            y = calculRPM(nB);
     
            port.DiscardInBuffer();

        }

        private void writeConfig(ref SerialPort port)
        {
            byte[] rawDataWriteRPM = new byte[] { 0x01, Convert.ToByte('F'), Convert.ToByte('1'), 0x0d };
            port.Write(rawDataWriteRPM, 0, NB_BYTE_WRITE);
        }

        public override void ReadDataSynchrone(SerialPort port, out double y)
        {
            y = double.NaN;
            
            readConfig(ref port, out y);
        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            throw new NotImplementedException();
        }

        public override string GetConcreteUnit()
        {
            return "rpm";
        }
    }
}
