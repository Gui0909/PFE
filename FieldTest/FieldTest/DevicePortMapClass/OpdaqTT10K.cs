﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using FieldTest.DevicePortMapClass.TT10KPortClass;

namespace FieldTest.DevicePortMapClass
{
    public class OpdaqTT10K : ADevicePortMapped
    {
        public SortedList<string, ADevicePortMapped> ListDevice { get; set; }

        public OpdaqTT10K(ADeviceSetting deviceSetting, SortedList<string, ADeviceReading> listReadDevice, bool enableError) : base(deviceSetting, listReadDevice, enableError)
        {
        }

        public override SortedList<string, Coordinate> ExtractData()
        {

            SortedList<string, Coordinate> listPlots = new SortedList<string, Coordinate>();
            SortedList<string, Coordinate> listTempPlots = new SortedList<string, Coordinate>();

            double rpm1 = double.NaN;
            double rpm2 = double.NaN;

            foreach (ADevicePortMapped device in ListDevice.Values)
            {

                listTempPlots = new SortedList<string, Coordinate>();
                //Gestion spéciel rpm et tt10k à revoir ou à enlever si le tt10k disparait
                if (device.DeviceName == DeviceSettingManager.DEVICE.TT10K_1.ToString())
                {
                    listTempPlots = ((TT10KPortMapped)device).ReadData(rpm1);
                    rpm1 = double.NaN;
                }
                else if (device.DeviceName == DeviceSettingManager.DEVICE.TT10K_2.ToString())
                {
                    listTempPlots = ((TT10KPortMapped)device).ReadData(rpm2);
                    rpm2 = double.NaN;
                }
                //Normalement pour tous les appareils
                else
                {
                    listTempPlots = device.ExtractData();
                }

                //Gestion spéciel rpm et tt10k à revoir ou à enlever si le tt10k disparait
                if (device.DeviceName == DeviceSettingManager.DEVICE.RPM.ToString())
                {
                    rpm1 = listTempPlots["RPM 1"].Data;
                    rpm2 = listTempPlots["RPM 2"].Data;
                }

            }

            for (int i = 0; i < listTempPlots.Count; i++)
            {
                listPlots.Add(listTempPlots.Keys[i], listTempPlots.Values[i]);
                i++;
            }

            return listPlots;
       }

           

        public override List<string> GetListError()
        {
            throw new NotImplementedException();
        }

        public override bool IsTransmittingData()
        {
            throw new NotImplementedException();
        }

        public override void OpenPort()
        {
            foreach (ADevicePortMapped device in ListDevice.Values)
            {
                device.OpenPort();
            }
        }

        public override void ReadThread()
        {
            foreach (ADevicePortMapped device in ListDevice.Values)
            {
                device.ReadThread();
            }
        }

        public override void UpdateDeviceSetting(ADeviceSetting deviceSetting)
        {
            foreach (ADevicePortMapped device in ListDevice.Values)
            {
                device.UpdateDeviceSetting(deviceSetting);
            }
        }
    }
}
