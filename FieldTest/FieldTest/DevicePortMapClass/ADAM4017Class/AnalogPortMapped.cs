﻿using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;

namespace FieldTest.DevicePortMapClass.ADAM4017Class
{
    public class AnalogPortMapped : ADevicePortMapped
    {

        private string stringBuffer;
        private const char CR = '\r';
        private const string DATACOMMAND = "#01";
        private const string CONFIRM = "!01";
        private const int DATALENGHT = 56;

        public AnalogPortMapped(ADeviceSetting deviceSetting, SortedList<string, ADeviceReading> listReadDevice, bool enableError)
            : base(deviceSetting, listReadDevice, enableError)
        {
            this.stringBuffer = string.Empty;
        }

        private string getVoltCode()
        {
            switch (((AnalogSetting)base.DeviceSetting).Volt)
            {
                case DeviceSettingManager.VOLT.V10:
                    return "08";
                case DeviceSettingManager.VOLT.V5:
                    return "09";
                case DeviceSettingManager.VOLT.V1:
                    return "0A";
                case DeviceSettingManager.VOLT.mV500:
                    return "0B";
                case DeviceSettingManager.VOLT.mV150:
                    return "0C";
                case DeviceSettingManager.VOLT.mA20:
                    return "0D";
                default:
                    return "0A";
            }
        }

        private void changeVolt()
        {
            if (base.Port.IsOpen && IsTransmittingData())
            {
                string tempBuffer = string.Empty;

                while (tempBuffer != CONFIRM)
                {
                    base.Port.WriteLine("%0101" + getVoltCode() + "0800" + CR);
                    tempBuffer = base.Port.ReadTo(CR.ToString());
                }

                Thread.Sleep(7000);
            }
        }

        public override void UpdateDeviceSetting(ADeviceSetting deviceSetting)
        {
            base.DeviceSetting = deviceSetting;
            base.Port.BaudRate = (int)deviceSetting.BaudRate;
            base.Port.Parity = deviceSetting.Parity;
            base.Port.DataBits = deviceSetting.DataBits;
            base.Port.StopBits = deviceSetting.StopBits;

            changeVolt();
        }

        public override void OpenPort()
        {
           
            if (!base.Port.IsOpen)
            {
                base.Port.Open();
                base.BaseStream = base.Port.BaseStream;

                this.changeVolt();

                GC.SuppressFinalize(base.BaseStream);
            }

            //Ouvrir tous les channels rajoute 7 secondes si utilisé
            //base.Port.WriteLine("$015FF" + CR);
            //Thread.Sleep(7000);
            //base.Port.ReadTo(CR.ToString());

        }

        public override void ReadThread()
        {
            try 
            { 
                base.Port.WriteTimeout = 20;

                base.Port.WriteLine(DATACOMMAND + CR);

                Thread.Sleep(20);

                if (base.Port.IsOpen && base.Port.BytesToRead != 0)
                {
                    this.stringBuffer = base.Port.ReadTo(CR.ToString()).Trim('>');
                }
            }
            catch (IOException e)
            {
                if (!base.IsCheckingPort)
                    base.StartThreadPortDectection();
              
            }

        }

        public override bool IsTransmittingData()
        {
           
            base.Port.WriteTimeout = 20;
            base.Port.WriteLine(DATACOMMAND + CR);

            Thread.Sleep(20);

            if (base.Port.IsOpen && base.Port.BytesToRead != 0)
            {
                base.Port.ReadTo(CR.ToString());
                return true;
            }
            else
                return false;
          
        }

        public override SortedList<string, Coordinate> ExtractData()
        {

            lock (this.stringBuffer)
            {
                SortedList<string, Coordinate> listPlots = new SortedList<string, Coordinate>();
                SerialPort sp = base.Port;

                foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
                {
                    double y = double.NaN;

                    if (base.Port.IsOpen && this.stringBuffer != string.Empty && !this.stringBuffer.Contains('?') && this.stringBuffer.Length == DATALENGHT)
                    {
                        //Chaque valeur de channel retourné comporte 7 charactères et il y a 8 channel (0 à 7)
                        //Donc, parcours chaque channel en commencant à zéro et en augmentant de 7 à chaque nouveau channel.
                        //Note : Pour que les données soit envoyé vers le bon channel, l'ajout de channel dans le PortFactory doit être en ordre croissant (0 à .., 7)
                        string value = this.stringBuffer.Substring(7 * ((ChannelReading)readDevice).Numero, 7);
                        readDevice.ReadDataAsynchrone(value, out y);
                    }

                    listPlots.Add(readDevice.Name, new Coordinate(y));

                }

                this.stringBuffer = string.Empty;


                return listPlots;
            }
            
        }

        public override List<string> GetListError()
        {
            throw new NotImplementedException();
        }
    }
}
