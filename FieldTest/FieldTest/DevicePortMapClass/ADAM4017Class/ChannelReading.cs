﻿using FieldTest.XMLConfigClass;
using System.Globalization;
using System.IO.Ports;
using System.Linq;

namespace FieldTest.DevicePortMapClass.ADAM4017Class
{
    public class ChannelReading : ADeviceReading
    {
        private const char CR = '\r';
        private const string DATACOMMAND = "#01";
        public int Numero { get; set; }


        public ChannelReading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, int numero, bool isEnableHighAcqui)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
            this.Numero = numero;
        }

        //Méthode utilisé
        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            y = double.Parse(stringBuffer, CultureInfo.InvariantCulture);
        }

        //N'est pas utilisé présentement, c'est la méthode individuel pour traiter chaque channel
        public override void ReadDataSynchrone(SerialPort port, out double y)
        {
            string tempBuffer = string.Empty;

            if (port.IsOpen)
                port.WriteLine(DATACOMMAND + this.Numero.ToString() + CR);

            while (port.IsOpen && port.BytesToRead == 0);

            if (port.IsOpen)
                tempBuffer = port.ReadTo(CR.ToString());

            string data = tempBuffer.Trim('>');

            //Gestion des ? reçu quelque fois
            if (port.IsOpen && !data.Contains('?'))
                y = double.Parse(data, CultureInfo.InvariantCulture);
            else
                y = double.NaN;
 
        }

        public override string GetConcreteUnit()
        {
            return "volts";
        }
    }
}
