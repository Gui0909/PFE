﻿using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;

namespace FieldTest.DevicePortMapClass.GPSPortClass
{
    class GPSPortMapped : ADevicePortMapped
    {

        private const string HEAD = "$GPRMC";
        private string stringBuffer;

        private static bool isCheckSumGood(string gprmc)
        {
            int indexBegin = gprmc.IndexOf("$");
            int indexEnd = gprmc.LastIndexOf("*");

            if (indexBegin != 0 || indexEnd != gprmc.Length - 3)
            {
                return false;
            }

            string checkSumString = gprmc.Substring(indexEnd + 1, 2);
            int checkSum1 = Convert.ToInt32(checkSumString, 16);

            string valToCheck = gprmc.Substring(indexBegin + 1, indexEnd - 1);
            char c = valToCheck[0];
            for (int i = 1; i < valToCheck.Length; i++)
            {
                c ^= valToCheck[i];
            }

            return checkSum1 == c;
        }

        public override void ReadThread()
        {
            try
            {
                string tempBuffer = string.Empty;

                if (this.IsTransmittingData())
                {
                    while (tempBuffer == string.Empty || !tempBuffer.StartsWith(HEAD) || !isCheckSumGood(stringBuffer))
                    {
                        tempBuffer = base.Port.ReadTo("\r\n");
                    }

                    stringBuffer = tempBuffer;
                }
            }
            catch 
            {
                if (!base.IsCheckingPort)
                    base.StartThreadPortDectection();
            }

        }

        public GPSPortMapped(ADeviceSetting deviceSetting, SortedList<string, ADeviceReading> listReadDevice, bool enableError)
            : base(deviceSetting, listReadDevice, enableError)
        {
            stringBuffer = string.Empty;
        }

        public override SortedList<string, Coordinate> ExtractData()
        {
           lock (stringBuffer)
           {
                SortedList<string, Coordinate> listPlots = new SortedList<string, Coordinate>();
                double y = double.NaN;
              
                foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
                {
                    if (this.IsTransmittingData())
                        readDevice.ReadDataAsynchrone(stringBuffer, out y);

                    listPlots.Add(readDevice.Name, new Coordinate(y));
                }

                stringBuffer = string.Empty;

                return listPlots;
            }
        }

        public override void OpenPort()
        {

            if (!base.Port.IsOpen)
            {
                base.Port.Open();
                base.BaseStream = base.Port.BaseStream;

                GC.SuppressFinalize(base.BaseStream);
            }

        }

        public override void UpdateDeviceSetting(ADeviceSetting deviceSetting)
        {
            base.DeviceSetting = deviceSetting;
            base.Port.BaudRate = (int)deviceSetting.BaudRate;
            base.Port.Parity = deviceSetting.Parity;
            base.Port.DataBits = deviceSetting.DataBits;
            base.Port.StopBits = deviceSetting.StopBits;
        }

        public override bool IsTransmittingData()
        {
            if (base.Port.BytesToRead != 0)
                return true;
            else
                return false;
        }


        public override List<string> GetListError()
        {
            throw new NotImplementedException();
        }
    }
}
