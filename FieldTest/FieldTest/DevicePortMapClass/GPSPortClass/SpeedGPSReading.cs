﻿using System;
using System.Globalization;
using System.IO.Ports;
using FieldTest.XMLConfigClass;

namespace FieldTest.DevicePortMapClass.GPSPortClass
{
    public class SpeedGPSReading : ADeviceReading
    {

        private double getVitesse(string[] tabGPRMC)
        {
            return double.Parse(tabGPRMC[7], CultureInfo.InvariantCulture);

        }

        public SpeedGPSReading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
        }

        private void readConfig(ref string stringBuffer, out double y)
        {
            
            y = double.NaN;

            string[] tabGPRMC = stringBuffer.Split(',');

            if (tabGPRMC.Length == 12)
            {
                y = getVitesse(tabGPRMC);
            }
           
        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            readConfig(ref stringBuffer, out y);
        }

        public override void ReadDataSynchrone(SerialPort port, out double y)
        {
            throw new NotImplementedException();
        }

        public override string GetConcreteUnit()
        {
            return "";
        }
    }
}
