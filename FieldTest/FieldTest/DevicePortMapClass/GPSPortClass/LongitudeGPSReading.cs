﻿using System;
using System.Globalization;
using System.IO.Ports;
using FieldTest.XMLConfigClass;

namespace FieldTest.DevicePortMapClass.GPSPortClass
{
    public class LongitudeGPSReading : ADeviceReading
    {
        private const string EAST = "E";

        public LongitudeGPSReading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
        }

        private double getLongitude(string[] tabGPRMC)
        {

            double degree = double.Parse(tabGPRMC[5].Substring(0, 3), CultureInfo.InvariantCulture);
            double minute = double.Parse(tabGPRMC[5].Substring(3, 8), CultureInfo.InvariantCulture);

            if (tabGPRMC[6] == EAST)
                return degree + (minute / 60);
            else
                return -(degree + (minute / 60));

        }

        private void readConfig(ref string stringBuffer, out double y)
        {
            y = double.NaN;

            string[] tabGPRMC = stringBuffer.Split(',');

            if (tabGPRMC.Length == 12)
            {
                y = getLongitude(tabGPRMC);
            }
        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            readConfig(ref stringBuffer, out y);
        }

        public override void ReadDataSynchrone(SerialPort port, out double y)
        {
            throw new NotImplementedException();
        }

        public override string GetConcreteUnit()
        {
            return "";
        }
    }
}
