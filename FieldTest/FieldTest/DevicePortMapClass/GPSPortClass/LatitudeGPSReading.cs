﻿using System;
using System.Globalization;
using System.IO.Ports;
using FieldTest.XMLConfigClass;

namespace FieldTest.DevicePortMapClass.GPSPortClass
{
    public class LatitudeGPSReading : ADeviceReading
    {

        private const string NORD = "N";

        public LatitudeGPSReading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {

        }

        private void readConfig(ref string stringBuffer, out double y)
        {
            y = double.NaN;

            string[] tabGPRMC = stringBuffer.Split(',');

            if (tabGPRMC.Length == 12)
            {
                y = getLatitude(tabGPRMC);
            }
       
        }

        private double getLatitude(string[] tabGPRMC)
        {
            double degree = double.Parse(tabGPRMC[3].Substring(0, 2), CultureInfo.InvariantCulture);
            double minute = double.Parse(tabGPRMC[3].Substring(2, 8), CultureInfo.InvariantCulture);

            if (tabGPRMC[4] == NORD)
                return degree + (minute / 60);
            else
                return -(degree + (minute / 60));

        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            readConfig(ref stringBuffer, out y);
        }

        public override void ReadDataSynchrone(SerialPort port, out double y)
        {
            throw new NotImplementedException();
        }

        public override string GetConcreteUnit()
        {
            return "";
        }
    }
}
