﻿
using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace FieldTest.DevicePortMapClass
{
    //Represent le materiel (device) avec toutes ces informations de port et de traitement
    public abstract class ADevicePortMapped
    {
        public string DeviceName { get; set; }
        public string DeviceDescription { get; set; }
        public SerialPort Port { get; set; }
        public SortedList<string, ADeviceReading> ListDeviceReading { get; set; }
        public ADeviceSetting DeviceSetting { get; set; }
        public bool IsFaultDetected { get; set; }
        public bool IsCheckingPort { get; set; }
        public Stream BaseStream { get; set; }

        private Thread threadPortDectecting;
        



        public ADevicePortMapped(ADeviceSetting deviceSetting, SortedList<string, ADeviceReading> listReadDevice, bool enableError)
        {
            this.DeviceSetting = deviceSetting;
            this.DeviceName = deviceSetting.InstrumentName.ToString();
            this.DeviceDescription = deviceSetting.InstrumentTag.ToString();
            this.Port = new SerialPort(deviceSetting.ComPort, (int)deviceSetting.BaudRate, deviceSetting.Parity, deviceSetting.DataBits, deviceSetting.StopBits);
            this.IsFaultDetected = enableError;
            this.IsCheckingPort = false;
            this.threadPortDectecting = new Thread(checkPortConnected);
            


            this.ListDeviceReading = listReadDevice;
        }

        public void StopThreadPortDectection()
        {
            this.IsCheckingPort = false;
            this.threadPortDectecting.Join();
        }

        public void StartThreadPortDectection()
        {
            this.IsCheckingPort = true;
            this.threadPortDectecting.Start();
        }

        //Function du threadPortDectecting activé quand un port usb à été déconnecté
        private void checkPortConnected()
        {
            while (IsCheckingPort && !this.Port.IsOpen)
            {
                if (PortScanner.FindFTDIDevise(this.DeviceSetting.DescriptionPort))
                {
                    this.Port.Open();
                }
            }
        }

        abstract public void UpdateDeviceSetting(ADeviceSetting deviceSetting);

        abstract public void OpenPort();

        public void ClosePort()
        {
            //Permet d'empêcher crash lié à la déconexion d'un port usb.
            if (this.Port.IsOpen)
            {
                GC.ReRegisterForFinalize(this.BaseStream);
                this.Port.Close();
            }
        }

        public bool IsOpenPort()
        {
            return this.Port.IsOpen;
        }

        abstract public void ReadThread();

        abstract public bool IsTransmittingData();
        
        abstract public SortedList<string, Coordinate> ExtractData();

        abstract public List<string> GetListError();
    }
}
