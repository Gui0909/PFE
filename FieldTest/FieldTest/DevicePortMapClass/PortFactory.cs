﻿using FieldTest.DevicePortMapClass.ADAM4017Class;
using FieldTest.DevicePortMapClass.GPSPortClass;
using FieldTest.DevicePortMapClass.RPMPortClass;
using FieldTest.DevicePortMapClass.TPM2PortClass;
using FieldTest.DevicePortMapClass.TT10KPortClass;
using FieldTest.XMLConfigClass;
using FTD2XX_NET;
using System.Collections.Generic;


namespace FieldTest.DevicePortMapClass
{
    static class PortFactory
    {

        private const string GPS = "GPS";
        private const string RPM = "RPM";
        private const string TPM2 = "TPM2";
        private const string TT10K_1 = "TT10K_1";
        private const string TT10K_2 = "TT10K_2";
        private const string ANALOG = "ANALOG";

        private const string LONGITUDE = "Longitude";
        private const string LATITUDE = "Latitude";
        private const string VITESSE = "Vitesse";

        private const string RPM1 = "RPM 1";
        private const string RPM2 = "RPM 2";

        private const string CHANNEL1 = "CHANNEL 1";
        private const string CHANNEL2 = "CHANNEL 2";
        private const string CHANNEL3 = "CHANNEL 3";
        private const string CHANNEL4 = "CHANNEL 4";
        private const string CHANNEL5 = "CHANNEL 5";
        private const string CHANNEL6 = "CHANNEL 6";
        private const string CHANNEL7 = "CHANNEL 7";
        private const string CHANNEL8 = "CHANNEL 8";

        private const string STRAINGAGE = "Torque";
        private const string SHAFTSPEED = "RPM";
        private const string POWER = "Power";


        // Pas finie manque d'information
        public static ADevicePortMapped MapPort(FTDI ftdiWrapper, ref DeviceSettingManager setting)
        {
            string description;
            ftdiWrapper.GetDescription(out description);

            string comPort;
            ftdiWrapper.GetCOMPort(out comPort);

            ADevicePortMapped devicePortMapped;
            SortedList<string, ADeviceReading> listDeviceReading = new SortedList<string, ADeviceReading>();

            switch (description)
            {
                 case "OpDAQ Serial Hub (4 Ports) A":

                    setting.GetDeviceSetting(RPM).ComPort = comPort;
                    setting.GetDeviceSetting(RPM).DescriptionPort = description;

                    listDeviceReading.Add(RPM1, new RPM1RPMReading(DeviceSettingManager.DEVICE.RPM, RPM1, false));
                    listDeviceReading.Add(RPM2, new RPM2RPMReading(DeviceSettingManager.DEVICE.RPM, RPM2, false));

                    devicePortMapped = new RPMPortMapped(setting.GetDeviceSetting(RPM), listDeviceReading, false);

                   return devicePortMapped;

                 case "OpDAQ Serial Hub (4 Ports) D":

                    setting.GetDeviceSetting(GPS).ComPort = comPort;
                    setting.GetDeviceSetting(GPS).DescriptionPort = description;

                    listDeviceReading.Add(LATITUDE, new LatitudeGPSReading(DeviceSettingManager.DEVICE.GPS, LATITUDE, false));
                    listDeviceReading.Add(LONGITUDE, new LongitudeGPSReading(DeviceSettingManager.DEVICE.GPS, LONGITUDE, false));
                    listDeviceReading.Add(VITESSE, new SpeedGPSReading(DeviceSettingManager.DEVICE.GPS, VITESSE, false));

                    devicePortMapped = new GPSPortMapped(setting.GetDeviceSetting(GPS), listDeviceReading, false);

                    return devicePortMapped;

                //case "USB-RS422 Cable":
                //case "OpDAQ Serial Hub A":
                case "TTL232R":
                    setting.GetDeviceSetting(TPM2).ComPort = comPort;
                    setting.GetDeviceSetting(TPM2).DescriptionPort = description;
                    TPM2Setting settingTPM2 = (TPM2Setting)setting.GetDeviceSetting(TPM2);

                    listDeviceReading.Add(STRAINGAGE, new StrainGageTPM2Reading(DeviceSettingManager.DEVICE.TPM2, STRAINGAGE, true, settingTPM2.GaugeFactor, (double)settingTPM2.Gain, settingTPM2.ModulusElasticity, settingTPM2.OutsideDiameter, settingTPM2.InsideDiameter, settingTPM2.PoissonRatio));
                    listDeviceReading.Add(SHAFTSPEED, new ShaftSpeedTPM2Reading(DeviceSettingManager.DEVICE.TPM2, SHAFTSPEED, false));
                    listDeviceReading.Add(POWER, new PowerTPM2Reading(DeviceSettingManager.DEVICE.TPM2, POWER, false, settingTPM2.GaugeFactor, (double)settingTPM2.Gain, settingTPM2.ModulusElasticity, settingTPM2.OutsideDiameter, settingTPM2.InsideDiameter, settingTPM2.PoissonRatio));

                    devicePortMapped = new TPM2PortMapped(setting.GetDeviceSetting(TPM2), listDeviceReading, true);
                    
                    return devicePortMapped;

                case "OpDAQ Serial Hub (4 Ports) C":
                    setting.GetDeviceSetting(TT10K_2).ComPort = comPort;
                    setting.GetDeviceSetting(TT10K_2).DescriptionPort = description;
                    TT10KSetting settingTT10K2 = (TT10KSetting)setting.GetDeviceSetting(TT10K_2);

                    listDeviceReading.Add(STRAINGAGE, new StrainGageTT10KReading(DeviceSettingManager.DEVICE.TT10K_2, STRAINGAGE, false, settingTT10K2.GaugeFactor, (double)settingTT10K2.Gain, settingTT10K2.ModulusElasticity, settingTT10K2.OutsideDiameter, settingTT10K2.InsideDiameter, settingTT10K2.PoissonRatio));
                    listDeviceReading.Add(POWER, new PowerTT10KReading(DeviceSettingManager.DEVICE.TT10K_2, POWER, false, settingTT10K2.GaugeFactor, (double)settingTT10K2.Gain, settingTT10K2.ModulusElasticity, settingTT10K2.OutsideDiameter, settingTT10K2.InsideDiameter, settingTT10K2.PoissonRatio));

                    devicePortMapped = new TT10KPortMapped(setting.GetDeviceSetting(TT10K_2), listDeviceReading, false);

                    return devicePortMapped;

                case "OpDAQ Serial Hub (4 Ports) B":
                    setting.GetDeviceSetting(TT10K_1).ComPort = comPort;
                    setting.GetDeviceSetting(TT10K_1).DescriptionPort = description;
                    TT10KSetting settingTT10K1 = (TT10KSetting)setting.GetDeviceSetting(TT10K_1);

                    listDeviceReading.Add(STRAINGAGE, new StrainGageTT10KReading(DeviceSettingManager.DEVICE.TT10K_1, STRAINGAGE, true, settingTT10K1.GaugeFactor, (double)settingTT10K1.Gain, settingTT10K1.ModulusElasticity, settingTT10K1.OutsideDiameter, settingTT10K1.InsideDiameter, settingTT10K1.PoissonRatio));
                    listDeviceReading.Add(POWER, new PowerTT10KReading(DeviceSettingManager.DEVICE.TT10K_1, POWER, false, settingTT10K1.GaugeFactor, (double)settingTT10K1.Gain, settingTT10K1.ModulusElasticity, settingTT10K1.OutsideDiameter, settingTT10K1.InsideDiameter, settingTT10K1.PoissonRatio));

                    devicePortMapped = new TT10KPortMapped(setting.GetDeviceSetting(TT10K_1), listDeviceReading, false);

                    return devicePortMapped;

                case "FT2AI4017":
                    setting.GetDeviceSetting(ANALOG).ComPort = comPort;
                    setting.GetDeviceSetting(ANALOG).DescriptionPort = description;

                    listDeviceReading.Add(CHANNEL1, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL1, 0, false));
                    listDeviceReading.Add(CHANNEL2, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL2, 1, false));
                    listDeviceReading.Add(CHANNEL3, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL3, 2, false));
                    listDeviceReading.Add(CHANNEL4, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL4, 3, false));
                    listDeviceReading.Add(CHANNEL5, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL5, 4, false));
                    listDeviceReading.Add(CHANNEL6, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL6, 5, false));
                    listDeviceReading.Add(CHANNEL7, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL7, 6, false));
                    listDeviceReading.Add(CHANNEL8, new ChannelReading(DeviceSettingManager.DEVICE.ANALOG, CHANNEL8, 7, false));

                    devicePortMapped = new AnalogPortMapped(setting.GetDeviceSetting(ANALOG), listDeviceReading, false);

                    return devicePortMapped;
              
                default:
                    return null;
            }



           
        }

    }
}
