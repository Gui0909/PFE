﻿
using FieldTest.XMLConfigClass;
using System.IO.Ports;

namespace FieldTest.DevicePortMapClass
{
    public abstract class ADeviceReading
    {
        public DeviceSettingManager.DEVICE DeviceName { get; set; }
        public string Name { get; set; }
        public DeviceSettingManager.UNIT Unit { get; set; }
        public bool IsEnableHighAcquisition { get; set; }

        public ADeviceReading(DeviceSettingManager.DEVICE deviceName, string name, bool isEnableHighAcqui)
        {
            this.DeviceName = deviceName;
            this.Name = name;
            this.IsEnableHighAcquisition = isEnableHighAcqui;
        }

        abstract public string GetConcreteUnit();
        abstract public void ReadDataAsynchrone(string stringBuffer, out double y);

        //Possibilite de disparition si enlever du rpm
        abstract public void ReadDataSynchrone(SerialPort port, out double y);
    }
}
