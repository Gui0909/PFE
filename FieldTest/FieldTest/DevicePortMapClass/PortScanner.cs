﻿using System;
using System.Collections.Generic;
using FTD2XX_NET;
using System.Collections;
using System.Windows.Forms;
using FieldTest.XMLConfigClass;

namespace FieldTest.DevicePortMapClass
{
    //Utilise la librairie FTD2XX_NET
    static class PortScanner
    {
        //Function s'occupant de vérifier les ports connecter et de mapper ces dernier avec la class factory
        public static SortedList<string, ADevicePortMapped> FindAllFTDIDevise(ref DeviceSettingManager setting)
        {
            FTDI ftdiWrapper = new FTDI();
            SortedList<string, ADevicePortMapped> listPortMapping = new SortedList<string, ADevicePortMapped>();
            UInt32 deviceCount = 0;
            FTDI.FT_STATUS status = FTDI.FT_STATUS.FT_OTHER_ERROR;

            status = ftdiWrapper.GetNumberOfDevices(ref deviceCount);

            if (status != FTDI.FT_STATUS.FT_OK)
                throw new Exception(string.Format("Error after GetNumberOfDevices(), ftStatus: {0}", status));

            if (deviceCount == 0)
            {
                if (MessageBox.Show("No FTDI device found.", "Device missing", MessageBoxButtons.RetryCancel, MessageBoxIcon.Hand) == DialogResult.Retry)
                {
                    FindAllFTDIDevise(ref setting); 
                }
            }
            else
            {
                var deviceList = new FTDI.FT_DEVICE_INFO_NODE[deviceCount];
                status = ftdiWrapper.GetDeviceList(deviceList);

                if (status == FTDI.FT_STATUS.FT_OK)
                {
                    for (int i = 0; i < deviceCount; i++)
                    {
                        status = ftdiWrapper.OpenByDescription(deviceList[i].Description);

                        if (status != FTDI.FT_STATUS.FT_OK)
                            MessageBox.Show("Check if one of your device is already use. \n If you want to check ports again click on the button Check Port Device.", "Device already open.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        else
                        {
                            ADevicePortMapped portMap = PortFactory.MapPort(ftdiWrapper, ref setting);

                            if (portMap != null)
                            {
                                listPortMapping.Add(portMap.DeviceName, portMap);
                            }
                        }

                        ftdiWrapper.Close();
                    }

                    
                }
            }
         
            return listPortMapping;
        }

        public static bool FindFTDIDevise(string descriptionPort)
        {
            FTDI ftdiWrapper = new FTDI();
            SortedList<string, ADevicePortMapped> listPortMapping = new SortedList<string, ADevicePortMapped>();
            UInt32 deviceCount = 0;
            FTDI.FT_STATUS status = FTDI.FT_STATUS.FT_OTHER_ERROR;
            bool isConnected = false;


            status = ftdiWrapper.GetNumberOfDevices(ref deviceCount);

            if (status != FTDI.FT_STATUS.FT_OK)
                throw new Exception(string.Format("Error after GetNumberOfDevices(), ftStatus: {0}", status));

            if (deviceCount != 0)
            {
                var deviceList = new FTDI.FT_DEVICE_INFO_NODE[deviceCount];
                status = ftdiWrapper.GetDeviceList(deviceList);

                if (status == FTDI.FT_STATUS.FT_OK)
                {

                    status = ftdiWrapper.OpenByDescription(descriptionPort);
              

                    if (status == FTDI.FT_STATUS.FT_OK)
                    {
                        isConnected = true;
                    }
                    else
                    {
                        isConnected = false;
                    }

                }

            }

            ftdiWrapper.Close();

            return isConnected;
        }
    }
}
