﻿
using FieldTest.XMLConfigClass;
using System;
using System.IO.Ports;

namespace FieldTest.DevicePortMapClass.TT10KPortClass
{
    public class StrainGageTT10KReading : ADeviceReading
    {

        public double GaugeFactor { get; set; }
        public double GainFactor { get; set; }
        public double E {get; set;}
        public double OD {get; set;}
        public double ID {get; set;}
        public double V {get; set;}

        public StrainGageTT10KReading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui, double gaugefactor, double gainFactor, double e, double od, double id, double v)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
            this.GaugeFactor = gaugefactor;
            this.GainFactor = gainFactor;
            this.E = e;
            this.OD = od;
            this.ID = id;
            this.V = v;
        }

        private double getTorque(ref string stringBuffer)
        {
            double kt = 0;

            if (this.Unit == DeviceSettingManager.UNIT.METRIC)
                kt = 1.6 * Math.Pow(10, 10);
            else if (this.Unit == DeviceSettingManager.UNIT.ENGLISH)
                kt = 192;
            else
                kt = 192;

            return (getStrain(ref stringBuffer) * Math.PI * this.E * (Math.Pow(this.OD, 4) - (Math.Pow(this.ID, 4))) / (kt * this.OD * (1 + this.V)));
        }

        private double getStrain(ref string stringBuffer)
        {
           
             int value = int.Parse(stringBuffer);

             return value / (this.GaugeFactor * this.GainFactor);
        }

        private void readConfig(ref string stringBuffer, out double y)
        {
            y = double.NaN;

            y = getTorque(ref stringBuffer);

        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            readConfig(ref stringBuffer, out y);
        }

        public override void ReadDataSynchrone(SerialPort port, out double y)
        {
            throw new NotImplementedException();
        }



        public override string GetConcreteUnit()
        {
            if (base.Unit == DeviceSettingManager.UNIT.METRIC)
                return "N-m";
            else if (base.Unit == DeviceSettingManager.UNIT.ENGLISH)
                return "ft-lbs";
            else
                return string.Empty;
        }
    }
}
