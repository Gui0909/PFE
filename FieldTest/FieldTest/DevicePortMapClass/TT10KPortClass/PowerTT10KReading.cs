﻿
using FieldTest.XMLConfigClass;
using System;

namespace FieldTest.DevicePortMapClass.TT10KPortClass
{
    public class PowerTT10KReading : ADeviceReading
    {

        public double GaugeFactor { get; set; }
        public double GainFactor { get; set; }
        public double E { get; set; }
        public double OD { get; set; }
        public double ID { get; set; }
        public double V { get; set; }
        public double W { get; set; }


        public PowerTT10KReading(DeviceSettingManager.DEVICE tagDevice, string tagReadDevice, bool isEnableHighAcqui, double gaugefactor, double gainFactor, double e, double od, double id, double v)
            : base(tagDevice, tagReadDevice, isEnableHighAcqui)
        {
            this.GaugeFactor = gaugefactor;
            this.GainFactor = gainFactor;
            this.E = e;
            this.OD = od;
            this.ID = id;
            this.V = v;

        }

        private double getTorque(ref string stringBuffer)
        {
            double kt = 0;

            if (this.Unit == DeviceSettingManager.UNIT.METRIC)
                kt = 1.6 * Math.Pow(10, 10);
            else if(this.Unit == DeviceSettingManager.UNIT.ENGLISH)
                kt = 192;
            else
                kt = 192;

            return (getStrain(ref stringBuffer) * Math.PI * this.E * (Math.Pow(this.OD, 4) - (Math.Pow(this.ID, 4))) / (kt * this.OD * (1 + this.V)));
        }

        private double getStrain(ref string stringBuffer)
        {
            int value = int.Parse(stringBuffer);

            return value / (this.GaugeFactor * this.GainFactor);
        }

        private double getPower(ref string stringBuffer, double rpm)
        {
            int kp = 0;

            if (this.Unit == DeviceSettingManager.UNIT.METRIC)
                kp = 60;
            else if (this.Unit == DeviceSettingManager.UNIT.ENGLISH)
                kp = 33000;
            else
                kp = 33000;

            return (getTorque(ref stringBuffer) * 2 * Math.PI * rpm) / kp;
        }

        public override void ReadDataAsynchrone(string stringBuffer, out double y)
        {
            throw new NotImplementedException();
        }

        public void ReadDataAsynchrone(string stringBuffer, ref double rpm, out double y)
        {
            y = double.NaN;
            y = getPower(ref stringBuffer, rpm);
        }

        public override void ReadDataSynchrone(System.IO.Ports.SerialPort port, out double y)
        {
            throw new NotImplementedException();
        }

        public override string GetConcreteUnit()
        {
            if (base.Unit == DeviceSettingManager.UNIT.METRIC)
                return "watts ";
            else if (base.Unit == DeviceSettingManager.UNIT.ENGLISH)
                return "hp";
            else
                return string.Empty;
        }
    }
}
