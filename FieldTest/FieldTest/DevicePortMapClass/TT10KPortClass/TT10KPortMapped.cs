﻿
using FieldTest.DataClass;
using FieldTest.XMLConfigClass;
using System;
using System.Collections.Generic;

namespace FieldTest.DevicePortMapClass.TT10KPortClass
{
    public class TT10KPortMapped : ADevicePortMapped
    {
        private const int NB_BYTE_READ = 4;
        private const byte START_BYTE = 0x01;
        private const byte STOP_BYTE = 0x0D;

        private string stringBuffer;

        public TT10KPortMapped(ADeviceSetting deviceSetting, SortedList<string, ADeviceReading> listReadDevice, bool enableError)
            : base(deviceSetting, listReadDevice, enableError)
        {
            stringBuffer = string.Empty;
        }

        public override void ReadThread()
        {
            try
            {
                byte[] rawDataRead = new byte[NB_BYTE_READ];
                bool isValidData = false;

                if (IsTransmittingData())
                {
                    //Permet d'attendre qu'il y est des données dans le buffer
                    while (base.Port.BytesToRead < NB_BYTE_READ);
                        base.Port.Read(rawDataRead, 0, NB_BYTE_READ);

                    //Pour traiter le buffer remplit pour la première fois
                    if (rawDataRead[0] == START_BYTE && rawDataRead[3] == STOP_BYTE)
                    {
                        isValidData = true;
                    }
                    else
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            rawDataRead[i] = rawDataRead[i + 1];
                        }
                    }

                    //Pour traiter le buffer pour toutes les autres lectures avec un bit changeant à la fois
                    while (!isValidData)
                    {
                        if (base.Port.BytesToRead != 0)
                        {

                            rawDataRead[3] = (byte)base.Port.ReadByte();

                            if (rawDataRead[0] == START_BYTE && rawDataRead[3] == STOP_BYTE)
                            {
                                isValidData = true;
                            }
                            else
                            {
                                for (int i = 0; i < 3; i++)
                                {
                                    rawDataRead[i] = rawDataRead[i + 1];
                                }
                            }
                        }

                    }

                    Int16 strainGage = BitConverter.ToInt16(rawDataRead, 1);
                    this.stringBuffer = strainGage.ToString();
                }

            }
            catch 
            {
                if (!base.IsCheckingPort)
                    base.StartThreadPortDectection();
            }
        }

        public override SortedList<string, Coordinate> ExtractData()
        {
           
            lock (stringBuffer)
            {
                SortedList<string, Coordinate> listPlots = new SortedList<string, Coordinate>();

                if (stringBuffer != string.Empty)
                {
                    foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
                    {
                        double y = double.NaN;
                        readDevice.ReadDataAsynchrone(stringBuffer, out y);

                        listPlots.Add(readDevice.Name, new Coordinate(y));
                        
                    }

                    stringBuffer = string.Empty;
                }

                return listPlots;
            }
        }

        public SortedList<string, Coordinate> ReadData(double rpm)
        {

            lock (stringBuffer)
            {
                SortedList<string, Coordinate> listPlots = new SortedList<string, Coordinate>();

                if (stringBuffer != string.Empty)
                {
                    foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
                    {
                        double y = double.NaN;

                        //Pour toute les autres lectures
                        if (readDevice.Name != "Power")
                        {
                            readDevice.ReadDataAsynchrone(stringBuffer, out y);
                        }
                        //Si power gestion spéciale
                        else
                        {
                            ((PowerTT10KReading)readDevice).ReadDataAsynchrone(stringBuffer, ref rpm, out y);
                        }

                        listPlots.Add(readDevice.Name, new Coordinate(y));
                    }

                    stringBuffer = string.Empty;
                }
               

                return listPlots;
            }
        }

        public override void OpenPort()
        {
            if (!base.Port.IsOpen)
            {
                base.Port.Open();
                base.BaseStream = base.Port.BaseStream;

                GC.SuppressFinalize(base.BaseStream);
            }
        }

        public override void UpdateDeviceSetting(ADeviceSetting deviceSetting)
        {
            base.DeviceSetting = deviceSetting;

            foreach (ADeviceReading readDevice in base.ListDeviceReading.Values)
            {
                readDevice.Unit = deviceSetting.Unit;
            }

            base.Port.BaudRate = (int)deviceSetting.BaudRate;
            base.Port.Parity = deviceSetting.Parity;
            base.Port.DataBits = deviceSetting.DataBits;
            base.Port.StopBits = deviceSetting.StopBits;
        }

        public override bool IsTransmittingData()
        {
            if (base.Port.BytesToRead != 0)
                return true;
            else
                return false;
        }


        public override List<string> GetListError()
        {
            throw new NotImplementedException();
        }
    }
}
