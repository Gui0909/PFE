﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportAnalysisFieldTest
{
    public partial class ReportAnalysisFieldTest : Form
    {
        List<object> ListData { get; set; }

        public ReportAnalysisFieldTest(List<object> listData)
        {
            InitializeComponent();
            //this.DataBindings.Add("XStr", listData, "XStr");
            //this.DataBindings.Add("Y", listData, "Y");
            ListData = listData;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           // this.reportViewer1.RefreshReport();
            //this.reportViewer1.DataBindings.Add("XStr", listData, "XStr");
            //this.reportViewer1.DataBindings.Add("Y", listData, "Y");
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet", ListData));
            var a = this.reportViewer1.LocalReport.DataSources[0].Value;
            string b = "";
  
            this.reportViewer1.RefreshReport();
      
        }
    }
}